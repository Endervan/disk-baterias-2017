-- phpMyAdmin SQL Dump
-- version 4.4.8
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 13-Dez-2017 às 11:30
-- Versão do servidor: 5.6.24
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bateriasserrinha`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atuacoes`
--

CREATE TABLE IF NOT EXISTS `tb_atuacoes` (
  `idatuacao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=159 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_atuacoes`
--

INSERT INTO `tb_atuacoes` (`idatuacao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(158, 'Inovação', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'inovacao', NULL, NULL, NULL, NULL, '', '', '', NULL),
(157, 'Qualidade', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'qualidade', NULL, NULL, NULL, NULL, '', '', '', NULL),
(156, 'Ética', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'etica', NULL, NULL, NULL, NULL, '', '', '', NULL),
(155, 'Busca por resultados', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'busca-por-resultados', NULL, NULL, NULL, NULL, '', '', '', NULL),
(154, 'Compromisso com os clientes', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'compromisso-com-os-clientes', NULL, NULL, NULL, NULL, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `frase_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_8` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`, `frase_1`, `frase_2`, `frase_3`, `frase_4`, `frase_5`, `frase_6`, `frase_7`, `frase_8`) VALUES
(4, 'BANNER 1', '1212201711441178241461.jpg', NULL, 'SIM', NULL, '1', 'banner-1', '', NULL, NULL, '', '', '', '', '', '', '', ''),
(5, 'BANNER 2', '1212201711471246657453.jpg', NULL, 'SIM', NULL, '1', 'banner-2', '', NULL, NULL, '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '1805201709451394984633.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Produtos', '1805201706431139056779.jpg', NULL, 'produtos', 'SIM', NULL),
(3, 'Produto Dentro', '1805201706431186997387.jpg', NULL, 'produto-dentro', 'SIM', NULL),
(4, 'Serviços', '0405201704311244059489.jpg', NULL, 'serviços', 'SIM', NULL),
(5, 'Serviço Dentro', '0405201706291133038505.jpg', NULL, 'serviço-dentro', 'SIM', NULL),
(6, 'Dicas', '0405201707181288484786.jpg', NULL, 'dicas', 'SIM', NULL),
(7, 'Dicas Dentro', '0405201708541313562759.jpg', NULL, 'dicas-dentro', 'SIM', NULL),
(8, 'Contatos', '0405201710171193797995.jpg', NULL, 'contatos', 'SIM', NULL),
(9, 'Orçamento', '0505201701241340219496.jpg', NULL, 'orçamento', 'SIM', NULL),
(10, 'Mobile Empresa', '0505201706151392297089.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(11, 'Mobile Produtos', '0505201706491301042808.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(12, 'Mobile Marcas', '0505201706491301042808.jpg', NULL, NULL, 'SIM', NULL),
(13, 'Mobile Produtos Dentro', '0505201706491301042808.jpg', NULL, NULL, 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_mensagens`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_mensagens` (
  `idcategoriamensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_mensagens`
--

INSERT INTO `tb_categorias_mensagens` (`idcategoriamensagem`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Mensagens de aniversário', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-aniversario', 'Mensagens de aniversário', 'Mensagens de aniversário', 'Mensagens de aniversário'),
(2, 'Mensagens de casamento', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-casamento', 'Mensagens de casamento', 'Mensagens de casamento', 'Mensagens de casamento'),
(3, 'Mensagens dia das mães', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-dia-das-maes', 'Mensagens dia das mães', 'Mensagens dia das mães', 'Mensagens dia das mães'),
(4, 'Mensagens de amor', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-amor', 'Mensagens de amor', 'Mensagens de amor', 'Mensagens de amor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(97, 'Yokohama', NULL, '1805201704371274310866.jpg', 'SIM', NULL, 'yokohama', '', '', ''),
(98, 'Michelin', NULL, '1805201704381266171799.jpg', 'SIM', NULL, 'michelin', '', '', ''),
(99, 'Pirelli', NULL, '1805201704391393915322.png', 'SIM', NULL, 'pirelli', '', '', ''),
(100, 'Bridgestone', NULL, '1805201704401172349408.jpg', 'SIM', NULL, 'bridgestone', '', '', ''),
(101, 'Firestone', NULL, '1805201704411193488464.jpg', 'SIM', NULL, 'firestone', '', '', ''),
(102, 'BFGoodrich', NULL, '1805201704411212116213.jpg', 'SIM', NULL, 'bfgoodrich', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_unidades`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_unidades` (
  `idcategoriaunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaunidade_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_unidades`
--

INSERT INTO `tb_categorias_unidades` (`idcategoriaunidade`, `titulo`, `idcategoriaunidade_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(103, 'GOIÂNIA', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'goiÂnia', '', '', ''),
(105, 'BRASÍLIA', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'brasÍlia', '', '', ''),
(104, 'ANÁPOLIS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'anÁpolis', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_clientes`
--

CREATE TABLE IF NOT EXISTS `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(40, 'VIA ENGENHARIA S.A', NULL, '2301201712493305767977..jpg', 'SIM', NULL, 'via-engenharia-sa', '', '', '', ''),
(41, 'BRASAL ADMINISTRAÇÃO E PARTICIPAÇÕES', NULL, '2301201712508758238206..jpg', 'SIM', NULL, 'brasal-administracao-e-participacoes', '', '', '', ''),
(42, 'EMPLAVI EMPREENDIMENTOS IMOBILIARIOS', NULL, '2301201712516345713362..jpg', 'SIM', NULL, 'emplavi-empreendimentos-imobiliarios', '', '', '', ''),
(43, 'ANTARES ENGENHARIA', NULL, '2301201712524572469099..jpg', 'SIM', NULL, 'antares-engenharia', '', '', '', ''),
(44, 'BROOKFIELD EMPREENDIMENTOS S.A', NULL, '2301201712532904414695..jpg', 'SIM', NULL, 'brookfield-empreendimentos-sa', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `link_place` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`, `link_place`, `instagram`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Rua C69 N.99 Quadra C22 Lote 32 - Jardim Goiás - Goiânia - Goiás', '3515-1111', '99646-7175', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61153.47748275862!2d-49.295201009588446!3d-16.67226189216418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1220d555557%3A0xfce1794c20456c2a!2sGriffe+Pneus+Michellin+Av.+85!5e0!3m2!1spt-BR!2s', NULL, NULL, 'junior@homewebbrasil.com.br', 'https://plus.google.com/103116009734588753907?hl=pt_BR', '', '', '(62)', '(62)', '', '', '', '', 'https://www.google.com.br/maps/place/Griffe+Pneus+Michellin+Jardim+Goi%C3%A1s+-+Flamboyant/@-16.7079353,-49.2366508,17z/data=!3m1!4b1!4m5!3m4!1s0x935ef1abc70916d7:0x116e733ee7fce10a!8m2!3d-16.7079353!4d-49.2344621', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(1, 'Vantagens do Alumínio', '<p>\r\n	As esquadrias de alum&iacute;nio s&atilde;o esteticamente bonitas, possibilitam ampla variedade de cores e tons em pintura eletrost&aacute;tica a p&oacute; ou anodiza&ccedil;&atilde;o, refor&ccedil;ando ainda mais sua resist&ecirc;ncia e propiciando uma apar&ecirc;ncia mais uniforme e agrad&aacute;vel de acordo com a cor a ser utilizada em seu projeto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecnicamente, o alum&iacute;nio &eacute; de extrema versatilidade, adequando-se aos mais variados tipos de projetos e de dimens&otilde;es de v&atilde;os. O alum&iacute;nio apresenta uma natural resist&ecirc;ncia &agrave; corros&atilde;o, o que garante longa vida &uacute;til para as esquadrias sem grandes esfor&ccedil;os e preocupa&ccedil;&atilde;o em sua manuten&ccedil;&atilde;o. Permite a fabrica&ccedil;&atilde;o de portas, janelas, esquadrias em v&aacute;rias tipologias. Recebem vidros simples, temperados, duplos insulados e laminados mesmo os de espessuras maiores. &Eacute;, tamb&eacute;m, o material que melhor aceita todos os componentes (acess&oacute;rios) e elementos de veda&ccedil;&atilde;o (escovas de veda&ccedil;&atilde;o, borrachas de EPDM, silicone).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quando bem projetados, constru&iacute;dos e instalados, os caixilhos de alum&iacute;nio apresentam elevado desempenho quanto &agrave; estanqueidade ao vento e &agrave; chuva. Por tudo isso, os caixilhos de alum&iacute;nio s&atilde;o os preferidos da arquitetura e da constru&ccedil;&atilde;o civil nos edif&iacute;cios residenciais, comerciais e institucionais, tanto de &aacute;reas urbanas como industriais e litor&acirc;neas. O alum&iacute;nio &eacute; um material leve, estrutural e de baixa manuten&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rajas Esquadrias de Alum&iacute;nio trabalha com diversas linhas de perfis do mercado.</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'vantagens-do-aluminio', '', '', '', NULL),
(2, 'Qual alumínio escolher para as esquadrias da sua casa?', '<p>\r\n	Qual alum&iacute;nio escolher para as esquadrias da sua casa?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Na hora de comprar as janelas e portas de alum&iacute;nio e vidro, surge aquela d&uacute;vida: Qual cor escolher?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pois bem, a cor de alum&iacute;nio mais usada hoje &eacute; a branca. Cl&aacute;ssica e discreta, acaba compondo mais com os ambientes pintados em cores claras (em geral a maioria das casas tem a cor geral interna branca ou algum tom suave).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mas outro alum&iacute;nio, vem roubando a cena por onde passa, destinado a casas contempor&acirc;neas que tendem para a mistura de cores s&oacute;brias como o cinza e elementos amadeirados. Trata-se do alum&iacute;nio preto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte:&nbsp;http://www.casanovah.com.br</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'qual-aluminio-escolher-para-as-esquadrias-da-sua-casa', '', '', '', NULL),
(3, 'Porque Esquadrias de Alumínio?', '<p>\r\n	Devo usar esquadrias de aluminio ou esquadrias de PVC? A maioria das pessoas que constroem ja fizeram essa pergunta.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atualmente encontramos no mercado os seguintes tipos de esquadrias:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esquadria de Alum&iacute;nio: As portas e janelas de alum&iacute;nio s&atilde;o amplamente utilizadas por estarem h&aacute; mais tempo no mercado e apresentarem algumas vantagens como custo beneficio, durabilidade e versatilidade. A aus&ecirc;ncia de manuten&ccedil;&atilde;o &eacute; outra qualidade das esquadrias de alum&iacute;nio. Tambem podemos destacar a baixa condutividade do aluminio, que assim como a esquadria de PVC auxilia no consumo de energia quando pensamos em ar condicionado.</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'porque-esquadrias-de-aluminio', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'HOME - ENTREGA RÁPIDA', '<p>Em 30 minutos, se tiver disponibilidade de entregador.</p>', 'SIM', 0, '', '', '', 'entrega-rÁpida', NULL, NULL, NULL),
(2, 'HOME - LEVAMOS A MAQUININHA', '<p>\r\n	Mesmo na emerg&ecirc;ncia pague com o cart&atilde;o.</p>', 'SIM', 0, '', '', '', 'home--levamos-a-maquininha', NULL, NULL, NULL),
(3, 'HOME - INSTALAÇÃO GRÁTIS', '<p>\r\n	Em 30 minutos, se tiver disponibilidade de entregador.</p>', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(4, 'HOME - Conheça a Manos Baterias', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been t\rhe industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of t\rype and scrambled it to make a type specimen book. It has survived not only five centuries, but also \rthe leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s\r with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with deskto\rp publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(5, 'Empresa', '<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum h<br />\r\n	as been the industry&#39;s standard dummy text ever since the 1500s, when an unknown prin<br />\r\n	ter took a galley of type and scramble it to make a type specimen book. It has survived no<br />\r\n	t only five centuries, but also the leap into electronic typesetting, remaining essentially un<br />\r\n	changed. It was popularised in the 1960s with the release of Letraset sheets containing L<br />\r\n	orem Ipsum passages, and more recently with desktop publishing software like Aldus Page<br />\r\n	Maker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum h<br />\r\n	as been the industry&#39;s standard dummy text ever since the 1500s, when an unknown prin<br />\r\n	ter took a galley of type and scramble it to make a type specimen book. It has survived no<br />\r\n	t only five centuries, but also the leap into electronic typesetting, remaining essentially un<br />\r\n	changed. It was popularised in the 1960s with the release of Letraset sheets containing L<br />\r\n	orem Ipsum passages, and more recently with desktop publishing software like Aldus Page<br />\r\n	Maker including versions of Lorem Ipsum.</p>', 'SIM', 0, '', '', '', 'empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentos`
--

CREATE TABLE IF NOT EXISTS `tb_equipamentos` (
  `idequipamento` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaequipamento` int(10) unsigned NOT NULL,
  `id_subcategoriaequipamento` int(11) DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modelo_produto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_equipamentos`
--

INSERT INTO `tb_equipamentos` (`idequipamento`, `titulo`, `imagem`, `descricao`, `id_categoriaequipamento`, `id_subcategoriaequipamento`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`) VALUES
(1, 'ESQUADRIAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	As esquadrias de alum&iacute;nio s&atilde;o esteticamente bonitas, possibilitam ampla variedade de cores e tons em pintura eletrost&aacute;tica a p&oacute; ou anodiza&ccedil;&atilde;o, refor&ccedil;ando ainda mais sua resist&ecirc;ncia e propiciando uma apar&ecirc;ncia mais uniforme e agrad&aacute;vel de acordo com a cor a ser utilizada em seu projeto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecnicamente, o alum&iacute;nio &eacute; de extrema versatilidade, adequando-se aos mais variados tipos de projetos e de dimens&otilde;es de v&atilde;os. O alum&iacute;nio apresenta uma natural resist&ecirc;ncia &agrave; corros&atilde;o, o que garante longa vida &uacute;til para as esquadrias sem grandes esfor&ccedil;os e preocupa&ccedil;&atilde;o em sua manuten&ccedil;&atilde;o. Permite a fabrica&ccedil;&atilde;o de portas, janelas, esquadrias em v&aacute;rias tipologias. Recebem vidros simples, temperados, duplos insulados e laminados mesmo os de espessuras maiores. &Eacute;, tamb&eacute;m, o material que melhor aceita todos os componentes (acess&oacute;rios) e elementos de veda&ccedil;&atilde;o (escovas de veda&ccedil;&atilde;o, borrachas de EPDM, silicone).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quando bem projetados, constru&iacute;dos e instalados, os caixilhos de alum&iacute;nio apresentam elevado desempenho quanto &agrave; estanqueidade ao vento e &agrave; chuva. Por tudo isso, os caixilhos de alum&iacute;nio s&atilde;o os preferidos da arquitetura e da constru&ccedil;&atilde;o civil nos edif&iacute;cios residenciais, comerciais e institucionais, tanto de &aacute;reas urbanas como industriais e litor&acirc;neas. O alum&iacute;nio &eacute; um material leve, estrutural e de baixa manuten&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rajas Esquadrias de Alum&iacute;nio trabalha com diversas linhas de perfis do mercado.</p>', 94, NULL, '', '', '', 'SIM', 0, 'esquadrias-de-aluminio', '99999', NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(32, 'JANELAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Nossas janelas de alum&iacute;nio s&atilde;o ideais para dar um toque requintado ao ambiente. Os melhores produtos com alta qualidade, ideiais para varanda, sacadas e salas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'janelas-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(33, 'PORTAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Disponibilizamos uma linha de portas de alum&iacute;nio que atendem todos os tipos de ambientes, dos mais simples aos mais requintados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	S&atilde;o confeccionados em alum&iacute;nio com os melhores e mais variados tipos de acabamento; toda linha possui alta resist&ecirc;ncia a intemp&eacute;ries. Oferecemos diversos modelos de ma&ccedil;anetas e puxadores.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'portas-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(34, 'FACHADA STRUCTURAL GLAZING', '1603201707151347515644.jpg', '<p>\r\n	Com a aplica&ccedil;&atilde;o da tecnologia Structural Glazing nas obras comerciais e residenciais, o empreendimento ganha seu valor no mercado imobili&aacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O avan&ccedil;o das modalidades e estilos das esquadrias de alum&iacute;nio, arquitetos utilizam de potentes ferramentas para criar ambientes altamente atrativos com muito mais intensidade de luz e um sentimento maior de espa&ccedil;o f&iacute;sico.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Structural Glazing proporciona a intera&ccedil;&atilde;o entre o funcional e o belo, onde sua constru&ccedil;&atilde;o oferecer&aacute; muito mais conforto e praticidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato com a Rajas Esquadrias de Alum&iacute;nio, que elaboraremos um planejamento perfeito &agrave; sua obra para implementa&ccedil;&atilde;o do Structural Glazing</p>', 96, NULL, '', '', '', 'SIM', 0, 'fachada-structural-glazing', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(35, 'REVESTIMENTO ACM', '1603201707151347515644.jpg', '<p>\r\n	Aluminium Composite Material. Laminado de duas chapas de alum&iacute;nio, sob tens&atilde;o controlada com um n&uacute;cleo de polietileno de baixa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Redu&ccedil;&atilde;o de custos com manuten&ccedil;&atilde;o: ACM protege a parede externa da polui&ccedil;&atilde;o e do clima.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mant&eacute;m a modernidade da fachada por muito tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A face externa do ACM bloqueia as radia&ccedil;&otilde;es solares. A c&acirc;mara de ar e o isolamento t&eacute;rmico reduzem a transmiss&atilde;o de calor por convec&ccedil;&atilde;o (frio ou calor, custo reduzido de ar condicionado).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sistema de fachadas ventilada, protege as paredes estruturais e n&atilde;o estruturais das trocas bruscas de temperatura, reduzindo dilata&ccedil;&otilde;es t&eacute;rmicas e impedindo riscos de fissura&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Possui reten&ccedil;&atilde;o de ru&iacute;dos e/ou vibra&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Proporciona redu&ccedil;&atilde;o de cargas aplicadas na estrutura da obra, racionalizando as se&ccedil;&otilde;es de vigas, pilares e funda&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sua estrutura de fixa&ccedil;&atilde;o tamb&eacute;m &eacute; leve, diminuindo a sobrecarga nas estruturas de apoio.</p>', 96, NULL, '', '', '', 'SIM', 0, 'revestimento-acm', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(36, 'BRISES DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Painel composto por perfil direitos e esquerdos formando m&oacute;dulos vari&aacute;veis, que se unem a outro painel id&ecirc;ntico mediante conectores de policarbonatos, obtendo-se um elemento compacto.</p>\r\n<p>\r\n	Brises s&atilde;o particularmente &uacute;til para fachadas quando se necessita diminuir a a&ccedil;&atilde;o da luz direta, ou quando se requer privacidade sem perder a luminosidade, ventila&ccedil;&atilde;o e vis&atilde;o do exterior.</p>\r\n<p>\r\n	Instala-se mediante uma ancoragem que se fixa diretamente na estrutura da obra e com um suporte de ajuste telesc&oacute;pico que se une ao Brise.</p>', 95, NULL, '', '', '', 'SIM', 0, 'brises-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(37, 'PORTÕES DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Com mais de 24 anos de atua&ccedil;&atilde;o no mercado de esquadrias em alum&iacute;nio fabricamos port&otilde;es de imensa qualidade e beleza no modelos e padr&otilde;es desejados pelo cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Trabalhamos com port&otilde;es em alum&iacute;nio anodizado nas cores : fosco, preto e bronze e com pintura eletrost&aacute;tica nas cores : branco, verde, azul, vermelho, bege... entre tantas outras.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'portoes-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipes`
--

CREATE TABLE IF NOT EXISTS `tb_equipes` (
  `idequipe` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE IF NOT EXISTS `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_facebook`
--

CREATE TABLE IF NOT EXISTS `tb_facebook` (
  `idface` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_fornecedores`
--

CREATE TABLE IF NOT EXISTS `tb_fornecedores` (
  `idfornecedor` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_fornecedores`
--

INSERT INTO `tb_fornecedores` (`idfornecedor`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(40, 'VIA ENGENHARIA S.A', NULL, '2301201712493305767977..jpg', 'SIM', NULL, 'via-engenharia-sa', '', '', '', ''),
(41, 'BRASAL ADMINISTRAÇÃO E PARTICIPAÇÕES', NULL, '2301201712508758238206..jpg', 'SIM', NULL, 'brasal-administracao-e-participacoes', '', '', '', ''),
(42, 'EMPLAVI EMPREENDIMENTOS IMOBILIARIOS', NULL, '2301201712516345713362..jpg', 'SIM', NULL, 'emplavi-empreendimentos-imobiliarios', '', '', '', ''),
(43, 'ANTARES ENGENHARIA', NULL, '2301201712524572469099..jpg', 'SIM', NULL, 'antares-engenharia', '', '', '', ''),
(44, 'BROOKFIELD EMPREENDIMENTOS S.A', NULL, '2301201712532904414695..jpg', 'SIM', NULL, 'brookfield-empreendimentos-sa', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_fretes`
--

CREATE TABLE IF NOT EXISTS `tb_fretes` (
  `idfrete` int(10) unsigned NOT NULL,
  `titulo` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(145) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_fretes`
--

INSERT INTO `tb_fretes` (`idfrete`, `titulo`, `uf`, `valor`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Taguatinga Sul', 'DF', 10.5, '', 0, 'taguatinga-sul'),
(2, 'Samambaia', '', 7.98, 'SIM', 0, 'samambaia'),
(3, 'Recanto das Emas', '', 15, 'SIM', 0, 'recanto-das-emas'),
(4, 'Samambaia Norte', '', 14.89, 'SIM', 0, 'samambaia-norte'),
(5, 'Asa Sul', '', 32.5, 'SIM', 0, 'asa-sul'),
(6, 'Asa Norte', '', 10.09, 'SIM', 0, 'asa-norte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias`
--

CREATE TABLE IF NOT EXISTS `tb_galerias` (
  `idgaleria` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias`
--

INSERT INTO `tb_galerias` (`idgaleria`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(36, 'BARES E RESTAURANTES', '1110201603492601083566.png', 'SIM', NULL, 'bares-e-restaurantes', NULL),
(37, 'LIMPEZA EM GERAL', '2409201612231228099970.png', 'SIM', NULL, 'limpeza-em-geral', NULL),
(38, 'ESCRITÓRIO E COMÉRCIO', '1110201603472583030532.png', 'SIM', NULL, 'escritorio-e-comercio', NULL),
(43, 'INDÚSTRIA E CONSTRUÇÃO CIVIL', '1110201603491738726090.png', 'SIM', NULL, 'industria-e-construcao-civil', NULL),
(44, 'ÁREA DE SAÚDE', '1110201603483164367804.png', 'SIM', NULL, 'area-de-saude', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_equipamentos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_equipamentos` (
  `id_galeriaequipamento` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_equipamento` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_equipamentos`
--

INSERT INTO `tb_galerias_equipamentos` (`id_galeriaequipamento`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_equipamento`) VALUES
(318, '1903201712231131483576.jpg', 'SIM', NULL, NULL, 1),
(319, '1903201712231272247915.jpg', 'SIM', NULL, NULL, 1),
(320, '1903201712461238500602.jpg', 'SIM', NULL, NULL, 1),
(321, '1903201712461268543830.jpg', 'SIM', NULL, NULL, 1),
(322, '1903201712461156664053.jpg', 'SIM', NULL, NULL, 1),
(323, '1903201712471288961912.jpg', 'SIM', NULL, NULL, 1),
(324, '1903201712471224158678.jpg', 'SIM', NULL, NULL, 1),
(325, '1903201712471218730278.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_geral`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_geral` (
  `id_galeriageral` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_geral`
--

INSERT INTO `tb_galerias_geral` (`id_galeriageral`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(126, '1110201606129370584777.jpg', 'SIM', NULL, NULL, 36),
(127, '1110201606128491200018.jpg', 'SIM', NULL, NULL, 36),
(128, '1110201606123935418981.jpg', 'SIM', NULL, NULL, 36),
(129, '1110201606125431446035.jpg', 'SIM', NULL, NULL, 36),
(130, '1110201606121990333273.jpg', 'SIM', NULL, NULL, 36),
(131, '1110201606129567316079.jpg', 'SIM', NULL, NULL, 36),
(132, '1110201606123052659552.jpg', 'SIM', NULL, NULL, 36),
(133, '1110201606128704088876.jpg', 'SIM', NULL, NULL, 36),
(134, '1110201606121395695730.jpg', 'SIM', NULL, NULL, 36),
(135, '1110201606129286724370.jpg', 'SIM', NULL, NULL, 36),
(136, '1110201606122500877558.jpg', 'SIM', NULL, NULL, 36),
(137, '1110201606127282586809.jpg', 'SIM', NULL, NULL, 36),
(138, '1110201606167336113651.jpg', 'SIM', NULL, NULL, 37),
(139, '1110201606167126144290.jpg', 'SIM', NULL, NULL, 37),
(140, '1110201606166641264855.jpg', 'SIM', NULL, NULL, 37),
(141, '1110201606166856153934.jpg', 'SIM', NULL, NULL, 37),
(142, '1110201606198216783127.jpg', 'SIM', NULL, NULL, 38),
(143, '1110201606194614951705.jpg', 'SIM', NULL, NULL, 38),
(144, '1110201606194581049274.jpg', 'SIM', NULL, NULL, 38),
(145, '1110201606196961685398.jpg', 'SIM', NULL, NULL, 38),
(146, '1110201606192095600320.jpg', 'SIM', NULL, NULL, 38),
(147, '1110201606236245474128.jpg', 'SIM', NULL, NULL, 43),
(148, '1110201606238815510364.jpg', 'SIM', NULL, NULL, 43),
(149, '1110201606237030607693.jpg', 'SIM', NULL, NULL, 43),
(150, '1110201606236153364814.jpg', 'SIM', NULL, NULL, 43),
(151, '1110201606235868531324.jpg', 'SIM', NULL, NULL, 43),
(152, '1110201606278714095320.jpg', 'SIM', NULL, NULL, 44),
(153, '1110201606278558400455.jpg', 'SIM', NULL, NULL, 44),
(154, '1110201606276515941147.jpg', 'SIM', NULL, NULL, 44),
(155, '1110201606272921856492.jpg', 'SIM', NULL, NULL, 44),
(156, '1110201606278393876192.jpg', 'SIM', NULL, NULL, 44),
(157, '1110201606279452905394.jpg', 'SIM', NULL, NULL, 44),
(158, '1110201606279434882745.jpg', 'SIM', NULL, NULL, 44);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=421 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(412, '1805201706471312623563.jpg', 'SIM', NULL, NULL, 38),
(413, '1805201706471367692317.jpg', 'SIM', NULL, NULL, 38),
(414, '1805201706471380524107.jpg', 'SIM', NULL, NULL, 38),
(415, '1805201706471291012833.jpg', 'SIM', NULL, NULL, 38),
(416, '1805201706471341232408.jpg', 'SIM', NULL, NULL, 38),
(417, '1805201706471334940215.jpg', 'SIM', NULL, NULL, 38),
(418, '1805201706471192631619.jpg', 'SIM', NULL, NULL, 38),
(419, '1805201706471191079837.jpg', 'SIM', NULL, NULL, 38),
(420, '1805201706471112267618.jpg', 'SIM', NULL, NULL, 38);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_servicos` (
  `id_galeriaservico` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_uniformes`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_uniformes` (
  `id_galeriauniformes` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_uniforme` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_uniformes`
--

INSERT INTO `tb_galerias_uniformes` (`id_galeriauniformes`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_uniforme`) VALUES
(111, '2809201612081274866319.jpg', 'SIM', NULL, NULL, 1),
(112, '2809201612081336658844.jpg', 'SIM', NULL, NULL, 1),
(113, '2809201612081284847276.jpg', 'SIM', NULL, NULL, 1),
(114, '2809201612081126000436.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(2, 'EMPRESA GALERIA 02', '2710201602115670501920.jpg', 'SIM', NULL, 'empresa-galeria-02', NULL),
(21, 'EMPRESA GALERIA 03', '2710201602114947246942.jpg', 'SIM', NULL, 'empresa-galeria-03', NULL),
(22, 'EMPRESA GALERIA 04', '2710201602115671943347.jpg', 'SIM', NULL, 'empresa-galeria-04', NULL),
(23, 'IMAGEM 4', '2710201603506180535070.jpg', 'SIM', NULL, 'imagem-4', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre', 'SIM'),
(3, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2010 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''4''', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''6''', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''6''', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''43''', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''43''', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''44''', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''44''', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''45''', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''46''', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''45''', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''3''', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''NAO'' WHERE idlogin = ''2''', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''SIM'' WHERE idlogin = ''2''', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''4''', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''5''', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''6''', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = '''' WHERE idlogin = ''2''', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = ''2'', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''4''', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''5''', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = ''1''', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''70''', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''73''', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''71''', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''72''', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''45''', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = ''45''', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''SIM'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''2''', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''5''', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''40''', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''42''', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''43''', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''44''', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''9''', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''10''', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''11''', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''12''', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''13''', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''14''', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''41''', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''46''', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''5''', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''3''', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''4''', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''14''', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''13''', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''15''', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''16''', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''18''', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''17''', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = ''43''', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1),
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:29', 1),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:38', 1),
(736, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: teste1@gmail.com', 'DELETE FROM tb_equipes WHERE idequipe = ''1''', '2016-08-25', '11:30:04', 1),
(737, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''2''', '2016-08-25', '11:30:06', 1),
(738, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''3''', '2016-08-25', '11:30:08', 1),
(739, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''4''', '2016-08-25', '11:30:09', 1),
(740, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''5''', '2016-08-25', '11:30:11', 1),
(741, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''6''', '2016-08-25', '11:30:13', 1),
(742, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''7''', '2016-08-25', '11:30:15', 1),
(743, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''8''', '2016-08-25', '11:30:19', 1),
(744, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''9''', '2016-08-25', '11:30:21', 1),
(745, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''10''', '2016-08-25', '11:30:23', 1),
(746, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''11''', '2016-08-25', '11:30:25', 1),
(747, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''12''', '2016-08-25', '11:30:27', 1),
(748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:12', 1),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:41', 1),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:55', 1),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:02', 1),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:09', 1),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:15', 1),
(754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:22', 1),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:47', 1),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:56', 1),
(757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:05', 1),
(758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:37', 1),
(759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:08', 1),
(760, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:30', 1),
(761, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''1''', '2016-08-25', '11:36:53', 1),
(762, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''2''', '2016-08-25', '11:36:54', 1),
(763, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''3''', '2016-08-25', '11:36:56', 1),
(764, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''4''', '2016-08-25', '11:36:58', 1),
(765, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''5''', '2016-08-25', '11:37:01', 1),
(766, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''6''', '2016-08-25', '11:37:03', 1),
(767, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''7''', '2016-08-25', '11:37:05', 1),
(768, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''8''', '2016-08-25', '11:37:07', 1),
(769, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''9''', '2016-08-25', '11:37:09', 1),
(770, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''10''', '2016-08-25', '11:37:10', 1),
(771, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''11''', '2016-08-25', '11:37:12', 1),
(772, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''12''', '2016-08-25', '11:37:13', 1),
(773, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''13''', '2016-08-25', '11:37:15', 1),
(774, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''14''', '2016-08-25', '11:37:17', 1),
(775, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''15''', '2016-08-25', '11:37:19', 1),
(776, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''16''', '2016-08-25', '11:37:21', 1),
(777, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''17''', '2016-08-25', '11:37:22', 1),
(778, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''18''', '2016-08-25', '11:37:24', 1),
(779, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-25', '11:38:15', 1),
(780, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-25', '11:38:17', 1),
(781, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-25', '11:38:19', 1),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:39:33', 1),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:41:06', 1),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:48:51', 1),
(785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:49:46', 1),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:51:10', 1),
(787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:18', 1),
(788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:50', 1),
(789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:02:47', 1),
(790, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''39''', '2016-08-25', '12:02:52', 1),
(791, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''40''', '2016-08-25', '12:02:55', 1),
(792, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''41''', '2016-08-25', '12:02:57', 1),
(793, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''42''', '2016-08-25', '12:02:59', 1),
(794, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''43''', '2016-08-25', '12:03:01', 1),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:03:41', 1),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:14:52', 1),
(797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:15:10', 1),
(798, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-08-25', '14:21:41', 1),
(799, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-08-25', '14:21:45', 1),
(800, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-08-25', '14:21:48', 1),
(801, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-08-25', '14:21:50', 1),
(802, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-08-25', '14:21:52', 1),
(803, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-08-25', '14:21:54', 1),
(804, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-08-25', '14:21:56', 1),
(805, 'CADASTRO DO CLIENTE ', '', '2016-08-25', '14:22:57', 1),
(806, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-08-25', '14:23:51', 1),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:32', 1),
(808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:55', 1),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:17', 1),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:30', 1),
(811, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''8''', '2016-08-25', '14:45:02', 1),
(812, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''50''', '2016-08-25', '14:45:07', 1),
(813, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''51''', '2016-08-25', '14:45:11', 1),
(814, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''52''', '2016-08-25', '14:45:15', 1),
(815, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''53''', '2016-08-25', '14:45:18', 1),
(816, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''54''', '2016-08-25', '14:45:20', 1),
(817, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''55''', '2016-08-25', '14:45:25', 1),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:01', 1),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:21', 1),
(820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:47', 1),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:52:26', 1),
(822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:55:43', 1),
(823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:57:09', 1),
(824, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:59:15', 1),
(825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:19', 1),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:59', 1),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:10:30', 1),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:12:32', 1),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:14', 1),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:41', 1),
(831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:20:23', 1),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:21:01', 1),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:41:28', 1),
(834, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:47', 1),
(835, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:58', 1),
(836, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:09', 1),
(837, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:17', 1),
(838, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:24', 1),
(839, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:32', 1),
(840, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:40', 1),
(841, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:49', 1),
(842, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:56', 1),
(843, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_atuacoes WHERE idatuacao = ''151''', '2016-08-26', '15:34:14', 1),
(844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:36:16', 1),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:42:46', 1),
(846, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:05', 1),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:22', 1),
(848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:33', 1),
(849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:46', 1),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:16', 1),
(851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:52', 1),
(852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:45:04', 1),
(853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:46:30', 1),
(854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '18:51:26', 0),
(855, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '18:51:30', 0),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:16:59', 0),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:18:46', 0),
(858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:21:34', 0),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:24:24', 0),
(860, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:24:28', 0),
(861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:33:13', 0),
(862, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:33:40', 0),
(863, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:41:11', 0),
(864, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:41:14', 0),
(865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:42:48', 0),
(866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:43:20', 0),
(867, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '19:43:24', 0),
(868, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-27', '19:43:27', 0),
(869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:47:39', 0),
(870, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:47:43', 0),
(871, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:50:56', 0),
(872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:54:50', 0),
(873, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:54:56', 0),
(874, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:54:58', 0),
(875, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:55:00', 0),
(876, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:56:22', 0),
(877, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:19:06', 0),
(878, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:19:10', 0),
(879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:20:55', 0),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:22:38', 0),
(881, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '20:22:41', 0),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:29:14', 0),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:32:24', 0),
(884, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '20:32:43', 0),
(885, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:34:14', 0),
(886, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:47:12', 0),
(887, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:47:22', 0),
(888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:49:08', 0),
(889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:53:49', 0),
(890, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:59:22', 0),
(891, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:01:55', 0),
(892, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:05:47', 0),
(893, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:27:16', 0),
(894, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:46:33', 0),
(895, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '11:46:28', 1),
(896, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''77''', '2016-08-29', '11:46:37', 1),
(897, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''78''', '2016-08-29', '11:46:39', 1),
(898, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''79''', '2016-08-29', '11:46:41', 1),
(899, 'CADASTRO DO CLIENTE ', '', '2016-08-29', '11:49:11', 1),
(900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '16:39:22', 13),
(901, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:47:03', 13),
(902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:25', 13),
(903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:41', 13),
(904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:52', 13),
(905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:26:42', 1),
(906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:28:49', 1),
(907, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:35:08', 1),
(908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:57:02', 1),
(909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:27:02', 1),
(910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:28:27', 1),
(911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:31:51', 1),
(912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:35:11', 1),
(913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:38:10', 1),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:53:56', 1),
(915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:01:02', 1),
(916, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:01:40', 1),
(917, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:03:58', 1),
(918, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:04:19', 1),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:05:43', 1),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:09:40', 1),
(921, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''74''', '2016-09-13', '19:09:48', 1),
(922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:26', 1),
(923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:47', 1),
(924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:15:07', 1),
(925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:57:44', 1),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '20:01:10', 1),
(927, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:23:45', 1),
(928, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:24:20', 1),
(929, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:27:15', 1),
(930, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:33:52', 1),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '10:39:26', 1),
(932, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:34:16', 1),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '11:46:08', 1),
(934, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:52:01', 1),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:07:45', 1),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:04', 1),
(937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:42', 1),
(938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:58', 1),
(939, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:11:57', 1),
(940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:29:28', 1),
(941, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:29:48', 1),
(942, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:30:16', 1),
(943, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:31:19', 1),
(944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '17:12:33', 1),
(945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '19:50:15', 1),
(946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '08:37:22', 1),
(947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:01:49', 1),
(948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:38:31', 1),
(949, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''37''', '2016-09-16', '09:38:38', 1),
(950, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''38''', '2016-09-16', '09:38:41', 1),
(951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:41:49', 1),
(952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:26', 1),
(953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:41', 1),
(954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:07:02', 1),
(955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:54:08', 1),
(956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:02:52', 1),
(957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:03:05', 1),
(958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:33:32', 1),
(959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '15:53:42', 1),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '19:20:41', 1),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '10:48:09', 1),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:49:25', 1),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:52:15', 1),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:08', 1),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:47', 1),
(966, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:48:21', 1),
(967, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''2''', '2016-09-17', '14:48:29', 1),
(968, 'DESATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''1''', '2016-09-17', '14:48:34', 1),
(969, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '14:50:07', 1),
(970, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''2''', '2016-09-17', '14:51:18', 1),
(971, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:51:21', 1),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:27:48', 1),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:28:34', 1),
(974, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '17:30:06', 1),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:13', 1),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:49', 1),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:11:03', 1),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:14:08', 1),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:11', 1),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:30', 1),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:48', 1),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:55:08', 1),
(983, 'CADASTRO DO CLIENTE ', '', '2016-09-18', '11:09:45', 1),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:43:17', 1),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:44:24', 1),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:51', 1),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:58', 1),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:07', 1),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:13', 1),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '15:06:09', 1),
(991, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''85''', '2016-09-19', '11:05:44', 1),
(992, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''76''', '2016-09-19', '11:05:46', 1),
(993, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''83''', '2016-09-19', '11:05:49', 1),
(994, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''84''', '2016-09-19', '11:05:52', 1),
(995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:07:45', 1),
(996, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:13:50', 1),
(997, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:14:46', 1),
(998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:15:35', 1),
(999, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:17:05', 1),
(1000, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:21:57', 1),
(1001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:03', 1),
(1002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:09', 1),
(1003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:15', 1),
(1004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:20', 1),
(1005, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:27:28', 1),
(1006, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:28:25', 1),
(1007, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:06:56', 1),
(1008, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:07:31', 1),
(1009, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:10:36', 1),
(1010, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:26', 1),
(1011, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:53', 1),
(1012, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '13:55:28', 1),
(1013, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:36:48', 1),
(1014, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:08', 1),
(1015, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:15', 1),
(1016, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:01', 1),
(1017, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:07', 1),
(1018, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:44:41', 1),
(1019, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:17', 1),
(1020, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:45', 1),
(1021, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:25', 1),
(1022, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:35', 1),
(1023, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:52', 1),
(1024, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:02:04', 1),
(1025, 'CADASTRO DO CLIENTE ', '', '2016-09-21', '14:58:35', 1),
(1026, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '15:45:26', 1),
(1027, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:06:57', 1),
(1028, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:07:12', 1),
(1029, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:51:52', 1),
(1030, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '20:17:45', 1),
(1031, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '08:46:52', 1),
(1032, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '09:19:31', 1),
(1033, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '10:09:37', 1),
(1034, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '11:36:44', 1),
(1035, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:23', 1),
(1036, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:40', 1),
(1037, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:04', 1),
(1038, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''39''', '2016-09-23', '12:49:16', 1),
(1039, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:42', 1),
(1040, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:07:13', 1),
(1041, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:20:03', 1),
(1042, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''40''', '2016-09-23', '14:07:52', 1),
(1043, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''41''', '2016-09-23', '14:07:56', 1),
(1044, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '16:32:57', 1),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:21:42', 1),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:22:23', 1),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:23:20', 1),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '19:59:32', 3),
(1049, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:05:57', 3),
(1050, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:06:32', 3),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:04', 3),
(1052, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:20', 3),
(1053, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:39', 3),
(1054, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:11', 3),
(1055, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:38', 3),
(1056, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:55', 3),
(1057, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:17', 3),
(1058, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:33', 3),
(1059, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_unidades WHERE idunidade = ''3''', '2016-09-26', '20:15:38', 3),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:17:17', 3),
(1061, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:13', 3),
(1062, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:47', 3),
(1063, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:07:05', 3),
(1064, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:34', 3),
(1065, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:47', 3),
(1066, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:08', 3),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:27', 3),
(1068, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:00:31', 3),
(1069, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:03:43', 3),
(1070, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2016-10-09', '15:02:38', 3),
(1071, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:08:48', 3),
(1072, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:09:23', 3),
(1073, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:09:50', 3),
(1074, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:02', 3),
(1075, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:18', 3),
(1076, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:10:51', 3),
(1077, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:03', 3),
(1078, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:24', 3),
(1079, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:53', 3),
(1080, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:07', 3),
(1081, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:49', 3),
(1082, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:01', 3),
(1083, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:23', 3),
(1084, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:38', 3),
(1085, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:04', 3),
(1086, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:28', 3),
(1087, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:40', 3),
(1088, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:47', 3),
(1089, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:14', 3),
(1090, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:22', 3),
(1091, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:23', 3),
(1092, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:30', 3),
(1093, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:22:21', 3),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:24:52', 3),
(1095, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:27:33', 3),
(1096, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:31:31', 3),
(1097, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:35:00', 3),
(1098, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:37:05', 3),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:37:34', 3),
(1100, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:39:37', 3),
(1101, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:41:29', 3),
(1102, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:43:11', 3),
(1103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:44:09', 3),
(1104, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:47:04', 3),
(1105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:49:03', 3),
(1106, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:50:09', 3),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:50:43', 3),
(1108, 'EXCLUSÃO DO LOGIN 19, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''19''', '2016-10-11', '13:05:30', 3),
(1109, 'EXCLUSÃO DO LOGIN 20, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''20''', '2016-10-11', '13:05:32', 3),
(1110, 'EXCLUSÃO DO LOGIN 21, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''21''', '2016-10-11', '13:05:34', 3),
(1111, 'EXCLUSÃO DO LOGIN 22, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''22''', '2016-10-11', '13:05:36', 3),
(1112, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:44:02', 3),
(1113, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:45:48', 3),
(1114, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:48:25', 3),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:48:50', 3),
(1116, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:51:01', 3),
(1117, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:53:55', 3),
(1118, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:56:29', 3),
(1119, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:58:04', 3),
(1120, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:59:16', 3),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:59:45', 3),
(1122, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:01:09', 3),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:01:33', 3),
(1124, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:02:41', 3),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:03:01', 3),
(1126, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:04:15', 3),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:33', 3),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:55', 3),
(1129, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:07:10', 3),
(1130, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:09:03', 3),
(1131, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:10:47', 3),
(1132, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:12:13', 3),
(1133, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:13:38', 3),
(1134, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:14:55', 3),
(1135, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:15:15', 3),
(1136, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:16:36', 3),
(1137, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:17:59', 3),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:22:48', 3),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:25:17', 3),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:26:00', 3),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:06', 3),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:55', 3),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:46:06', 3),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:47:39', 3),
(1145, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:48:02', 3),
(1146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:01', 3),
(1147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:37', 3),
(1148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '18:32:33', 3),
(1149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:20:42', 3),
(1150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:27:20', 3),
(1151, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '19:29:11', 3),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:30:38', 3),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:25:13', 3),
(1154, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''1''', '2016-10-11', '22:25:17', 3),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:30:29', 3),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:31:54', 3),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:40:25', 3),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:50:00', 3),
(1159, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-10-11', '22:50:08', 3),
(1160, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-10-11', '22:50:10', 3),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:51:37', 3),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:54:18', 3),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:04:12', 3),
(1164, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-10-11', '23:04:16', 3),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:09:40', 3),
(1166, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:22:21', 3),
(1167, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:23:46', 3),
(1168, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:24:49', 3),
(1169, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:25:46', 3),
(1170, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:26:22', 3),
(1171, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:03', 3),
(1172, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:55', 3),
(1173, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:28:45', 3),
(1174, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:29:31', 3),
(1175, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:45:46', 1),
(1176, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:20', 1),
(1177, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:46', 1),
(1178, 'DESATIVOU O LOGIN 31', 'UPDATE tb_clientes SET ativo = ''NAO'' WHERE idcliente = ''31''', '2016-10-25', '16:48:23', 1),
(1179, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:14', 1),
(1180, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:39', 1),
(1181, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:12', 1),
(1182, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:36', 1),
(1183, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:59', 1),
(1184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:55:28', 1),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:58:06', 1),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:11:41', 1),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:33:20', 1),
(1188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:05', 1),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:14', 1),
(1190, 'DESATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''11''', '2016-10-25', '17:47:19', 1),
(1191, 'DESATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''12''', '2016-10-25', '17:47:21', 1),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:56:16', 1),
(1193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:57:49', 1),
(1194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:01:26', 1),
(1195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:14:28', 1),
(1196, 'ATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''11''', '2016-10-25', '18:14:32', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:26:53', 1),
(1198, 'ATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''12''', '2016-10-25', '18:26:57', 1),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:03:00', 1),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:00', 1),
(1201, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-10-26', '18:33:05', 1),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:55', 1),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:40:01', 1),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:10:45', 1),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:03', 1),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:10', 1),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:18', 1),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:29:57', 1),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:30:44', 1),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:32:35', 1),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:34:08', 1),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:35:40', 1),
(1213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:36:34', 1),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:47:29', 1),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:49:00', 1),
(1216, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_galeria_empresa WHERE idgaleriaempresa = ''1''', '2016-10-27', '15:49:32', 1),
(1217, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:49:57', 1),
(1218, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''45''', '2016-10-27', '15:50:31', 1),
(1219, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:50:45', 1),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:20', 1),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:37', 1),
(1222, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:08:08', 1),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:08:51', 1),
(1224, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:17:24', 1),
(1225, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:25:59', 1),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:28:19', 1),
(1227, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:32:55', 1),
(1228, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:38:41', 1),
(1229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-26', '03:07:31', 1),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:54:44', 1),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:55:21', 1),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:08', 1),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:21', 1),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:05:57', 1),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:06:35', 1),
(1236, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:26:55', 3),
(1237, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:27:52', 3),
(1238, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:28:19', 3),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:10:04', 3),
(1240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:11:06', 3),
(1241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:36', 3),
(1242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:56', 3),
(1243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:17', 3),
(1244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:28', 3),
(1245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:40', 3),
(1246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:55', 3),
(1247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:08', 3),
(1248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:51', 3),
(1249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:18', 3),
(1250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:35', 3),
(1251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:49', 3),
(1252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:06', 3),
(1253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:29', 3),
(1254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:50', 3),
(1255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:33:29', 3),
(1256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:34:11', 3),
(1257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:52:09', 3),
(1258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:53:50', 3),
(1259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:54:26', 3),
(1260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:32', 3),
(1261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:49', 3),
(1262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:18:59', 3),
(1263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:39', 3),
(1264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:58', 3),
(1265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:08', 3),
(1266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:17', 3),
(1267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:15:39', 3),
(1268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:33:15', 3),
(1269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '12:09:39', 3),
(1270, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '13:14:03', 3),
(1271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '14:12:12', 3),
(1272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '15:56:06', 3),
(1273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '16:42:24', 3),
(1274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '17:53:31', 3),
(1275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:02:16', 3),
(1276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:31:59', 3),
(1277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:52:22', 3),
(1278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '19:41:09', 3),
(1279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '00:13:19', 3),
(1280, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:49', 3),
(1281, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:59', 3),
(1282, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:40:06', 3),
(1283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:41:05', 3),
(1284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:46:57', 3),
(1285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:25', 3),
(1286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:42', 3),
(1287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:42:08', 3),
(1288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:43:18', 3),
(1289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:44:05', 3),
(1290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:03:32', 3),
(1291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:37:32', 3),
(1292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:45:20', 1),
(1293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:49:00', 1),
(1294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:50:57', 1),
(1295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:53:12', 1),
(1296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:53:45', 1),
(1297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:03:39', 1),
(1298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:10:36', 1),
(1299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:11:11', 1),
(1300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:21:25', 1),
(1301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:22:51', 1),
(1302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:23:15', 1),
(1303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:29:34', 1),
(1304, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''45''', '2016-12-07', '11:30:02', 1),
(1305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:31:13', 1),
(1306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:57:52', 1),
(1307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:06:49', 1),
(1308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:08:52', 1),
(1309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:11:00', 1),
(1310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:13:02', 1),
(1311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:32:42', 1),
(1312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:33:58', 1),
(1313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:47:31', 1),
(1314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:47:51', 1),
(1315, 'DESATIVOU O LOGIN 6', 'UPDATE tb_servicos SET ativo = ''NAO'' WHERE idservico = ''6''', '2016-12-07', '12:48:16', 1),
(1316, 'DESATIVOU O LOGIN 7', 'UPDATE tb_servicos SET ativo = ''NAO'' WHERE idservico = ''7''', '2016-12-07', '12:48:18', 1),
(1317, 'CADASTRO DO CLIENTE ', '', '2016-12-08', '02:03:38', 1),
(1318, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''10''', '2016-12-08', '02:16:57', 1),
(1319, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''11''', '2016-12-08', '02:16:59', 1),
(1320, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''12''', '2016-12-08', '02:17:44', 1),
(1321, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''13''', '2016-12-08', '02:17:46', 1),
(1322, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''14''', '2016-12-08', '02:17:49', 1),
(1323, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''15''', '2016-12-08', '02:17:51', 1),
(1324, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''16''', '2016-12-08', '02:17:53', 1),
(1325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '02:20:00', 1),
(1326, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-12-08', '02:20:24', 1),
(1327, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '02:37:18', 1),
(1328, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '16:03:00', 1),
(1329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '21:52:47', 1),
(1330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '14:45:32', 1),
(1331, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:47:43', 1),
(1332, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:50:04', 1),
(1333, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:51:54', 1),
(1334, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:53:37', 1),
(1335, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:55:04', 1),
(1336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '14:58:15', 1),
(1337, 'DESATIVOU O LOGIN 90', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''90''', '2016-12-09', '14:58:52', 1),
(1338, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:00:29', 1),
(1339, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:01:58', 1),
(1340, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:05:34', 1),
(1341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '15:05:58', 1),
(1342, 'ATIVOU O LOGIN 90', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''90''', '2016-12-09', '15:06:02', 1),
(1343, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:07:55', 1),
(1344, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '22:45:36', 1),
(1345, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:48:13', 1),
(1346, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:50:38', 1),
(1347, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:52:20', 1),
(1348, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:53:48', 1),
(1349, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:55:56', 1),
(1350, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:57:44', 1),
(1351, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:00:34', 1),
(1352, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:01:12', 1),
(1353, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '23:06:32', 1),
(1354, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''7''', '2016-12-09', '23:07:03', 1),
(1355, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''6''', '2016-12-09', '23:07:07', 1),
(1356, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:07:47', 1),
(1357, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:08:15', 1),
(1358, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:08:49', 1),
(1359, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '23:22:13', 1),
(1360, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:23:53', 1),
(1361, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-12-09', '23:24:08', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1362, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:24:39', 1),
(1363, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:24:57', 1),
(1364, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:26:39', 1),
(1365, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:27:04', 1),
(1366, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:27:43', 1),
(1367, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:06', 1),
(1368, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:15', 1),
(1369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:23', 1),
(1370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:32', 1),
(1371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:30:36', 1),
(1372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:32:12', 1),
(1373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '19:55:52', 3),
(1374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-13', '16:44:42', 1),
(1375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:25:45', 1),
(1376, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:27:35', 1),
(1377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:27:56', 1),
(1378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:28:25', 1),
(1379, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:28:48', 1),
(1380, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:16', 1),
(1381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:40', 1),
(1382, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:58', 1),
(1383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:14', 1),
(1384, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:29', 1),
(1385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:45', 1),
(1386, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:31:59', 1),
(1387, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:33:59', 1),
(1388, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:34:26', 1),
(1389, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '15:29:57', 1),
(1390, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:38:27', 1),
(1391, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:39:03', 1),
(1392, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:41:23', 1),
(1393, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:43:06', 1),
(1394, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:44:08', 1),
(1395, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:45:59', 1),
(1396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:47:12', 1),
(1397, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:47:47', 1),
(1398, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:50:12', 1),
(1399, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:52:24', 1),
(1400, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:54:17', 1),
(1401, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:55:32', 1),
(1402, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:56:43', 1),
(1403, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:58:28', 1),
(1404, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:59:13', 1),
(1405, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:02', 1),
(1406, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:15', 1),
(1407, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:23', 1),
(1408, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:29', 1),
(1409, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:50', 1),
(1410, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:04', 1),
(1411, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:10', 1),
(1412, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:25', 1),
(1413, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:33', 1),
(1414, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:47', 1),
(1415, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:55', 1),
(1416, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:04:23', 1),
(1417, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:04:32', 1),
(1418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:05:45', 1),
(1419, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:12:16', 1),
(1420, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-12-15', '03:12:20', 1),
(1421, 'CADASTRO DO CLIENTE ', '', '2016-12-15', '03:20:08', 1),
(1422, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''7''', '2016-12-15', '03:21:00', 1),
(1423, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:23:29', 1),
(1424, 'ATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''7''', '2016-12-15', '03:23:32', 1),
(1425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:24:50', 1),
(1426, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:27:11', 1),
(1427, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-12-15', '03:27:47', 1),
(1428, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:30:33', 1),
(1429, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:35:57', 1),
(1430, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:36:57', 1),
(1431, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:41:28', 1),
(1432, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:41:51', 1),
(1433, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:51:19', 1),
(1434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:53:11', 1),
(1435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:53:22', 1),
(1436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:56:17', 1),
(1437, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:56:28', 1),
(1438, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:57:59', 1),
(1439, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:59:37', 1),
(1440, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:59:57', 1),
(1441, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:00:36', 1),
(1442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:00:52', 1),
(1443, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:01:11', 1),
(1444, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:01:40', 1),
(1445, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:05:19', 1),
(1446, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:05:39', 1),
(1447, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:07:28', 1),
(1448, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:09:19', 1),
(1449, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:28:00', 1),
(1450, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:30:40', 1),
(1451, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-12-29', '11:28:30', 1),
(1452, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:00', 1),
(1453, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:10', 1),
(1454, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:29', 1),
(1455, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:41:13', 1),
(1456, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '14:26:51', 1),
(1457, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '14:28:42', 1),
(1458, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:01:53', 1),
(1459, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:02:16', 1),
(1460, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:07:10', 1),
(1461, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '15:12:38', 1),
(1462, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '15:14:15', 1),
(1463, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:04:02', 1),
(1464, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:04:46', 1),
(1465, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '16:25:47', 1),
(1466, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '16:46:32', 1),
(1467, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:47:02', 1),
(1468, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:48:45', 1),
(1469, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:48:57', 1),
(1470, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:49:08', 1),
(1471, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:49:20', 1),
(1472, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '17:55:55', 1),
(1473, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '17:57:33', 1),
(1474, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''2''', '2016-12-29', '18:14:10', 1),
(1475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''46''', '2016-12-29', '18:14:12', 1),
(1476, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''44''', '2016-12-29', '18:14:16', 1),
(1477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '18:38:14', 1),
(1478, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '18:38:45', 1),
(1479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '19:52:19', 1),
(1480, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '20:00:32', 1),
(1481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:38:15', 1),
(1482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:38:31', 1),
(1483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:49:29', 1),
(1484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:49:41', 1),
(1485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:52:47', 1),
(1486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:53:02', 1),
(1487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '22:09:11', 1),
(1488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '23:20:53', 1),
(1489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '01:19:13', 1),
(1490, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''7''', '2016-12-31', '01:48:46', 1),
(1491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '03:04:50', 1),
(1492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '03:25:01', 1),
(1493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '12:49:31', 1),
(1494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '18:06:07', 1),
(1495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '18:40:52', 1),
(1496, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:39', 1),
(1497, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:48', 1),
(1498, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:56', 1),
(1499, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:33:18', 1),
(1500, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:35:52', 1),
(1501, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:55:07', 1),
(1502, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '19:23:25', 1),
(1503, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '20:09:59', 1),
(1504, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '20:11:33', 1),
(1505, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-02', '23:25:43', 1),
(1506, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-03', '00:18:23', 1),
(1507, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-03', '00:36:41', 1),
(1508, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '18:58:16', 3),
(1509, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '18:59:01', 3),
(1510, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:00:01', 3),
(1511, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:00:29', 3),
(1512, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:07:19', 3),
(1513, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:09:33', 3),
(1514, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:09:59', 3),
(1515, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:11:11', 3),
(1516, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:17:38', 3),
(1517, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:02', 3),
(1518, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:26', 3),
(1519, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:59', 3),
(1520, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:49:23', 1),
(1521, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:50:35', 1),
(1522, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:51:45', 1),
(1523, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:52:38', 1),
(1524, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:53:24', 1),
(1525, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:55:15', 1),
(1526, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:56:55', 1),
(1527, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:57:10', 1),
(1528, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:57:51', 1),
(1529, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:58:11', 1),
(1530, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:58:30', 1),
(1531, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:58:50', 1),
(1532, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:58:58', 1),
(1533, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:08', 1),
(1534, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:32', 1),
(1535, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:38', 1),
(1536, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:44', 1),
(1537, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:50', 1),
(1538, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:54', 1),
(1539, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:01:16', 1),
(1540, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:02:27', 1),
(1541, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:03:04', 1),
(1542, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:03:46', 1),
(1543, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:04:32', 1),
(1544, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:04:59', 1),
(1545, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:05:30', 1),
(1546, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:06:09', 1),
(1547, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:06:34', 1),
(1548, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:19:17', 1),
(1549, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:20:46', 1),
(1550, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:23:44', 1),
(1551, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '13:07:57', 1),
(1552, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:32:27', 1),
(1553, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:33:26', 1),
(1554, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:36:11', 1),
(1555, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:38:59', 1),
(1556, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:39:28', 1),
(1557, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:42:33', 1),
(1558, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:43:54', 1),
(1559, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:44:29', 1),
(1560, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:47:06', 1),
(1561, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:47:32', 1),
(1562, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:50:16', 1),
(1563, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:50:44', 1),
(1564, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:51:56', 1),
(1565, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:52:48', 1),
(1566, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '13:54:27', 1),
(1567, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''7''', '2017-02-27', '13:55:31', 1),
(1568, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:58:40', 1),
(1569, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:06:14', 1),
(1570, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:08:30', 1),
(1571, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '14:12:52', 1),
(1572, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:13:15', 1),
(1573, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:13:51', 1),
(1574, 'ATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''7''', '2017-02-27', '14:14:39', 1),
(1575, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:16:05', 1),
(1576, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:17:15', 1),
(1577, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:56:54', 1),
(1578, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '15:14:57', 1),
(1579, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '16:52:22', 1),
(1580, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-12', '09:05:14', 1),
(1581, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-15', '19:07:35', 1),
(1582, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-15', '18:36:54', 1),
(1583, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '12:52:07', 1),
(1584, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '13:04:00', 1),
(1585, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '13:59:02', 1),
(1586, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '14:05:17', 1),
(1587, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:42:38', 1),
(1588, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:54:18', 1),
(1589, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:55:51', 1),
(1590, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:57:15', 1),
(1591, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:59:24', 1),
(1592, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '18:39:14', 1),
(1593, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '19:15:16', 1),
(1594, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:28:05', 1),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:34:43', 1),
(1596, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:35:54', 1),
(1597, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:16:31', 1),
(1598, 'CADASTRO DO CLIENTE ', '', '2017-03-19', '23:26:58', 1),
(1599, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''1''', '2017-03-19', '23:27:36', 1),
(1600, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''2''', '2017-03-19', '23:27:40', 1),
(1601, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''3''', '2017-03-19', '23:27:45', 1),
(1602, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''8''', '2017-03-19', '23:27:52', 1),
(1603, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''9''', '2017-03-19', '23:27:56', 1),
(1604, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = ''10''', '2017-03-19', '23:28:01', 1),
(1605, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:35:11', 1),
(1606, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:36:33', 1),
(1607, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:39:47', 1),
(1608, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:57:24', 1),
(1609, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '00:10:50', 1),
(1610, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '17:56:33', 1),
(1611, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:08:57', 1),
(1612, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:25:38', 1),
(1613, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:25:52', 1),
(1614, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '21:23:10', 1),
(1615, 'CADASTRO DO CLIENTE ', '', '2017-03-20', '21:44:06', 1),
(1616, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '10:34:38', 1),
(1617, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:17:29', 1),
(1618, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:17:48', 1),
(1619, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:14', 1),
(1620, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:15', 1),
(1621, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:16', 1),
(1622, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''7''', '2017-03-21', '11:18:39', 1),
(1623, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''7''', '2017-03-21', '11:18:41', 1),
(1624, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '11:19:39', 1),
(1625, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '11:19:50', 1),
(1626, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2017-03-21', '11:21:53', 1),
(1627, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2017-03-21', '11:21:55', 1),
(1628, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2017-03-21', '11:21:57', 1),
(1629, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2017-03-21', '11:21:57', 1),
(1630, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2017-03-21', '11:21:58', 1),
(1631, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:22:22', 1),
(1632, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '12:35:53', 1),
(1633, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:00:12', 1),
(1634, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:00:34', 1),
(1635, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:01:21', 1),
(1636, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:01:25', 1),
(1637, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:01:45', 1),
(1638, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:02:02', 1),
(1639, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '02:54:51', 1),
(1640, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '21:28:06', 1),
(1641, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '21:43:17', 1),
(1642, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '22:00:34', 1),
(1643, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:29:19', 1),
(1644, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:35:40', 1),
(1645, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:56:52', 1),
(1646, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-25', '10:56:26', 1),
(1647, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-25', '13:26:02', 1),
(1648, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '08:59:43', 1),
(1649, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '08:59:53', 1),
(1650, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:01', 1),
(1651, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:11', 1),
(1652, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:33', 1),
(1653, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:02:27', 1),
(1654, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:10:56', 1),
(1655, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:11:15', 1),
(1656, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:15:27', 1),
(1657, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:15:50', 1),
(1658, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:16:03', 1),
(1659, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:16:13', 1),
(1660, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:23:28', 1),
(1661, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:25:07', 1),
(1662, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:01', 1),
(1663, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:18', 1),
(1664, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:41', 1),
(1665, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:47:59', 1),
(1666, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:16:46', 1),
(1667, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:47:43', 1),
(1668, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:48:04', 1),
(1669, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:48:47', 1),
(1670, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:53:24', 1),
(1671, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:55:26', 1),
(1672, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:56:06', 1),
(1673, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:57:28', 1),
(1674, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:57:44', 1),
(1675, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:58:07', 1),
(1676, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:59:23', 1),
(1677, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:59:36', 1),
(1678, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '12:00:05', 1),
(1679, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '12:01:40', 1),
(1680, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '12:02:09', 1),
(1681, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '16:20:43', 1),
(1682, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '16:24:19', 1),
(1683, 'EXCLUSÃO DO LOGIN 102, NOME: , Email: ', 'DELETE FROM tb_categorias_unidades WHERE idcategoriaunidade = ''102''', '2017-04-14', '16:39:41', 1),
(1684, 'EXCLUSÃO DO LOGIN 101, NOME: , Email: ', 'DELETE FROM tb_categorias_unidades WHERE idcategoriaunidade = ''101''', '2017-04-14', '16:40:19', 1),
(1685, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '16:40:49', 1),
(1686, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '16:53:12', 1),
(1687, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_unidades WHERE idunidade = ''1''', '2017-04-14', '17:08:34', 1),
(1688, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '17:08:52', 1),
(1689, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '17:09:58', 1),
(1690, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '18:28:47', 1),
(1691, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '10:44:44', 1),
(1692, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '10:58:34', 1),
(1693, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '11:08:39', 1),
(1694, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '11:09:42', 1),
(1695, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-16', '14:55:51', 1),
(1696, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-16', '17:02:15', 1),
(1697, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:17', 0),
(1698, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:28', 0),
(1699, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:44', 0),
(1700, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:53', 0),
(1701, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '13:58:40', 0),
(1702, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '14:07:15', 0),
(1703, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''2''', '2017-04-17', '20:19:36', 0),
(1704, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''2''', '2017-04-17', '20:20:04', 0),
(1705, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '23:47:18', 1),
(1706, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '23:49:21', 1),
(1707, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-18', '23:32:24', 1),
(1708, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:36:30', 1),
(1709, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:37:08', 1),
(1710, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:38:23', 1),
(1711, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '00:58:25', 1),
(1712, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '11:07:11', 1),
(1713, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '11:32:36', 1),
(1714, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '14:29:56', 1),
(1715, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '14:44:56', 1),
(1716, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '15:22:59', 1),
(1717, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:08:53', 1),
(1718, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:09:27', 1),
(1719, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:23:40', 1),
(1720, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '20:34:36', 1),
(1721, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '16:44:49', 1),
(1722, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '16:45:47', 1),
(1723, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:01:09', 0),
(1724, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '20:01:23', 0),
(1725, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '20:01:36', 0),
(1726, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:03:10', 0),
(1727, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:03:24', 0),
(1728, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:00', 0),
(1729, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:14', 0),
(1730, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:30', 0),
(1731, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:09', 0),
(1732, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:21', 0),
(1733, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:29', 0),
(1734, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:31:18', 1),
(1735, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:36:47', 1),
(1736, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:38:50', 1),
(1737, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '21:40:13', 0),
(1738, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '21:40:23', 0),
(1739, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:40:57', 0),
(1740, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:10', 0),
(1741, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:19', 0),
(1742, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:28', 0),
(1743, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:42:29', 0),
(1744, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:42:41', 0),
(1745, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:01', 0),
(1746, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:32', 0),
(1747, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:51', 0),
(1748, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''40''', '2017-04-20', '21:47:29', 0),
(1749, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:50:47', 1),
(1750, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:14:12', 1),
(1751, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:24:42', 1),
(1752, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:24:53', 1),
(1753, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:25:12', 1),
(1754, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = ''3''', '2017-04-20', '19:36:27', 1),
(1755, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:39:42', 1),
(1756, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:39:58', 1),
(1757, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:40:14', 1),
(1758, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '01:24:58', 1),
(1759, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '01:25:11', 1),
(1760, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:26:11', 1),
(1761, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:27:37', 1),
(1762, 'EXCLUSÃO DO LOGIN 47, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = ''47''', '2017-04-21', '01:30:18', 1),
(1763, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:31:05', 1),
(1764, 'EXCLUSÃO DO LOGIN 48, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = ''48''', '2017-04-21', '01:31:54', 1),
(1765, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '12:53:41', 0),
(1766, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '12:53:53', 0),
(1767, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '13:30:32', 1),
(1768, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '19:05:26', 1),
(1769, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '20:06:12', 1),
(1770, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-23', '12:26:31', 1),
(1771, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-23', '12:27:05', 1),
(1772, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:32:24', 1),
(1773, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:32:59', 1),
(1774, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:33:47', 1),
(1775, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:34:10', 1),
(1776, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:21:25', 1),
(1777, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:23:18', 1),
(1778, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:26:28', 1),
(1779, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:29:07', 1),
(1780, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:30:55', 1),
(1781, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:32:33', 1),
(1782, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:33:54', 1),
(1783, 'DESATIVOU O LOGIN 103', 'UPDATE tb_categorias_unidades SET ativo = ''NAO'' WHERE idcategoriaunidade = ''103''', '2017-05-03', '14:34:18', 1),
(1784, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:34:43', 1),
(1785, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:34:52', 1),
(1786, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:01', 1),
(1787, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:36', 1),
(1788, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:50', 1),
(1789, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:57', 1),
(1790, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:36:03', 1),
(1791, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:36:08', 1),
(1792, 'ATIVOU O LOGIN 103', 'UPDATE tb_categorias_unidades SET ativo = ''SIM'' WHERE idcategoriaunidade = ''103''', '2017-05-03', '14:36:51', 1),
(1793, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:37:46', 1),
(1794, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:38:29', 1),
(1795, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:38:42', 1),
(1796, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:39:23', 1),
(1797, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:39:41', 1),
(1798, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:40:20', 1),
(1799, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:40:33', 1),
(1800, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:41:19', 1),
(1801, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:41:47', 1),
(1802, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:42:20', 1),
(1803, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:44:42', 1),
(1804, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:45:15', 1),
(1805, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:50:45', 1),
(1806, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:52:52', 1),
(1807, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:54:15', 1),
(1808, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:56:08', 1),
(1809, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:57:55', 1),
(1810, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:58:53', 1),
(1811, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:03', 1),
(1812, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:09', 1),
(1813, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:31', 1),
(1814, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:36', 1),
(1815, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:41', 1),
(1816, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:45', 1),
(1817, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:51', 1),
(1818, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:56', 1),
(1819, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:01:23', 1),
(1820, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:02:15', 1),
(1821, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:02:38', 1),
(1822, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:03:03', 1),
(1823, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:03:22', 1),
(1824, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:04:05', 1),
(1825, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:04:41', 1),
(1826, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:07:31', 1),
(1827, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:08:08', 1),
(1828, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:08:36', 1),
(1829, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:09:20', 1),
(1830, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:10:28', 1),
(1831, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:11:50', 1),
(1832, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:12:54', 1),
(1833, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:13:23', 1),
(1834, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:13:47', 1),
(1835, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:14:07', 1),
(1836, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:15:57', 1),
(1837, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:17:02', 1),
(1838, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-05', '18:13:46', 1),
(1839, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2017-05-07', '14:55:58', 1),
(1840, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-08', '22:59:28', 1),
(1841, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-08', '23:00:42', 1),
(1842, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:10:39', 1),
(1843, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:20:04', 1),
(1844, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:20:31', 1),
(1845, 'CADASTRO DO CLIENTE ', '', '2017-05-09', '16:26:46', 1),
(1846, 'DESATIVOU O LOGIN 19', 'UPDATE tb_tipos_servicos SET ativo = ''NAO'' WHERE idtiposervico = ''19''', '2017-05-09', '16:27:21', 1),
(1847, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:28:14', 1),
(1848, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:28:39', 1),
(1849, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:31:54', 1),
(1850, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:32:24', 1),
(1851, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:34:31', 1),
(1852, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:44:59', 1),
(1853, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:45:32', 1),
(1854, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:46:13', 1),
(1855, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:46:41', 1),
(1856, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:47:53', 1),
(1857, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:52:33', 1),
(1858, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:53:26', 1),
(1859, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:53:58', 1),
(1860, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:00:19', 1),
(1861, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2017-05-10', '02:00:24', 1),
(1862, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:08:13', 1),
(1863, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:17:42', 1),
(1864, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:28:47', 1),
(1865, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '03:20:28', 1),
(1866, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '03:22:50', 1),
(1867, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2017-05-10', '03:24:02', 1),
(1868, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2017-05-10', '03:24:03', 1),
(1869, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '12:58:27', 1),
(1870, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '12:58:37', 1),
(1871, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:02:26', 1),
(1872, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:02:30', 1),
(1873, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:07:09', 1),
(1874, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:07:14', 1),
(1875, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:19:04', 1),
(1876, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:20:41', 1),
(1877, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:31:08', 1),
(1878, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '13:33:13', 1),
(1879, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '19:08:24', 1),
(1880, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '19:09:11', 1),
(1881, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '19:09:28', 1),
(1882, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:09:45', 1),
(1883, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:09:54', 1),
(1884, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:03', 1),
(1885, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:12', 1),
(1886, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:22', 1),
(1887, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:33', 1),
(1888, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:42', 1),
(1889, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:10:51', 1),
(1890, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:11:00', 1),
(1891, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:11:09', 1),
(1892, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:11:26', 1),
(1893, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:11:41', 1),
(1894, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:11:51', 1),
(1895, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:00', 1),
(1896, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:08', 1),
(1897, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:17', 1),
(1898, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:26', 1),
(1899, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:35', 1),
(1900, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:44', 1),
(1901, 'CADASTRO DO CLIENTE ', '', '2017-05-10', '19:12:55', 1),
(1902, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '19:23:43', 1),
(1903, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:16:16', 1),
(1904, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:16:54', 1),
(1905, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:17:27', 1),
(1906, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:17:59', 1),
(1907, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:21:42', 1),
(1908, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:23:01', 1),
(1909, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:23:54', 1),
(1910, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:24:24', 1),
(1911, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:24:51', 1),
(1912, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:25:18', 1),
(1913, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:25:47', 1),
(1914, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '12:47:02', 1),
(1915, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:26:41', 1),
(1916, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:27:19', 1),
(1917, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:27:39', 1),
(1918, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:27:56', 1),
(1919, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:28:17', 1),
(1920, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:28:33', 1),
(1921, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:28:44', 1),
(1922, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:28:55', 1),
(1923, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:29:05', 1),
(1924, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:29:32', 1),
(1925, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:29:45', 1),
(1926, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:29:57', 1),
(1927, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:45:00', 1),
(1928, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:45:57', 1),
(1929, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:48:29', 1),
(1930, 'CADASTRO DO CLIENTE ', '', '2017-05-11', '13:49:56', 1),
(1931, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:51:54', 1),
(1932, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2017-05-11', '13:52:03', 1),
(1933, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2017-05-11', '13:52:47', 1),
(1934, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '13:55:23', 1),
(1935, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '14:04:21', 1),
(1936, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '14:06:22', 1),
(1937, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '16:03:00', 1),
(1938, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''1''', '2017-05-11', '16:03:13', 1),
(1939, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2017-05-11', '16:03:45', 1),
(1940, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''1''', '2017-05-11', '16:09:40', 1),
(1941, 'DESATIVOU O LOGIN 9', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''9''', '2017-05-11', '16:09:43', 1),
(1942, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '16:17:14', 1),
(1943, 'ATIVOU O LOGIN 9', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''9''', '2017-05-11', '16:17:18', 1),
(1944, 'DESATIVOU O LOGIN 9', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''9''', '2017-05-11', '16:18:11', 1),
(1945, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '16:19:43', 1),
(1946, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:53:36', 1),
(1947, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:54:18', 1),
(1948, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:55:00', 1),
(1949, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:55:34', 1),
(1950, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:56:04', 1),
(1951, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:56:43', 1),
(1952, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:57:22', 1),
(1953, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:58:04', 1),
(1954, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:58:39', 1),
(1955, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:59:12', 1),
(1956, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '17:59:45', 1),
(1957, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '21:39:12', 0),
(1958, 'ATIVOU O LOGIN 9', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''9''', '2017-05-11', '21:39:46', 0),
(1959, 'CADASTRO DO CLIENTE ', '', '2017-05-11', '21:41:13', 0),
(1960, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2017-05-11', '21:41:20', 0),
(1961, 'DESATIVOU O LOGIN 9', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''9''', '2017-05-11', '21:41:23', 0),
(1962, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '21:41:59', 0),
(1963, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-11', '21:43:54', 0),
(1964, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '10:55:47', 0),
(1965, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''1''', '2017-05-17', '10:55:53', 0),
(1966, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '11:07:12', 0),
(1967, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2017-05-17', '11:07:19', 0),
(1968, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '11:08:38', 0),
(1969, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '11:12:24', 0),
(1970, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2017-05-17', '11:13:06', 0),
(1971, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '11:14:20', 0),
(1972, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '11:17:23', 0),
(1973, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2017-05-17', '11:17:35', 0),
(1974, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2017-05-17', '11:17:37', 0),
(1975, 'DESATIVOU O LOGIN 10', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''10''', '2017-05-17', '11:17:41', 0),
(1976, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '12:29:08', 0),
(1977, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '12:31:34', 0),
(1978, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '12:32:57', 0),
(1979, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''5''', '2017-05-17', '12:33:26', 0),
(1980, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''7''', '2017-05-17', '12:33:29', 0),
(1981, 'DESATIVOU O LOGIN 8', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''8''', '2017-05-17', '12:33:31', 0),
(1982, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '12:37:07', 0),
(1983, 'DESATIVOU O LOGIN 2', 'UPDATE tb_promocoes SET ativo = ''NAO'' WHERE idpromocao = ''2''', '2017-05-17', '12:37:20', 0),
(1984, 'DESATIVOU O LOGIN 46', 'UPDATE tb_promocoes SET ativo = ''NAO'' WHERE idpromocao = ''46''', '2017-05-17', '12:37:23', 0),
(1985, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-17', '12:38:18', 0),
(1986, 'CADASTRO DO CLIENTE ', '', '2017-05-17', '16:58:34', 1),
(1987, 'CADASTRO DO CLIENTE ', '', '2017-05-17', '16:58:49', 1),
(1988, 'CADASTRO DO CLIENTE ', '', '2017-05-17', '16:59:00', 1),
(1989, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''1''', '2017-05-18', '16:10:50', 1),
(1990, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2017-05-18', '16:10:53', 1),
(1991, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''3''', '2017-05-18', '16:10:54', 1),
(1992, 'CADASTRO DO CLIENTE ', '', '2017-05-18', '16:12:38', 1),
(1993, 'CADASTRO DO CLIENTE ', '', '2017-05-18', '16:17:00', 1),
(1994, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:18:26', 1),
(1995, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:37:24', 1),
(1996, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:38:58', 1),
(1997, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:39:33', 1),
(1998, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:40:06', 1),
(1999, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:41:02', 1),
(2000, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '16:41:53', 1),
(2001, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '17:03:41', 1),
(2002, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '18:40:10', 1),
(2003, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '18:43:09', 1),
(2004, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '18:43:21', 1),
(2005, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '21:43:16', 1),
(2006, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-18', '21:45:43', 1),
(2007, 'ALTERAÇÃO DO CLIENTE ', '', '2017-12-12', '23:09:51', 2),
(2008, 'ALTERAÇÃO DO CLIENTE ', '', '2017-12-12', '23:44:37', 2),
(2009, 'ALTERAÇÃO DO CLIENTE ', '', '2017-12-12', '23:47:15', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE IF NOT EXISTS `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_marcas_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_marcas_veiculos` (
  `idmarcaveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_marcas_veiculos`
--

INSERT INTO `tb_marcas_veiculos` (`idmarcaveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_tipoveiculo`) VALUES
(12, 'AGRALE', 0, 'SIM', 'agrale', 11),
(13, 'ALFA ROMEO', 0, 'SIM', 'alfa-romeo', 1),
(14, 'AUDI', 0, 'SIM', 'audi', 1),
(15, 'BMW', 0, 'SIM', 'bmw', 1),
(16, 'BRM', 0, 'SIM', 'brm', 1),
(17, 'BUGRE', 0, 'SIM', 'bugre', 1),
(18, 'CBT JIPE', 0, 'SIM', 'cbt-jipe', 1),
(19, 'CHANGAN', 0, 'SIM', 'changan', 1),
(20, 'CHERY', 0, 'SIM', 'chery', 1),
(21, 'CHRYSLER', 0, 'SIM', 'chrysler', 1),
(22, 'CITROËN', 0, 'SIM', 'citroËn', 1),
(23, 'CROSS LANDER', 0, 'SIM', 'cross-lander', 1),
(24, 'DAEWOO', 0, 'SIM', 'daewoo', 1),
(25, 'DODGE', 0, 'SIM', 'dodge', 1),
(26, 'EFFA', 0, 'SIM', 'effa', 1),
(27, 'FERRARI', 0, 'SIM', 'ferrari', 1),
(28, 'FIAT', 0, 'SIM', 'fiat', 1),
(29, 'FIBRAVAN', 0, 'SIM', 'fibravan', 1),
(30, 'FORD', 0, 'SIM', 'ford', 1),
(31, 'GM - CHEVROLET', 0, 'SIM', 'gm--chevrolet', 1),
(32, 'GREAT WALL', 0, 'SIM', 'great-wall', 1),
(33, 'HAFEI', 0, 'SIM', 'hafei', 1),
(34, 'HONDA', 0, 'SIM', 'honda', 1),
(35, 'HYUNDAI', 0, 'SIM', 'hyundai', 1),
(36, 'ISUZU', 0, 'SIM', 'isuzu', 1),
(37, 'JAC', 0, 'SIM', 'jac', 1),
(38, 'JAGUAR', 0, 'SIM', 'jaguar', 1),
(39, 'JEEP', 0, 'SIM', 'jeep', 1),
(40, 'JINBEI', 0, 'SIM', 'jinbei', 1),
(41, 'JPX', 0, 'SIM', 'jpx', 1),
(42, 'KIA MOTORS', 0, 'SIM', 'kia-motors', 1),
(43, 'LAND ROVER', 0, 'SIM', 'land-rover', 1),
(44, 'LEXUS', 0, 'SIM', 'lexus', 1),
(45, 'LOBINI', 0, 'SIM', 'lobini', 1),
(46, 'MAHINDRA', 0, 'SIM', 'mahindra', 1),
(47, 'MASERATI', 0, 'SIM', 'maserati', 1),
(48, 'MATRA', 0, 'SIM', 'matra', 1),
(49, 'MAZDA', 0, 'SIM', 'mazda', 1),
(50, 'MERCEDES-BENZ', 0, 'SIM', 'mercedesbenz', 1),
(51, 'MG', 0, 'SIM', 'mg', 1),
(52, 'MINI', 0, 'SIM', 'mini', 1),
(53, 'MITSUBISHI', 0, 'SIM', 'mitsubishi', 1),
(54, 'NISSAN', 0, 'SIM', 'nissan', 1),
(55, 'PEUGEOT', 0, 'SIM', 'peugeot', 1),
(56, 'PORSCHE', 0, 'SIM', 'porsche', 1),
(57, 'RENAULT', 0, 'SIM', 'renault', 1),
(58, 'SEAT', 0, 'SIM', 'seat', 1),
(59, 'SSANGYONG', 0, 'SIM', 'ssangyong', 1),
(60, 'SUBARU', 0, 'SIM', 'subaru', 1),
(61, 'SUZUKI', 0, 'SIM', 'suzuki', 1),
(62, 'TAC', 0, 'SIM', 'tac', 1),
(63, 'TOYOTA', 0, 'SIM', 'toyota', 1),
(64, 'TROLLER', 0, 'SIM', 'troller', 1),
(65, 'VOLVO', 0, 'SIM', 'volvo', 1),
(66, 'VW - VOLKSWAGEN', 0, 'SIM', 'vw--volkswagen', 1),
(67, 'WAKE', 0, 'SIM', 'wake', 1),
(68, 'WALK', 0, 'SIM', 'walk', 1),
(69, 'AGRALE', 0, 'SIM', 'agrale', 3),
(70, 'CICCOBUS', 0, 'SIM', 'ciccobus', 3),
(71, 'FORD', 0, 'SIM', 'ford', 3),
(72, 'GMC', 0, 'SIM', 'gmc', 3),
(73, 'IVECO', 0, 'SIM', 'iveco', 3),
(74, 'MERCEDES-BENZ', 0, 'SIM', 'mercedesbenz', 3),
(75, 'DURASTAR', 0, 'SIM', 'durastar', 3),
(76, 'SCANIA', 0, 'SIM', 'scania', 3),
(77, 'VOLVO', 0, 'SIM', 'volvo', 3),
(78, 'VW - VOLKSWAGEN', 0, 'SIM', 'vw--volkswagen', 3),
(79, 'WALKBUS', 0, 'SIM', 'walkbus', 3),
(80, '4830/5030/6630/7630/7830/8030', 0, 'SIM', '483050306630763078308030', 11),
(81, 'AGCO', 0, 'SIM', 'agco', 11),
(82, 'AGRALE TRATORES', 0, 'SIM', 'agrale-tratores', 11),
(83, 'BRASITALIA', 0, 'SIM', 'brasitalia', 11),
(84, 'CASE', 0, 'SIM', 'case', 11),
(85, 'CATERPILLAR TRATORES', 0, 'SIM', 'caterpillar-tratores', 11),
(86, 'CBT', 0, 'SIM', 'cbt', 11),
(87, 'CLARK TRATORES', 0, 'SIM', 'clark-tratores', 11),
(88, 'ENGESA', 0, 'SIM', 'engesa', 11),
(89, 'FIAT ALLIS', 0, 'SIM', 'fiat-allis', 11),
(90, 'HUBER WARCO/ DRESSER', 0, 'SIM', 'huber-warco-dresser', 11),
(91, 'HYSTER', 0, 'SIM', 'hyster', 11),
(92, 'IDEAL', 0, 'SIM', 'ideal', 11),
(93, 'JOHN DEERE', 0, 'SIM', 'john-deere', 11),
(94, 'KOMATSU', 0, 'SIM', 'komatsu', 11),
(95, 'KRANE-KAR', 0, 'SIM', 'kranekar', 11),
(96, 'MALVES', 0, 'SIM', 'malves', 11),
(97, 'MASSEY-FERGUSON/MAXION', 0, 'SIM', 'masseyfergusonmaxion', 11),
(98, 'MÜLLER', 0, 'SIM', 'mÜller', 11),
(99, 'POCLAIN', 0, 'SIM', 'poclain', 11),
(100, 'RANDON', 0, 'SIM', 'randon', 11),
(101, 'RETROESO/ 750', 0, 'SIM', 'retroeso-750', 11),
(102, 'SVEDALA DYNAPAC', 0, 'SIM', 'svedala-dynapac', 11),
(103, 'TEMA TERRA', 0, 'SIM', 'tema-terra', 11),
(104, 'TEREX', 0, 'SIM', 'terex', 11),
(105, 'TRATORES TX 1100/ 4100', 0, 'SIM', 'tratores-tx-1100-4100', 11),
(106, 'VALMET/ VALTRA', 0, 'SIM', 'valmet-valtra', 11),
(107, 'WABCO', 0, 'SIM', 'wabco', 11),
(108, 'YALE', 0, 'SIM', 'yale', 11),
(109, 'ZECTOR', 0, 'SIM', 'zector', 11),
(110, 'BMW', 0, 'SIM', 'bmw', 2),
(111, 'DAFRA', 0, 'SIM', 'dafra', 2),
(112, 'DUCATI', 0, 'SIM', 'ducati', 2),
(113, 'HARLEY DAVIDSON', 0, 'SIM', 'harley-davidson', 2),
(114, 'HONDA', 0, 'SIM', 'honda', 2),
(115, 'KASINSKI', 0, 'SIM', 'kasinski', 2),
(116, 'KAWASAKI', 0, 'SIM', 'kawasaki', 2),
(117, 'SHINERAY', 0, 'SIM', 'shineray', 2),
(118, 'SUNDOWN', 0, 'SIM', 'sundown', 2),
(119, 'SUZUKI', 0, 'SIM', 'suzuki', 2),
(120, 'TRAXX', 0, 'SIM', 'traxx', 2),
(121, 'TRIUMPH', 0, 'SIM', 'triumph', 2),
(122, 'YAMAHA', 0, 'SIM', 'yamaha', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_modelos_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_modelos_veiculos` (
  `idmodeloveiculo` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) CHARACTER SET latin1 NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_modelos_veiculos`
--

INSERT INTO `tb_modelos_veiculos` (`idmodeloveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_marcaveiculo`, `id_tipoveiculo`) VALUES
(8, 'MARRUÁ AM 100', 0, 'SIM', 'marrua-am-100', 12, 1),
(9, 'MARRUÁ AM200', 0, 'SIM', 'marruÁ-am200', 12, 1),
(10, 'MARRUÁ AM200 G2', 0, 'SIM', 'marruÁ-am200-g2', 12, 1),
(11, 'MARRUÁ AM200 CD', 0, 'SIM', 'marruÁ-am200-cd', 12, 1),
(12, 'MARRUÁ AM300', 0, 'SIM', 'marruÁ-am300', 12, 1),
(13, '8C', 0, 'SIM', '8c', 13, 1),
(14, '147', 0, 'SIM', '147', 13, 1),
(15, '156', 0, 'SIM', '156', 13, 1),
(16, '166', 0, 'SIM', '166', 13, 1),
(17, 'A1', 0, 'SIM', 'a1', 14, 1),
(18, 'A3', 0, 'SIM', 'a3', 14, 1),
(19, 'A6', 0, 'SIM', 'a6', 14, 1),
(20, 'A7', 0, 'SIM', 'a7', 14, 1),
(21, 'A8', 0, 'SIM', 'a8', 14, 1),
(22, 'ALLROAD', 0, 'SIM', 'allroad', 14, 1),
(23, 'Q3', 0, 'SIM', 'q3', 14, 1),
(24, 'Q5', 0, 'SIM', 'q5', 14, 1),
(25, 'Q7', 0, 'SIM', 'q7', 14, 1),
(26, 'R8', 0, 'SIM', 'r8', 14, 1),
(27, 'R8 SPYDER', 0, 'SIM', 'r8-spyder', 14, 1),
(28, 'R8 COUPÉ', 0, 'SIM', 'r8-coupÉ', 14, 1),
(29, 'RS3', 0, 'SIM', 'rs3', 14, 1),
(30, 'RS4', 0, 'SIM', 'rs4', 14, 1),
(31, 'RS5', 0, 'SIM', 'rs5', 14, 1),
(32, 'RS6', 0, 'SIM', 'rs6', 14, 1),
(33, 'RS7', 0, 'SIM', 'rs7', 14, 1),
(34, 'RSQ3', 0, 'SIM', 'rsq3', 14, 1),
(35, 'S3', 0, 'SIM', 's3', 14, 1),
(36, 'S4', 0, 'SIM', 's4', 14, 1),
(37, 'S5', 0, 'SIM', 's5', 14, 1),
(38, 'S6', 0, 'SIM', 's6', 14, 1),
(39, 'S7', 0, 'SIM', 's7', 14, 1),
(40, 'S8', 0, 'SIM', 's8', 14, 1),
(41, 'SQ5', 0, 'SIM', 'sq5', 14, 1),
(42, 'TT', 0, 'SIM', 'tt', 14, 1),
(43, 'TTS', 0, 'SIM', 'tts', 14, 1),
(44, '318iA', 0, 'SIM', '318ia', 15, 1),
(45, '320i', 0, 'SIM', '320i', 15, 1),
(46, '320iA', 0, 'SIM', '320ia', 15, 1),
(47, '323Ci', 0, 'SIM', '323ci', 15, 1),
(48, '323CiA', 0, 'SIM', '323cia', 15, 1),
(49, '323i', 0, 'SIM', '323i', 15, 1),
(50, '323iA', 0, 'SIM', '323ia', 15, 1),
(51, '323Ti', 0, 'SIM', '323ti', 15, 1),
(52, '325i', 0, 'SIM', '325i', 15, 1),
(53, '325iA', 0, 'SIM', '325ia', 15, 1),
(54, '328i', 0, 'SIM', '328i', 15, 1),
(55, '328iA', 0, 'SIM', '328ia', 15, 1),
(56, '330Ci', 0, 'SIM', '330ci', 15, 1),
(57, '330CiA', 0, 'SIM', '330cia', 15, 1),
(58, '330i', 0, 'SIM', '330i', 15, 1),
(59, '330iA', 0, 'SIM', '330ia', 15, 1),
(60, '525i', 0, 'SIM', '525i', 15, 1),
(61, '528i', 0, 'SIM', '528i', 15, 1),
(62, '528IA', 0, 'SIM', '528ia', 15, 1),
(63, '530i', 0, 'SIM', '530i', 15, 1),
(64, '530iA', 0, 'SIM', '530ia', 15, 1),
(65, '535iA', 0, 'SIM', '535ia', 15, 1),
(66, '540i', 0, 'SIM', '540i', 15, 1),
(67, '540iA', 0, 'SIM', '540ia', 15, 1),
(68, '540iTA', 0, 'SIM', '540ita', 15, 1),
(69, '545iA', 0, 'SIM', '545ia', 15, 1),
(70, '550iA', 0, 'SIM', '550ia', 15, 1),
(71, '645Ci', 0, 'SIM', '645ci', 15, 1),
(72, '645iA', 0, 'SIM', '645ia', 15, 1),
(73, '650Ci', 0, 'SIM', '650ci', 15, 1),
(74, '650iA', 0, 'SIM', '650ia', 15, 1),
(75, '740iA', 0, 'SIM', '740ia', 15, 1),
(76, '740iLA', 0, 'SIM', '740ila', 15, 1),
(77, '745iA', 0, 'SIM', '745ia', 15, 1),
(78, '750i', 0, 'SIM', '750i', 15, 1),
(79, '750iA', 0, 'SIM', '750ia', 15, 1),
(80, '750iLA', 0, 'SIM', '750ila', 15, 1),
(81, '760iL', 0, 'SIM', '760il', 15, 1),
(82, 'M', 0, 'SIM', 'm', 15, 1),
(83, 'M1', 0, 'SIM', 'm1', 15, 1),
(84, 'M3', 0, 'SIM', 'm3', 15, 1),
(85, 'M5', 0, 'SIM', 'm5', 15, 1),
(86, 'M6', 0, 'SIM', 'm6', 15, 1),
(87, 'S5', 0, 'SIM', 's5', 15, 1),
(88, 'X1', 0, 'SIM', 'x1', 15, 1),
(89, 'X5', 0, 'SIM', 'x5', 15, 1),
(90, 'X6', 0, 'SIM', 'x6', 15, 1),
(91, 'Z4', 0, 'SIM', 'z4', 15, 1),
(92, 'Z8', 0, 'SIM', 'z8', 15, 1),
(93, 'BUGGY', 0, 'SIM', 'buggy', 16, 1),
(94, 'BUGGY IV E V', 0, 'SIM', 'buggy-iv-e-v', 17, 1),
(95, 'BUGGY VII', 0, 'SIM', 'buggy-vii', 17, 1),
(96, 'UTILITY', 0, 'SIM', 'utility', 18, 1),
(97, 'CARGO', 0, 'SIM', 'cargo', 19, 1),
(98, 'FAMILY', 0, 'SIM', 'family', 19, 1),
(99, 'UTILITY', 0, 'SIM', 'utility', 19, 1),
(100, 'CELER', 0, 'SIM', 'celer', 20, 1),
(101, 'CIELO', 0, 'SIM', 'cielo', 20, 1),
(102, 'FACE', 0, 'SIM', 'face', 20, 1),
(103, 'QQ', 0, 'SIM', 'qq', 20, 1),
(104, 'RELY', 0, 'SIM', 'rely', 20, 1),
(105, 'S-18', 0, 'SIM', 's18', 20, 1),
(106, 'TIGGO', 0, 'SIM', 'tiggo', 20, 1),
(107, '300 C', 0, 'SIM', '300-c', 21, 1),
(108, 'CARAVAN', 0, 'SIM', 'caravan', 21, 1),
(109, 'CIRRUS', 0, 'SIM', 'cirrus', 21, 1),
(110, 'NEON', 0, 'SIM', 'neon', 21, 1),
(111, 'SEBRING', 0, 'SIM', 'sebring', 21, 1),
(112, 'STRATUS', 0, 'SIM', 'stratus', 21, 1),
(113, 'AIRCROSS', 0, 'SIM', 'aircross', 22, 1),
(114, 'C4', 0, 'SIM', 'c4', 22, 1),
(115, 'C4 LOUNGE', 0, 'SIM', 'c4-lounge', 22, 1),
(116, 'C5', 0, 'SIM', 'c5', 22, 1),
(117, 'C6', 0, 'SIM', 'c6', 22, 1),
(118, 'C8', 0, 'SIM', 'c8', 22, 1),
(119, 'EVASION', 0, 'SIM', 'evasion', 22, 1),
(120, 'GRAND PICASSO', 0, 'SIM', 'grand-picasso', 22, 1),
(121, 'XANTIA', 0, 'SIM', 'xantia', 22, 1),
(122, 'XM', 0, 'SIM', 'xm', 22, 1),
(123, 'CL-244', 0, 'SIM', 'cl244', 23, 1),
(124, 'CL-330', 0, 'SIM', 'cl330', 23, 1),
(125, 'LANOS', 0, 'SIM', 'lanos', 24, 1),
(126, 'LEGANZA', 0, 'SIM', 'leganza', 24, 1),
(127, 'NUBIRA', 0, 'SIM', 'nubira', 24, 1),
(128, 'DURANGO', 0, 'SIM', 'durango', 25, 1),
(129, 'DAKOTA', 0, 'SIM', 'dakota', 25, 1),
(130, 'JOURNEY', 0, 'SIM', 'journey', 25, 1),
(131, 'RAM', 0, 'SIM', 'ram', 25, 1),
(132, 'M-100', 0, 'SIM', 'm100', 26, 1),
(133, 'ULC', 0, 'SIM', 'ulc', 26, 1),
(134, '458', 0, 'SIM', '458', 27, 1),
(135, '575M', 0, 'SIM', '575m', 27, 1),
(136, '500', 0, 'SIM', '500', 28, 1),
(137, 'BRAVA', 0, 'SIM', 'brava', 28, 1),
(138, 'BRAVO', 0, 'SIM', 'bravo', 28, 1),
(139, 'DOBLO', 0, 'SIM', 'doblo', 28, 1),
(140, 'DOBLÒ ADVENTURE', 0, 'SIM', 'doblÒ-adventure', 28, 1),
(141, 'DOBLÒ CARGO', 0, 'SIM', 'doblÒ-cargo', 28, 1),
(142, 'DUCATO', 0, 'SIM', 'ducato', 28, 1),
(143, 'DUCATO CARGO', 0, 'SIM', 'ducato-cargo', 28, 1),
(144, 'DUCATO MULTI', 0, 'SIM', 'ducato-multi', 28, 1),
(145, 'DUCATO PASAGEIRO', 0, 'SIM', 'ducato-pasageiro', 28, 1),
(146, 'FIORINO', 0, 'SIM', 'fiorino', 28, 1),
(147, 'FREEMONT', 0, 'SIM', 'freemont', 28, 1),
(148, 'GRAND SIENA', 0, 'SIM', 'grand-siena', 28, 1),
(149, 'IDEA', 0, 'SIM', 'idea', 28, 1),
(150, 'LINEA', 0, 'SIM', 'linea', 28, 1),
(151, 'MAREA', 0, 'SIM', 'marea', 28, 1),
(152, 'PALIO', 0, 'SIM', 'palio', 28, 1),
(153, 'PALIO FIRE', 0, 'SIM', 'palio-fire', 28, 1),
(154, 'PALIO WEEKEND', 0, 'SIM', 'palio-weekend', 28, 1),
(155, 'PUNTO', 0, 'SIM', 'punto', 28, 1),
(156, 'SIENA', 0, 'SIM', 'siena', 28, 1),
(157, 'STILO', 0, 'SIM', 'stilo', 28, 1),
(158, 'STRADA', 0, 'SIM', 'strada', 28, 1),
(159, 'STRADA ADVENTURE', 0, 'SIM', 'strada-adventure', 28, 1),
(160, 'STRADA TREKKING', 0, 'SIM', 'strada-trekking', 28, 1),
(161, 'STRADA WORKING', 0, 'SIM', 'strada-working', 28, 1),
(162, 'NOVO UNO', 0, 'SIM', 'novo-uno', 28, 1),
(163, 'UNO FURGÃO', 0, 'SIM', 'uno-furgÃo', 28, 1),
(164, 'UNO MILLE', 0, 'SIM', 'uno-mille', 28, 1),
(165, 'UNO WAY', 0, 'SIM', 'uno-way', 28, 1),
(166, 'BUGGY', 0, 'SIM', 'buggy', 29, 1),
(167, 'COURIER', 0, 'SIM', 'courier', 30, 1),
(168, 'ECO SPORT 1.6', 0, 'SIM', 'eco-sport-16', 30, 1),
(170, 'ECO SPORT 2.0', 0, 'SIM', 'eco-sport-20', 30, 1),
(171, 'ESCORT', 0, 'SIM', 'escort', 30, 1),
(172, 'F-4000', 0, 'SIM', 'f4000', 30, 1),
(173, 'F-150', 0, 'SIM', 'f150', 30, 1),
(174, 'F-250', 0, 'SIM', 'f250', 30, 1),
(175, 'F-350', 0, 'SIM', 'f350', 30, 1),
(176, 'FIESTA', 0, 'SIM', 'fiesta', 30, 1),
(177, 'FOCUS HATCH', 0, 'SIM', 'focus-hatch', 30, 1),
(178, 'FOCUS SEDAN', 0, 'SIM', 'focus-sedan', 30, 1),
(179, 'FUSION', 0, 'SIM', 'fusion', 30, 1),
(180, 'NOVO FUSION', 0, 'SIM', 'novo-fusion', 30, 1),
(181, 'FUSION HYBRID', 0, 'SIM', 'fusion-hybrid', 30, 1),
(182, 'KA', 0, 'SIM', 'ka', 30, 1),
(183, 'NOVO KA', 0, 'SIM', 'novo-ka', 30, 1),
(184, 'NOVO KA+ 1.5', 0, 'SIM', 'novo-ka-15', 30, 1),
(185, 'NOVO KA+ 1.0', 0, 'SIM', 'novo-ka-10', 30, 1),
(186, 'MONDEO', 0, 'SIM', 'mondeo', 30, 1),
(187, 'MUSTANG', 0, 'SIM', 'mustang', 30, 1),
(188, 'NEW FIESTA SEDAN', 0, 'SIM', 'new-fiesta-sedan', 30, 1),
(189, 'NEW FIESTA HATCH', 0, 'SIM', 'new-fiesta-hatch', 30, 1),
(190, 'TRANSIT', 0, 'SIM', 'transit', 30, 1),
(191, 'AGILE', 0, 'SIM', 'agile', 31, 1),
(192, 'ASTRA', 0, 'SIM', 'astra', 31, 1),
(193, 'CAPTIVA V6', 0, 'SIM', 'captiva-v6', 31, 1),
(194, 'CAPTIVA 2.4', 0, 'SIM', 'captiva-24', 31, 1),
(195, 'CAMARO', 0, 'SIM', 'camaro', 31, 1),
(196, 'CELTA', 0, 'SIM', 'celta', 31, 1),
(197, 'CLASSIC', 0, 'SIM', 'classic', 31, 1),
(198, 'COBALT', 0, 'SIM', 'cobalt', 31, 1),
(199, 'CORSA', 0, 'SIM', 'corsa', 31, 1),
(200, 'CRUZE', 0, 'SIM', 'cruze', 31, 1),
(201, 'CRUZE SPORT 6', 0, 'SIM', 'cruze-sport-6', 31, 1),
(202, 'MALIBU', 0, 'SIM', 'malibu', 31, 1),
(203, 'MERIVA', 0, 'SIM', 'meriva', 31, 1),
(204, 'MONTANA', 0, 'SIM', 'montana', 31, 1),
(205, 'OMEGA', 0, 'SIM', 'omega', 31, 1),
(206, 'ONIX', 0, 'SIM', 'onix', 31, 1),
(207, 'PRISMA', 0, 'SIM', 'prisma', 31, 1),
(208, 'S10', 0, 'SIM', 's10', 31, 1),
(209, 'SILVERADO', 0, 'SIM', 'silverado', 31, 1),
(210, 'SONIC', 0, 'SIM', 'sonic', 31, 1),
(211, 'SPIN', 0, 'SIM', 'spin', 31, 1),
(212, 'SUBURBAN', 0, 'SIM', 'suburban', 31, 1),
(213, 'TRACKER', 0, 'SIM', 'tracker', 31, 1),
(214, 'TRAILBLAIZER', 0, 'SIM', 'trailblaizer', 31, 1),
(215, 'VECTRA', 0, 'SIM', 'vectra', 31, 1),
(216, 'ZAFIRA', 0, 'SIM', 'zafira', 31, 1),
(217, 'HOVER', 0, 'SIM', 'hover', 32, 1),
(218, 'TOWNER', 0, 'SIM', 'towner', 33, 1),
(219, 'ACCORD 4 CILINDRO', 0, 'SIM', 'accord-4-cilindro', 34, 1),
(220, 'ACCORD 6 CILINDRO', 0, 'SIM', 'accord-6-cilindro', 34, 1),
(221, 'CITY', 0, 'SIM', 'city', 34, 1),
(222, 'NOVO CITY', 0, 'SIM', 'novo-city', 34, 1),
(223, 'CITY SEDAN', 0, 'SIM', 'city-sedan', 34, 1),
(224, 'CIVIC', 0, 'SIM', 'civic', 34, 1),
(225, 'HR-V', 0, 'SIM', 'hrv', 34, 1),
(226, 'NEW CIVIC', 0, 'SIM', 'new-civic', 34, 1),
(227, 'NOVO CIVIC', 0, 'SIM', 'novo-civic', 34, 1),
(228, 'CR-V', 0, 'SIM', 'crv', 34, 1),
(229, 'FIT', 0, 'SIM', 'fit', 34, 1),
(230, 'NOVO FIT', 0, 'SIM', 'novo-fit', 34, 1),
(231, 'ODYSSEY', 0, 'SIM', 'odyssey', 34, 1),
(232, 'PASSPORT', 0, 'SIM', 'passport', 34, 1),
(233, 'PRELUDE', 0, 'SIM', 'prelude', 34, 1),
(234, 'ACCENT', 0, 'SIM', 'accent', 35, 1),
(235, 'ATOS', 0, 'SIM', 'atos', 35, 1),
(236, 'AZERA', 0, 'SIM', 'azera', 35, 1),
(237, 'ELANTRA', 0, 'SIM', 'elantra', 35, 1),
(238, 'GENESIS', 0, 'SIM', 'genesis', 35, 1),
(239, 'H100', 0, 'SIM', 'h100', 35, 1),
(240, 'HB20', 0, 'SIM', 'hb20', 35, 1),
(241, 'HB20X', 0, 'SIM', 'hb20x', 35, 1),
(242, 'HB20S', 0, 'SIM', 'hb20s', 35, 1),
(243, 'HR', 0, 'SIM', 'hr', 35, 1),
(244, 'I30', 0, 'SIM', 'i30', 35, 1),
(245, 'IX35', 0, 'SIM', 'ix35', 35, 1),
(246, 'MATRIX', 0, 'SIM', 'matrix', 35, 1),
(247, 'PORTER', 0, 'SIM', 'porter', 35, 1),
(248, 'SANTA FÉ', 0, 'SIM', 'santa-fÉ', 35, 1),
(249, 'SONATA', 0, 'SIM', 'sonata', 35, 1),
(250, 'TERRACAN', 0, 'SIM', 'terracan', 35, 1),
(251, 'TRAJET', 0, 'SIM', 'trajet', 35, 1),
(252, 'TUCSON', 0, 'SIM', 'tucson', 35, 1),
(253, 'VELOSTER', 0, 'SIM', 'veloster', 35, 1),
(254, 'VERACRUZ', 0, 'SIM', 'veracruz', 35, 1),
(255, 'RODEO', 0, 'SIM', 'rodeo', 36, 1),
(256, 'J2', 0, 'SIM', 'j2', 37, 1),
(257, 'J3', 0, 'SIM', 'j3', 37, 1),
(258, 'J5', 0, 'SIM', 'j5', 37, 1),
(259, 'J6', 0, 'SIM', 'j6', 37, 1),
(260, 'T8', 0, 'SIM', 't8', 37, 1),
(261, 'T140', 0, 'SIM', 't140', 37, 1),
(262, 'DAIMLER 4.0', 0, 'SIM', 'daimler-40', 38, 1),
(263, 'S- TYPE', 0, 'SIM', 's-type', 38, 1),
(264, 'XF', 0, 'SIM', 'xf', 38, 1),
(265, 'XFR', 0, 'SIM', 'xfr', 38, 1),
(266, 'XJ', 0, 'SIM', 'xj', 38, 1),
(267, 'XJR', 0, 'SIM', 'xjr', 38, 1),
(268, 'XK-8', 0, 'SIM', 'xk8', 38, 1),
(269, 'XKR', 0, 'SIM', 'xkr', 38, 1),
(270, 'X-TYPE', 0, 'SIM', 'xtype', 38, 1),
(271, 'CHEROKEE', 0, 'SIM', 'cherokee', 39, 1),
(272, 'COMMANDER', 0, 'SIM', 'commander', 39, 1),
(273, 'RENEGADE', 0, 'SIM', 'renegade', 39, 1),
(274, 'WRANGLER', 0, 'SIM', 'wrangler', 39, 1),
(275, 'TOPIC', 0, 'SIM', 'topic', 40, 1),
(276, 'JIPE', 0, 'SIM', 'jipe', 41, 1),
(277, 'BONGO K-2400', 0, 'SIM', 'bongo-k2400', 42, 1),
(278, 'CADENZA', 0, 'SIM', 'cadenza', 42, 1),
(279, 'CARENS', 0, 'SIM', 'carens', 42, 1),
(280, 'CARNIVAL', 0, 'SIM', 'carnival', 42, 1),
(281, 'CERATO', 0, 'SIM', 'cerato', 42, 1),
(282, 'NEW CERATO', 0, 'SIM', 'new-cerato', 42, 1),
(283, 'CLARUS', 0, 'SIM', 'clarus', 42, 1),
(284, 'MAGENTIS', 0, 'SIM', 'magentis', 42, 1),
(285, 'MOHAVE', 0, 'SIM', 'mohave', 42, 1),
(286, 'OPIRUS', 0, 'SIM', 'opirus', 42, 1),
(287, 'OPTIMA', 0, 'SIM', 'optima', 42, 1),
(288, 'SORENTO', 0, 'SIM', 'sorento', 42, 1),
(289, 'SORENTO DIESEL', 0, 'SIM', 'sorento-diesel', 42, 1),
(290, 'SOUL', 0, 'SIM', 'soul', 42, 1),
(291, 'SPORTAGE', 0, 'SIM', 'sportage', 42, 1),
(292, 'SPORTAGE DIESEL', 0, 'SIM', 'sportage-diesel', 42, 1),
(293, 'DISCOVERY', 0, 'SIM', 'discovery', 43, 1),
(294, 'DISCOVERY 3', 0, 'SIM', 'discovery-3', 43, 1),
(295, 'RANGE DISCOVERY SPORT', 0, 'SIM', 'range-discovery-sport', 43, 1),
(296, 'FREELANDER', 0, 'SIM', 'freelander', 43, 1),
(297, 'FREELANDER 2', 0, 'SIM', 'freelander-2', 43, 1),
(298, 'RANGE ROVER EVOQUE', 0, 'SIM', 'range-rover-evoque', 43, 1),
(299, 'RANGE ROVER VOQUE', 0, 'SIM', 'range-rover-voque', 43, 1),
(300, 'RANGE ROVER SPORT', 0, 'SIM', 'range-rover-sport', 43, 1),
(301, 'RANGER ROVER', 0, 'SIM', 'ranger-rover', 43, 1),
(302, 'ES-300', 0, 'SIM', 'es300', 44, 1),
(303, 'ES-330', 0, 'SIM', 'es330', 44, 1),
(304, 'ES-350', 0, 'SIM', 'es350', 44, 1),
(305, 'GS-300', 0, 'SIM', 'gs300', 44, 1),
(306, 'LS-400', 0, 'SIM', 'ls400', 44, 1),
(307, 'LS-430', 0, 'SIM', 'ls430', 44, 1),
(308, 'LS-460', 0, 'SIM', 'ls460', 44, 1),
(309, 'RX-300', 0, 'SIM', 'rx300', 44, 1),
(310, 'SC 400', 0, 'SIM', 'sc-400', 44, 1),
(311, 'H1', 0, 'SIM', 'h1', 45, 1),
(312, 'SCORPIO', 0, 'SIM', 'scorpio', 46, 1),
(313, 'GRANCABRIO', 0, 'SIM', 'grancabrio', 47, 1),
(314, 'PICK-UP', 0, 'SIM', 'pickup', 48, 1),
(315, '626', 0, 'SIM', '626', 49, 1),
(316, 'B-2500 PICK-UP', 0, 'SIM', 'b2500-pickup', 49, 1),
(317, 'MILLENIA', 0, 'SIM', 'millenia', 49, 1),
(318, 'MV', 0, 'SIM', 'mv', 49, 1),
(319, 'MX-5', 0, 'SIM', 'mx5', 49, 1),
(320, 'PROTEGÉ', 0, 'SIM', 'protegÉ', 49, 1),
(321, 'C-180', 0, 'SIM', 'c180', 50, 1),
(322, 'TODA A LINHA', 0, 'SIM', 'toda-a-linha', 50, 1),
(323, 'C-230', 0, 'SIM', 'c230', 50, 1),
(324, 'C-240', 0, 'SIM', 'c240', 50, 1),
(325, 'C-280', 0, 'SIM', 'c280', 50, 1),
(326, 'C-320', 0, 'SIM', 'c320', 50, 1),
(327, 'C-43', 0, 'SIM', 'c43', 50, 1),
(328, 'C-55', 0, 'SIM', 'c55', 50, 1),
(329, 'C-63', 0, 'SIM', 'c63', 50, 1),
(330, 'CL-500', 0, 'SIM', 'cl500', 50, 1),
(331, 'CL-600', 0, 'SIM', 'cl600', 50, 1),
(332, 'CL-63', 0, 'SIM', 'cl63', 50, 1),
(333, 'CL-65', 0, 'SIM', 'cl65', 50, 1),
(334, 'CLASSE A', 0, 'SIM', 'classe-a', 50, 1),
(335, 'CLASSE B', 0, 'SIM', 'classe-b', 50, 1),
(336, 'CLASSE C', 0, 'SIM', 'classe-c', 50, 1),
(337, 'CLA', 0, 'SIM', 'cla', 50, 1),
(338, 'CLASSE E', 0, 'SIM', 'classe-e', 50, 1),
(339, 'CLASSE M', 0, 'SIM', 'classe-m', 50, 1),
(340, 'CLASSE S', 0, 'SIM', 'classe-s', 50, 1),
(341, 'SL', 0, 'SIM', 'sl', 50, 1),
(342, 'CLASSE R', 0, 'SIM', 'classe-r', 50, 1),
(343, 'CLK-230', 0, 'SIM', 'clk230', 50, 1),
(344, 'CLK-320', 0, 'SIM', 'clk320', 50, 1),
(345, 'CLK-350', 0, 'SIM', 'clk350', 50, 1),
(346, 'CLK-430', 0, 'SIM', 'clk430', 50, 1),
(347, 'CLK-500', 0, 'SIM', 'clk500', 50, 1),
(348, 'CLK-55', 0, 'SIM', 'clk55', 50, 1),
(349, 'CLS-500', 0, 'SIM', 'cls500', 50, 1),
(350, 'CLS-55', 0, 'SIM', 'cls55', 50, 1),
(351, 'CLS-63', 0, 'SIM', 'cls63', 50, 1),
(352, 'E-240', 0, 'SIM', 'e240', 50, 1),
(353, 'E-250', 0, 'SIM', 'e250', 50, 1),
(354, 'E-280', 0, 'SIM', 'e280', 50, 1),
(355, 'E-320', 0, 'SIM', 'e320', 50, 1),
(356, 'E-350', 0, 'SIM', 'e350', 50, 1),
(357, 'E-430', 0, 'SIM', 'e430', 50, 1),
(358, 'E-500', 0, 'SIM', 'e500', 50, 1),
(359, 'E-55', 0, 'SIM', 'e55', 50, 1),
(360, 'E-63', 0, 'SIM', 'e63', 50, 1),
(361, 'G-55', 0, 'SIM', 'g55', 50, 1),
(362, 'GL-500', 0, 'SIM', 'gl500', 50, 1),
(363, 'ML-320', 0, 'SIM', 'ml320', 50, 1),
(364, 'ML-350', 0, 'SIM', 'ml350', 50, 1),
(365, 'ML-430', 0, 'SIM', 'ml430', 50, 1),
(366, 'ML-500', 0, 'SIM', 'ml500', 50, 1),
(367, 'ML-55', 0, 'SIM', 'ml55', 50, 1),
(368, 'ML-63', 0, 'SIM', 'ml63', 50, 1),
(369, 'S-500', 0, 'SIM', 's500', 50, 1),
(370, 'S-55', 0, 'SIM', 's55', 50, 1),
(371, 'S-600', 0, 'SIM', 's600', 50, 1),
(372, 'S-63', 0, 'SIM', 's63', 50, 1),
(373, 'S-65', 0, 'SIM', 's65', 50, 1),
(374, 'SL-350', 0, 'SIM', 'sl350', 50, 1),
(375, 'SL-55', 0, 'SIM', 'sl55', 50, 1),
(376, 'SLS-55', 0, 'SIM', 'sls55', 50, 1),
(377, 'SLS-63', 0, 'SIM', 'sls63', 50, 1),
(378, 'SPRINTER', 0, 'SIM', 'sprinter', 50, 1),
(379, '550', 0, 'SIM', '550', 51, 1),
(380, 'MG6', 0, 'SIM', 'mg6', 51, 1),
(381, 'COOPER', 0, 'SIM', 'cooper', 52, 1),
(382, 'ONE', 0, 'SIM', 'one', 52, 1),
(383, 'AIRTREK', 0, 'SIM', 'airtrek', 53, 1),
(384, 'ASX', 0, 'SIM', 'asx', 53, 1),
(385, 'ECLIPSE', 0, 'SIM', 'eclipse', 53, 1),
(386, 'GALANT', 0, 'SIM', 'galant', 53, 1),
(387, 'GRANDIS', 0, 'SIM', 'grandis', 53, 1),
(388, 'L200', 0, 'SIM', 'l200', 53, 1),
(389, 'L200 OUTDOOR', 0, 'SIM', 'l200-outdoor', 53, 1),
(390, 'L200 TRITON', 0, 'SIM', 'l200-triton', 53, 1),
(391, 'L300', 0, 'SIM', 'l300', 53, 1),
(392, 'LANCER', 0, 'SIM', 'lancer', 53, 1),
(393, 'LANCER EVOLUTION', 0, 'SIM', 'lancer-evolution', 53, 1),
(394, 'MIRAGE', 0, 'SIM', 'mirage', 53, 1),
(395, 'MONTERO', 0, 'SIM', 'montero', 53, 1),
(396, 'PAJERO DAKAR', 0, 'SIM', 'pajero-dakar', 53, 1),
(397, 'PAJERO FULL', 0, 'SIM', 'pajero-full', 53, 1),
(398, 'PAJERO TR4', 0, 'SIM', 'pajero-tr4', 53, 1),
(399, 'SPACE', 0, 'SIM', 'space', 53, 1),
(400, 'ALTIMA', 0, 'SIM', 'altima', 54, 1),
(401, 'FRONTIER', 0, 'SIM', 'frontier', 54, 1),
(402, 'GRAND LIVINA', 0, 'SIM', 'grand-livina', 54, 1),
(403, 'MARCH', 0, 'SIM', 'march', 54, 1),
(404, 'MAXIMA', 0, 'SIM', 'maxima', 54, 1),
(405, 'MURANO', 0, 'SIM', 'murano', 54, 1),
(406, 'PATHFINDER', 0, 'SIM', 'pathfinder', 54, 1),
(407, 'PRIMERA', 0, 'SIM', 'primera', 54, 1),
(408, 'QUEST', 0, 'SIM', 'quest', 54, 1),
(409, 'SENTRA', 0, 'SIM', 'sentra', 54, 1),
(410, 'TIIDA', 0, 'SIM', 'tiida', 54, 1),
(411, 'VERSA', 0, 'SIM', 'versa', 54, 1),
(412, 'X -GEAR', 0, 'SIM', 'x-gear', 54, 1),
(413, 'X - TERRA', 0, 'SIM', 'x--terra', 54, 1),
(414, '106', 0, 'SIM', '106', 55, 1),
(415, '206', 0, 'SIM', '206', 55, 1),
(416, '207', 0, 'SIM', '207', 55, 1),
(417, '207 SEDAN', 0, 'SIM', '207-sedan', 55, 1),
(418, '208', 0, 'SIM', '208', 55, 1),
(419, '306', 0, 'SIM', '306', 55, 1),
(420, '307', 0, 'SIM', '307', 55, 1),
(421, '308', 0, 'SIM', '308', 55, 1),
(422, '308 SEDAN', 0, 'SIM', '308-sedan', 55, 1),
(423, '308 cc', 0, 'SIM', '308-cc', 55, 1),
(424, '406', 0, 'SIM', '406', 55, 1),
(425, '407', 0, 'SIM', '407', 55, 1),
(426, '408', 0, 'SIM', '408', 55, 1),
(427, '408 TURBO', 0, 'SIM', '408-turbo', 55, 1),
(428, '504', 0, 'SIM', '504', 55, 1),
(429, '508', 0, 'SIM', '508', 55, 1),
(430, '605', 0, 'SIM', '605', 55, 1),
(431, '806', 0, 'SIM', '806', 55, 1),
(432, '2008', 0, 'SIM', '2008', 55, 1),
(433, '3008', 0, 'SIM', '3008', 55, 1),
(434, 'RCZ', 0, 'SIM', 'rcz', 55, 1),
(435, '607 SEDAN', 0, 'SIM', '607-sedan', 55, 1),
(436, 'BOXER', 0, 'SIM', 'boxer', 55, 1),
(437, 'HOGGAR', 0, 'SIM', 'hoggar', 55, 1),
(438, 'PARTNER', 0, 'SIM', 'partner', 55, 1),
(439, '911', 0, 'SIM', '911', 56, 1),
(440, '918 SPYDER', 0, 'SIM', '918-spyder', 56, 1),
(441, 'CAYENNE', 0, 'SIM', 'cayenne', 56, 1),
(442, 'MACAN', 0, 'SIM', 'macan', 56, 1),
(443, 'PANAMERA', 0, 'SIM', 'panamera', 56, 1),
(444, 'DUSTER', 0, 'SIM', 'duster', 57, 1),
(445, 'DUSTER OROCH', 0, 'SIM', 'duster-oroch', 57, 1),
(446, 'EXPRESS', 0, 'SIM', 'express', 57, 1),
(447, 'FLUENCE', 0, 'SIM', 'fluence', 57, 1),
(448, 'FLUENCE GT', 0, 'SIM', 'fluence-gt', 57, 1),
(449, 'KANGOO', 0, 'SIM', 'kangoo', 57, 1),
(450, 'LAGUNA', 0, 'SIM', 'laguna', 57, 1),
(451, 'LOGAN', 0, 'SIM', 'logan', 57, 1),
(452, 'MASTER', 0, 'SIM', 'master', 57, 1),
(453, 'MEGANE', 0, 'SIM', 'megane', 57, 1),
(454, 'SANDERO', 0, 'SIM', 'sandero', 57, 1),
(455, 'SANDEIRO RS', 0, 'SIM', 'sandeiro-rs', 57, 1),
(456, 'SANDEIRO STETWAY', 0, 'SIM', 'sandeiro-stetway', 57, 1),
(457, 'SCÉNIC', 0, 'SIM', 'scÉnic', 57, 1),
(458, 'SYMBOL', 0, 'SIM', 'symbol', 57, 1),
(459, 'TRAFIC', 0, 'SIM', 'trafic', 57, 1),
(460, 'TWINGO', 0, 'SIM', 'twingo', 57, 1),
(461, 'CORDOBA', 0, 'SIM', 'cordoba', 58, 1),
(462, 'IBIZA', 0, 'SIM', 'ibiza', 58, 1),
(463, 'INCA', 0, 'SIM', 'inca', 58, 1),
(464, 'ACTYON', 0, 'SIM', 'actyon', 59, 1),
(465, 'CHAIRMAN', 0, 'SIM', 'chairman', 59, 1),
(466, 'ISTANA', 0, 'SIM', 'istana', 59, 1),
(467, 'KORANDO', 0, 'SIM', 'korando', 59, 1),
(468, 'KYRON', 0, 'SIM', 'kyron', 59, 1),
(469, 'MUSSO', 0, 'SIM', 'musso', 59, 1),
(470, 'REXTON', 0, 'SIM', 'rexton', 59, 1),
(471, 'IMPREZA', 0, 'SIM', 'impreza', 60, 1),
(472, 'OUTBACK', 0, 'SIM', 'outback', 60, 1),
(473, 'BALENO', 0, 'SIM', 'baleno', 61, 1),
(474, 'GRAND VITARA', 0, 'SIM', 'grand-vitara', 61, 1),
(475, 'IGNIS', 0, 'SIM', 'ignis', 61, 1),
(476, 'SWIFT SPORT', 0, 'SIM', 'swift-sport', 61, 1),
(477, 'SX4', 0, 'SIM', 'sx4', 61, 1),
(478, 'S-CROSS', 0, 'SIM', 'scross', 61, 1),
(479, 'VITARA', 0, 'SIM', 'vitara', 61, 1),
(480, 'STARK', 0, 'SIM', 'stark', 62, 1),
(481, 'AVOLON', 0, 'SIM', 'avolon', 63, 1),
(482, 'CAMRY', 0, 'SIM', 'camry', 63, 1),
(483, 'CELICA', 0, 'SIM', 'celica', 63, 1),
(484, 'COROLLA', 0, 'SIM', 'corolla', 63, 1),
(485, 'ETIOS', 0, 'SIM', 'etios', 63, 1),
(486, 'ETIOS SEDAN', 0, 'SIM', 'etios-sedan', 63, 1),
(487, 'ETIOS SPORT', 0, 'SIM', 'etios-sport', 63, 1),
(488, 'HILUX', 0, 'SIM', 'hilux', 63, 1),
(489, 'LAND', 0, 'SIM', 'land', 63, 1),
(490, 'PRIUS', 0, 'SIM', 'prius', 63, 1),
(491, 'RAV4', 0, 'SIM', 'rav4', 63, 1),
(492, 'SW4', 0, 'SIM', 'sw4', 63, 1),
(493, 'PANTANAL', 0, 'SIM', 'pantanal', 64, 1),
(494, 'RF', 0, 'SIM', 'rf', 64, 1),
(495, 'T-4', 0, 'SIM', 't4', 64, 1),
(496, 'C30', 0, 'SIM', 'c30', 65, 1),
(497, 'C70', 0, 'SIM', 'c70', 65, 1),
(498, 'S40', 0, 'SIM', 's40', 65, 1),
(499, 'S60', 0, 'SIM', 's60', 65, 1),
(500, 'S70', 0, 'SIM', 's70', 65, 1),
(501, 'S80', 0, 'SIM', 's80', 65, 1),
(502, 'V40', 0, 'SIM', 'v40', 65, 1),
(503, 'V50', 0, 'SIM', 'v50', 65, 1),
(504, 'V60', 0, 'SIM', 'v60', 65, 1),
(505, 'V70', 0, 'SIM', 'v70', 65, 1),
(506, 'NOVO XC 90', 0, 'SIM', 'novo-xc-90', 65, 1),
(507, 'XC 90 OPCIONAL', 0, 'SIM', 'xc-90-opcional', 65, 1),
(508, 'WAY', 0, 'SIM', 'way', 67, 1),
(509, 'BUGGY', 0, 'SIM', 'buggy', 68, 1),
(510, 'AMAROK DIESEL', 0, 'SIM', 'amarok-diesel', 66, 1),
(511, 'AMAROK GASOLINA', 0, 'SIM', 'amarok-gasolina', 66, 1),
(512, 'APOLLO', 0, 'SIM', 'apollo', 66, 1),
(513, 'BORA', 0, 'SIM', 'bora', 66, 1),
(514, 'CARAVELLE', 0, 'SIM', 'caravelle', 66, 1),
(515, 'CROSSFOX', 0, 'SIM', 'crossfox', 66, 1),
(516, 'EUROVAN', 0, 'SIM', 'eurovan', 66, 1),
(517, 'FOX', 0, 'SIM', 'fox', 66, 1),
(518, 'GOL', 0, 'SIM', 'gol', 66, 1),
(519, 'NOVO GOL', 0, 'SIM', 'novo-gol', 66, 1),
(520, 'GOLF', 0, 'SIM', 'golf', 66, 1),
(521, 'GOLF BLACK', 0, 'SIM', 'golf-black', 66, 1),
(522, 'GRAND', 0, 'SIM', 'grand', 66, 1),
(523, 'JETTA', 0, 'SIM', 'jetta', 66, 1),
(524, 'KOMBI', 0, 'SIM', 'kombi', 66, 1),
(525, 'KOMBI PICK-UP', 0, 'SIM', 'kombi-pickup', 66, 1),
(526, 'NEW FUSCA', 0, 'SIM', 'new-fusca', 66, 1),
(527, 'PARATI', 0, 'SIM', 'parati', 66, 1),
(528, 'PASSAT', 0, 'SIM', 'passat', 66, 1),
(529, 'POLO', 0, 'SIM', 'polo', 66, 1),
(530, 'QUANTUM', 0, 'SIM', 'quantum', 66, 1),
(531, 'SANTANA', 0, 'SIM', 'santana', 66, 1),
(532, 'SAVEIRO', 0, 'SIM', 'saveiro', 66, 1),
(533, 'SPACECROSS', 0, 'SIM', 'spacecross', 66, 1),
(534, 'SPACEFOX', 0, 'SIM', 'spacefox', 66, 1),
(535, 'TIGUAN 2.0', 0, 'SIM', 'tiguan-20', 66, 1),
(536, 'TOUAREG', 0, 'SIM', 'touareg', 66, 1),
(537, 'UP!', 0, 'SIM', 'up', 66, 1),
(538, 'VAN 1.6', 0, 'SIM', 'van-16', 66, 1),
(539, 'VOYAGE', 0, 'SIM', 'voyage', 66, 1),
(540, 'F 800 GS Adventure', 0, 'SIM', 'f-800-gs-adventure', 110, 2),
(541, 'C 600', 0, 'SIM', 'c-600', 110, 2),
(542, 'C 650 GT', 0, 'SIM', 'c-650-gt', 110, 2),
(543, 'F 800 GS', 0, 'SIM', 'f-800-gs', 110, 2),
(544, 'F 800 R', 0, 'SIM', 'f-800-r', 110, 2),
(545, 'F 800 ST', 0, 'SIM', 'f-800-st', 110, 2),
(546, 'G 650 GS', 0, 'SIM', 'g-650-gs', 110, 2),
(547, 'K 1300 R', 0, 'SIM', 'k-1300-r', 110, 2),
(548, 'K 1300 S', 0, 'SIM', 'k-1300-s', 110, 2),
(549, 'R 1200 GS', 0, 'SIM', 'r-1200-gs', 110, 2),
(550, 'R 1200 GS ADVENTURE', 0, 'SIM', 'r-1200-gs-adventure', 110, 2),
(551, 'R NINE T', 0, 'SIM', 'r-nine-t', 110, 2),
(552, 'S 1000 R', 0, 'SIM', 's-1000-r', 110, 2),
(553, 'S 1000 RR', 0, 'SIM', 's-1000-rr', 110, 2),
(554, 'APACHE 150', 0, 'SIM', 'apache-150', 111, 2),
(555, 'LASER 150', 0, 'SIM', 'laser-150', 111, 2),
(556, 'DIAVEL', 0, 'SIM', 'diavel', 112, 2),
(557, 'DIAVEL CARBON', 0, 'SIM', 'diavel-carbon', 112, 2),
(558, 'MONSTER 696 ABS', 0, 'SIM', 'monster-696-abs', 112, 2),
(559, 'MONSTER 796', 0, 'SIM', 'monster-796', 112, 2),
(560, 'MONSTER 1100', 0, 'SIM', 'monster-1100', 112, 2),
(561, 'MONSTER 1200', 0, 'SIM', 'monster-1200', 112, 2),
(562, 'MONTER 1200 S', 0, 'SIM', 'monter-1200-s', 112, 2),
(563, 'HYPERMOTARD', 0, 'SIM', 'hypermotard', 112, 2),
(564, 'HYPERSTRADA', 0, 'SIM', 'hyperstrada', 112, 2),
(565, 'MULTISTRADA 1200', 0, 'SIM', 'multistrada-1200', 112, 2),
(566, 'MULTISTRADA 1200 S TOURING', 0, 'SIM', 'multistrada-1200-s-touring', 112, 2),
(567, 'MULTISTRADA 1200 S PIKES PEAK', 0, 'SIM', 'multistrada-1200-s-pikes-peak', 112, 2),
(568, 'SUPERBIKE 1199 PANIGALE / S / R', 0, 'SIM', 'superbike-1199-panigale--s--r', 112, 2),
(569, 'DYNA S GLIDE 1600', 0, 'SIM', 'dyna-s-glide-1600', 113, 2),
(570, '883R', 0, 'SIM', '883r', 113, 2),
(571, 'IRON 883', 0, 'SIM', 'iron-883', 113, 2),
(572, '1200 CUSTOM', 0, 'SIM', '1200-custom', 113, 2),
(573, 'Forty-Eight', 0, 'SIM', 'fortyeight', 113, 2),
(574, 'NIGHT ROD SPECIAL', 0, 'SIM', 'night-rod-special', 113, 2),
(575, 'V-ROD MUSCLE', 0, 'SIM', 'vrod-muscle', 113, 2),
(576, 'BLACKLINE', 0, 'SIM', 'blackline', 113, 2),
(577, 'DYNA SUPER GLIDE CUSTOM', 0, 'SIM', 'dyna-super-glide-custom', 113, 2),
(578, 'FAT BOB', 0, 'SIM', 'fat-bob', 113, 2),
(579, 'STREET BOB', 0, 'SIM', 'street-bob', 113, 2),
(580, 'LOW RIDER', 0, 'SIM', 'low-rider', 113, 2),
(581, 'BREAKOUT', 0, 'SIM', 'breakout', 113, 2),
(582, 'FAT BOY', 0, 'SIM', 'fat-boy', 113, 2),
(583, 'FAT BOY SPECIAL', 0, 'SIM', 'fat-boy-special', 113, 2),
(584, 'HERITAGE SOFTAIL CLASSIC', 0, 'SIM', 'heritage-softail-classic', 113, 2),
(585, 'SOFTAIL DELUXE', 0, 'SIM', 'softail-deluxe', 113, 2),
(586, 'SWITCHBACK', 0, 'SIM', 'switchback', 113, 2),
(587, 'ROAD KING CLASSIC', 0, 'SIM', 'road-king-classic', 113, 2),
(588, 'ULTRA LIMITED', 0, 'SIM', 'ultra-limited', 113, 2),
(589, 'STREET GLIDE SPECIAL', 0, 'SIM', 'street-glide-special', 113, 2),
(590, 'STREET GLIDE', 0, 'SIM', 'street-glide', 113, 2),
(591, 'XL SPORTER', 0, 'SIM', 'xl-sporter', 113, 2),
(592, 'XLH SPORTER', 0, 'SIM', 'xlh-sporter', 113, 2),
(593, 'FAT BOY SPECIAL 1600', 0, 'SIM', 'fat-boy-special-1600', 113, 2),
(594, 'BIZ 100', 0, 'SIM', 'biz-100', 114, 2),
(595, 'BIZ 100 ES', 0, 'SIM', 'biz-100-es', 114, 2),
(596, '600 SHADOW', 0, 'SIM', '600-shadow', 114, 2),
(597, 'BIZ 125 +', 0, 'SIM', 'biz-125-', 114, 2),
(598, 'BIZ 125 ES', 0, 'SIM', 'biz-125-es', 114, 2),
(599, 'BIZ 125 FLEX', 0, 'SIM', 'biz-125-flex', 114, 2),
(600, 'BIZ 125 KS', 0, 'SIM', 'biz-125-ks', 114, 2),
(601, 'CB 1000R', 0, 'SIM', 'cb-1000r', 114, 2),
(602, 'CB 300R', 0, 'SIM', 'cb-300r', 114, 2),
(603, 'CB 300R FLEX', 0, 'SIM', 'cb-300r-flex', 114, 2),
(604, 'CB 500', 0, 'SIM', 'cb-500', 114, 2),
(605, 'CB 500F', 0, 'SIM', 'cb-500f', 114, 2),
(606, 'CB 500X', 0, 'SIM', 'cb-500x', 114, 2),
(607, 'CB 600F HORNET', 0, 'SIM', 'cb-600f-hornet', 114, 2),
(608, 'CB 600F HORNET', 0, 'SIM', 'cb-600f-hornet', 114, 2),
(609, 'CB 650F', 0, 'SIM', 'cb-650f', 114, 2),
(610, 'CBR 1000 RA (ABS)', 0, 'SIM', 'cbr-1000-ra-abs', 114, 2),
(611, 'CBR 1000 RR C/ ABS', 0, 'SIM', 'cbr-1000-rr-c-abs', 114, 2),
(612, 'CBR 1000 RR S/ ABS', 0, 'SIM', 'cbr-1000-rr-s-abs', 114, 2),
(613, 'CBR 500R', 0, 'SIM', 'cbr-500r', 114, 2),
(614, 'CBR 600 RR', 0, 'SIM', 'cbr-600-rr', 114, 2),
(615, 'CBR 650F', 0, 'SIM', 'cbr-650f', 114, 2),
(616, 'CBX 250 TWISTER', 0, 'SIM', 'cbx-250-twister', 114, 2),
(617, 'CG 125 CARGO ES', 0, 'SIM', 'cg-125-cargo-es', 114, 2),
(618, 'CG 125 CARGO KS /ES', 0, 'SIM', 'cg-125-cargo-ks-es', 114, 2),
(619, 'CG 125 TITAN ES', 0, 'SIM', 'cg-125-titan-es', 114, 2),
(620, 'CG 125 TITAN KS', 0, 'SIM', 'cg-125-titan-ks', 114, 2),
(621, 'CG 150 Fan', 0, 'SIM', 'cg-150-fan', 114, 2),
(622, 'CG 150 GARGO', 0, 'SIM', 'cg-150-gargo', 114, 2),
(623, 'CG 150 JOB', 0, 'SIM', 'cg-150-job', 114, 2),
(624, 'CG 150 SPORT', 0, 'SIM', 'cg-150-sport', 114, 2),
(625, 'CG 150 TITAN KS', 0, 'SIM', 'cg-150-titan-ks', 114, 2),
(626, 'CG 150 TITAN CARGO', 0, 'SIM', 'cg-150-titan-cargo', 114, 2),
(627, 'CG 150 TITAN ES', 0, 'SIM', 'cg-150-titan-es', 114, 2),
(628, 'CG 150 TITAN ESD', 0, 'SIM', 'cg-150-titan-esd', 114, 2),
(629, 'CG 150 TITAN ESX', 0, 'SIM', 'cg-150-titan-esx', 114, 2),
(630, 'CG 150 TITAN MIX', 0, 'SIM', 'cg-150-titan-mix', 114, 2),
(631, 'CG 160 Fan', 0, 'SIM', 'cg-160-fan', 114, 2),
(632, 'CG 160 TITAN', 0, 'SIM', 'cg-160-titan', 114, 2),
(633, 'CG125 FAN ES', 0, 'SIM', 'cg125-fan-es', 114, 2),
(634, 'CG125 FAN KS', 0, 'SIM', 'cg125-fan-ks', 114, 2),
(635, 'CRF 110 F', 0, 'SIM', 'crf-110-f', 114, 2),
(636, 'CRF 150 F', 0, 'SIM', 'crf-150-f', 114, 2),
(637, 'CRF 230 F', 0, 'SIM', 'crf-230-f', 114, 2),
(638, 'CRF 250L', 0, 'SIM', 'crf-250l', 114, 2),
(639, 'GL 1800 GOLD WING', 0, 'SIM', 'gl-1800-gold-wing', 114, 2),
(640, 'LEAD 110', 0, 'SIM', 'lead-110', 114, 2),
(641, 'NX 200', 0, 'SIM', 'nx-200', 114, 2),
(642, 'NX 400i FALCON', 0, 'SIM', 'nx-400i-falcon', 114, 2),
(643, 'NXR 125 BROS KS / ES', 0, 'SIM', 'nxr-125-bros-ks--es', 114, 2),
(644, 'NXR 150 BROS ESD', 0, 'SIM', 'nxr-150-bros-esd', 114, 2),
(645, 'NXR 150 BROS KS', 0, 'SIM', 'nxr-150-bros-ks', 114, 2),
(646, 'NXR 160 BROS', 0, 'SIM', 'nxr-160-bros', 114, 2),
(647, 'POP 100', 0, 'SIM', 'pop-100', 114, 2),
(648, 'POP 110i', 0, 'SIM', 'pop-110i', 114, 2),
(649, 'TRX 350 FourTrax', 0, 'SIM', 'trx-350-fourtrax', 114, 2),
(650, 'TRX 420 FourTrax', 0, 'SIM', 'trx-420-fourtrax', 114, 2),
(651, 'TRX 450 FourTrax', 0, 'SIM', 'trx-450-fourtrax', 114, 2),
(652, 'TRX 700 FourTrax', 0, 'SIM', 'trx-700-fourtrax', 114, 2),
(653, 'VT 750 C/CA/CD SHADOW', 0, 'SIM', 'vt-750-ccacd-shadow', 114, 2),
(654, 'VTX 1800C/F/N/R', 0, 'SIM', 'vtx-1800cfnr', 114, 2),
(655, 'XR 200 R', 0, 'SIM', 'xr-200-r', 114, 2),
(656, 'XR 250 TORNADO', 0, 'SIM', 'xr-250-tornado', 114, 2),
(657, 'XRE 300', 0, 'SIM', 'xre-300', 114, 2),
(658, 'COMET 250', 0, 'SIM', 'comet-250', 114, 2),
(659, 'COMET GT- 250R', 0, 'SIM', 'comet-gt-250r', 114, 2),
(660, 'COMET 250', 0, 'SIM', 'comet-250', 115, 2),
(661, 'COMET GT- 250R', 0, 'SIM', 'comet-gt-250r', 115, 2),
(662, 'CONCOURS', 0, 'SIM', 'concours', 116, 2),
(663, 'ER-6n', 0, 'SIM', 'er6n', 116, 2),
(664, 'NINJA 1000', 0, 'SIM', 'ninja-1000', 116, 2),
(665, 'NINJA 1000 TOURER', 0, 'SIM', 'ninja-1000-tourer', 116, 2),
(666, 'NINJA 250R', 0, 'SIM', 'ninja-250r', 116, 2),
(667, 'NINJA 300', 0, 'SIM', 'ninja-300', 116, 2),
(668, 'NINJA 650R', 0, 'SIM', 'ninja-650r', 116, 2),
(669, 'NINJA 650R', 0, 'SIM', 'ninja-650r', 116, 2),
(670, 'NINJA ZX-10', 0, 'SIM', 'ninja-zx10', 116, 2),
(671, 'NINJA ZX-10R', 0, 'SIM', 'ninja-zx10r', 116, 2),
(672, 'NINJA ZX-12', 0, 'SIM', 'ninja-zx12', 116, 2),
(673, 'NINJA ZX-12R 1200', 0, 'SIM', 'ninja-zx12r-1200', 116, 2),
(674, 'NINJA ZX-6R', 0, 'SIM', 'ninja-zx6r', 116, 2),
(675, 'NINJA ZX-6R 636', 0, 'SIM', 'ninja-zx6r-636', 116, 2),
(676, 'NINJA ZX-7', 0, 'SIM', 'ninja-zx7', 116, 2),
(677, 'VERSYS 650', 0, 'SIM', 'versys-650', 116, 2),
(678, 'VUCAN 900 CLASSIC', 0, 'SIM', 'vucan-900-classic', 116, 2),
(679, 'VUCAN 900 CLASSIC LT', 0, 'SIM', 'vucan-900-classic-lt', 116, 2),
(680, 'VUCAN 900 CUSTOM', 0, 'SIM', 'vucan-900-custom', 116, 2),
(681, 'Z 1000', 0, 'SIM', 'z-1000', 116, 2),
(682, 'ZX-10R', 0, 'SIM', 'zx10r', 116, 2),
(683, 'VENICE 50', 0, 'SIM', 'venice-50', 117, 2),
(684, 'RETRO 50', 0, 'SIM', 'retro-50', 117, 2),
(685, 'BIKE 50', 0, 'SIM', 'bike-50', 117, 2),
(686, 'NEW SUPER SMART 50', 0, 'SIM', 'new-super-smart-50', 117, 2),
(687, 'STRONG 250', 0, 'SIM', 'strong-250', 117, 2),
(688, 'HUNTER 90', 0, 'SIM', 'hunter-90', 118, 2),
(689, 'HUNTER 100', 0, 'SIM', 'hunter-100', 118, 2),
(690, 'WEB 100', 0, 'SIM', 'web-100', 118, 2),
(691, 'WEB EVO 100', 0, 'SIM', 'web-evo-100', 118, 2),
(692, 'FUTURE 125', 0, 'SIM', 'future-125', 118, 2),
(693, 'HUNTER 125', 0, 'SIM', 'hunter-125', 118, 2),
(694, 'MAX SE 125', 0, 'SIM', 'max-se-125', 118, 2),
(695, 'MAX SED 125', 0, 'SIM', 'max-sed-125', 118, 2),
(696, 'MOTARD 125', 0, 'SIM', 'motard-125', 118, 2),
(697, 'MOTARD 200', 0, 'SIM', 'motard-200', 118, 2),
(698, 'STX 125', 0, 'SIM', 'stx-125', 118, 2),
(699, 'STX 200', 0, 'SIM', 'stx-200', 118, 2),
(700, 'V BLADE 250', 0, 'SIM', 'v-blade-250', 118, 2),
(701, 'ADDRESS/AE 50', 0, 'SIM', 'addressae-50', 119, 2),
(702, 'ADDRESS/AG 100', 0, 'SIM', 'addressag-100', 119, 2),
(703, 'BURGMAN AN 125', 0, 'SIM', 'burgman-an-125', 119, 2),
(704, 'BURGMAN 650', 0, 'SIM', 'burgman-650', 119, 2),
(705, 'BURGMAN 400', 0, 'SIM', 'burgman-400', 119, 2),
(706, 'BURGMAN i', 0, 'SIM', 'burgman-i', 119, 2),
(707, 'BOULEVARD M800R', 0, 'SIM', 'boulevard-m800r', 119, 2),
(708, 'BOULEVARD M800', 0, 'SIM', 'boulevard-m800', 119, 2),
(709, 'DL 1000 V-STROM', 0, 'SIM', 'dl-1000-vstrom', 119, 2),
(710, 'DL 650 V-STROM', 0, 'SIM', 'dl-650-vstrom', 119, 2),
(711, 'EN 125 YES', 0, 'SIM', 'en-125-yes', 119, 2),
(712, 'GLADIUS', 0, 'SIM', 'gladius', 119, 2),
(713, 'GS 500 F', 0, 'SIM', 'gs-500-f', 119, 2),
(714, 'GSF 1200S BANDIT', 0, 'SIM', 'gsf-1200s-bandit', 119, 2),
(715, 'GSF 600S BANDIT', 0, 'SIM', 'gsf-600s-bandit', 119, 2),
(716, 'BANDIT 650S', 0, 'SIM', 'bandit-650s', 119, 2),
(717, 'BANDIT 650', 0, 'SIM', 'bandit-650', 119, 2),
(718, 'GSR 150i', 0, 'SIM', 'gsr-150i', 119, 2),
(719, 'GSR 150 S', 0, 'SIM', 'gsr-150-s', 119, 2),
(720, 'GSR 125', 0, 'SIM', 'gsr-125', 119, 2),
(721, 'GS 120', 0, 'SIM', 'gs-120', 119, 2),
(722, 'GSR 750', 0, 'SIM', 'gsr-750', 119, 2),
(723, 'GSR 750ZA', 0, 'SIM', 'gsr-750za', 119, 2),
(724, 'GSR 750A', 0, 'SIM', 'gsr-750a', 119, 2),
(725, 'GSX R1000', 0, 'SIM', 'gsx-r1000', 119, 2),
(726, 'GSX 1250FA', 0, 'SIM', 'gsx-1250fa', 119, 2),
(727, 'GSX 1300BK B-KING', 0, 'SIM', 'gsx-1300bk-bking', 119, 2),
(728, 'GSX 1300 R HAYABUSA', 0, 'SIM', 'gsx-1300-r-hayabusa', 119, 2),
(729, 'GSX 1300R Z HAYABUSA ABS', 0, 'SIM', 'gsx-1300r-z-hayabusa-abs', 119, 2),
(730, 'GSX 650F', 0, 'SIM', 'gsx-650f', 119, 2),
(731, 'GSX 750F KATANA', 0, 'SIM', 'gsx-750f-katana', 119, 2),
(732, 'GSX R600', 0, 'SIM', 'gsx-r600', 119, 2),
(733, 'GSX-R 1100 W', 0, 'SIM', 'gsxr-1100-w', 119, 2),
(734, 'GSX-R750', 0, 'SIM', 'gsxr750', 119, 2),
(735, 'INAZUMA', 0, 'SIM', 'inazuma', 119, 2),
(736, 'INTRUDER 125', 0, 'SIM', 'intruder-125', 119, 2),
(737, 'INTRUDER 250', 0, 'SIM', 'intruder-250', 119, 2),
(738, 'SV 650 S', 0, 'SIM', 'sv-650-s', 119, 2),
(739, 'SV1000', 0, 'SIM', 'sv1000', 119, 2),
(740, 'VL800 INTRUDER', 0, 'SIM', 'vl800-intruder', 119, 2),
(741, 'VZ 800 MARAUDER', 0, 'SIM', 'vz-800-marauder', 119, 2),
(742, 'STAR 50', 0, 'SIM', 'star-50', 120, 2),
(743, 'MOBY 50', 0, 'SIM', 'moby-50', 120, 2),
(744, 'SKY 50', 0, 'SIM', 'sky-50', 120, 2),
(745, 'SKY 125', 0, 'SIM', 'sky-125', 120, 2),
(746, 'TSS 150', 0, 'SIM', 'tss-150', 120, 2),
(747, 'FLY 150', 0, 'SIM', 'fly-150', 120, 2),
(748, 'TSS 250', 0, 'SIM', 'tss-250', 120, 2),
(749, 'FLY 250', 0, 'SIM', 'fly-250', 120, 2),
(750, 'MONTEZ 250', 0, 'SIM', 'montez-250', 120, 2),
(751, 'SKY 110', 0, 'SIM', 'sky-110', 120, 2),
(752, 'FLY 125', 0, 'SIM', 'fly-125', 120, 2),
(753, 'WORK 125', 0, 'SIM', 'work-125', 120, 2),
(754, 'BONNEVILLE T100', 0, 'SIM', 'bonneville-t100', 121, 2),
(755, 'DAYTONA 675 ABS', 0, 'SIM', 'daytona-675-abs', 121, 2),
(756, 'DAYTONA 675R ABS', 0, 'SIM', 'daytona-675r-abs', 121, 2),
(757, 'DAYTONA 955', 0, 'SIM', 'daytona-955', 121, 2),
(758, 'ROCKET III', 0, 'SIM', 'rocket-iii', 121, 2),
(759, 'ROCKET III CLASSIC', 0, 'SIM', 'rocket-iii-classic', 121, 2),
(760, 'ROCKET III ROADSTER', 0, 'SIM', 'rocket-iii-roadster', 121, 2),
(761, 'SCRAMBLER', 0, 'SIM', 'scrambler', 121, 2),
(762, 'SPEED TRIPLE', 0, 'SIM', 'speed-triple', 121, 2),
(763, 'SPRINT ST', 0, 'SIM', 'sprint-st', 121, 2),
(764, 'STREET TRIPLE ABS', 0, 'SIM', 'street-triple-abs', 121, 2),
(765, 'STREET TRIPLE R', 0, 'SIM', 'street-triple-r', 121, 2),
(766, 'THRUXTON', 0, 'SIM', 'thruxton', 121, 2),
(767, 'THUNDERBIRD COMMANDER', 0, 'SIM', 'thunderbird-commander', 121, 2),
(768, 'THUNDERBIRD STORN ABS', 0, 'SIM', 'thunderbird-storn-abs', 121, 2),
(769, 'TIGER 1050', 0, 'SIM', 'tiger-1050', 121, 2),
(770, 'TIGER 1050 SPORT', 0, 'SIM', 'tiger-1050-sport', 121, 2),
(771, 'TIGER EXPLORER ABS', 0, 'SIM', 'tiger-explorer-abs', 121, 2),
(772, 'TIGER EXPLORER XC ABS', 0, 'SIM', 'tiger-explorer-xc-abs', 121, 2),
(773, 'TROPHY SE', 0, 'SIM', 'trophy-se', 121, 2),
(774, 'TT600', 0, 'SIM', 'tt600', 121, 2),
(775, 'CRYPTON ED PENELOPE', 0, 'SIM', 'crypton-ed-penelope', 122, 2),
(776, 'CRYPTON ED', 0, 'SIM', 'crypton-ed', 122, 2),
(777, 'CRYPTON K', 0, 'SIM', 'crypton-k', 122, 2),
(778, 'MIDNIGHT STAR', 0, 'SIM', 'midnight-star', 122, 2),
(779, 'MT-07', 0, 'SIM', 'mt07', 122, 2),
(780, 'MT-07 ABS', 0, 'SIM', 'mt07-abs', 122, 2),
(781, 'MT-09', 0, 'SIM', 'mt09', 122, 2),
(782, 'R3', 0, 'SIM', 'r3', 122, 2),
(783, 'ROYAL STAR', 0, 'SIM', 'royal-star', 122, 2),
(784, 'TMAX', 0, 'SIM', 'tmax', 122, 2),
(785, 'TT-R 125 E', 0, 'SIM', 'ttr-125-e', 122, 2),
(786, 'TT-R 230 E', 0, 'SIM', 'ttr-230-e', 122, 2),
(787, 'V-MAX', 0, 'SIM', 'vmax', 122, 2),
(788, 'XTZ 125 E', 0, 'SIM', 'xtz-125-e', 122, 2),
(789, 'XTZ 125 K', 0, 'SIM', 'xtz-125-k', 122, 2),
(790, 'XTZ 125 XE', 0, 'SIM', 'xtz-125-xe', 122, 2),
(791, 'XV 250 VIRAGO', 0, 'SIM', 'xv-250-virago', 122, 2),
(792, 'XVS 650', 0, 'SIM', 'xvs-650', 122, 2),
(793, 'YBR FACTOR CENTENARIO', 0, 'SIM', 'ybr-factor-centenario', 122, 2),
(794, 'YBR FACTOR E', 0, 'SIM', 'ybr-factor-e', 122, 2),
(795, 'YBR FACTOR PRO E', 0, 'SIM', 'ybr-factor-pro-e', 122, 2),
(796, 'YBR FACTOR ED', 0, 'SIM', 'ybr-factor-ed', 122, 2),
(797, 'YBR FACTOR K', 0, 'SIM', 'ybr-factor-k', 122, 2),
(798, 'YZF 600R', 0, 'SIM', 'yzf-600r', 122, 2),
(799, 'YZF R1', 0, 'SIM', 'yzf-r1', 122, 2),
(800, 'YZF-R6', 0, 'SIM', 'yzfr6', 122, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_noticias`
--

CREATE TABLE IF NOT EXISTS `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style="text-align: justify;">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style="text-align: justify;">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	bons produtos, etiquetados;</p>\r\n<p style="text-align: justify;">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style="text-align: justify;">\r\n	bons instaladores, qualificados;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE IF NOT EXISTS `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE IF NOT EXISTS `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `id_subcategoriaproduto` int(11) NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modelo_produto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aro` int(100) DEFAULT NULL,
  `medida` int(100) DEFAULT NULL,
  `indice` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carga` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `largura` int(100) DEFAULT NULL,
  `perfil` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `garantia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `altura` int(11) NOT NULL,
  `indice_carga` int(11) NOT NULL,
  `indice_velocidade` int(11) NOT NULL,
  `imagem_flayer` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(38, 'PNEU MICHELIN 165/70 R 13 79T ENERGY XM2', '1805201704261257781246..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16570-r-13-79t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, 205, 'V (Até 240 km/h)', '96 (710 kg por pneu)', 165, 'teste perfil', '5 ANOS', 1, 0, 0, 70, 79, 190, '0605201702054235714231..jpg'),
(39, 'PNEU MICHELIN 175/70 R 13 82T ENERGY XM2', '1805201704271157934260..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17570-r-13-82t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, 175, 'V (Até 220 km/h)', '92 (790 kg por pneu)', 175, 'Fórmula Energy', '5 ANOS', 1, 0, 0, 70, 82, 190, '0605201702198208783177..jpg'),
(41, 'PNEU MICHELIN 185/70 R 13 86T ENERGY XM2', '1805201704271275967639..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade: </strong>em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18570-r-13-86t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, NULL, NULL, NULL, 185, NULL, '5 ANOS', 1, 0, 0, 70, 86, 190, '0605201702241377029029..jpg'),
(42, 'PNEU MICHELIN 165/70 R 14 81T ENERGY XM2', '1805201704281300633181..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16570-r-14-81t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 165, NULL, '5 ANOS', 1, 0, 0, 70, 81, 190, '0605201702277447726825..jpg'),
(43, 'PNEU MICHELIN 165/65 R 14 79T ENERGY XM2', '1805201704281215930504..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16565-r-14-79t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 165, NULL, '5 ANOS', 1, 0, 0, 65, 79, 190, '0605201702293259450329..jpg'),
(44, 'PNEU MICHELIN 175/80 R 14 88H ENERGY XM2', '1805201704291337939716..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17580-r-14-88h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 1, 0, 0, 80, 88, 210, '0605201702331297741581..jpg'),
(45, 'PNEU MICHELIN 175/70 R 14 88T ENERGY XM2', '1805201704291165942557..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17570-r-14-88t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 1, 0, 0, 70, 88, 190, '0605201702356130405105..jpg'),
(46, 'PNEU MICHELIN 175/65 R 14 82T ENERGY XM2', '1805201704301372934657..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-14-82t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 1, 0, 0, 65, 82, 190, '0605201702378196271976..jpg'),
(47, 'PNEU MICHELIN 175/65 R 14 86T ENERGY XM2', '1805201704311117795948..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-14-86t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 1, 0, 0, 65, 86, 190, '0605201702401587241064..jpg'),
(48, 'PNEU MICHELIN 185/70 R 14 88T ENERGY XM2 T', '1805201704311176758595..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18570-r-14-88t-energy-xm2-t', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 1, 0, 0, 70, 88, 190, '0605201702416462010091..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_aplicacoes`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_aplicacoes` (
  `idprodutoaplicacao` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_categorias`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_categorias` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_categoriaproduto` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos_categorias`
--

INSERT INTO `tb_produtos_categorias` (`id`, `id_produto`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(14, 1, 75, 'SIM', 0, ''),
(15, 1, 76, 'SIM', 0, ''),
(16, 1, 77, 'SIM', 0, ''),
(17, 82, 81, 'SIM', 0, ''),
(18, 82, 78, 'SIM', 0, ''),
(19, 83, 75, 'SIM', 0, ''),
(20, 83, 80, 'SIM', 0, ''),
(21, 84, 74, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(107, 258, 1, 43, 296, 0, 0, 'SIM', 0, ''),
(128, 256, 1, 13, 13, 0, 0, 'SIM', 0, ''),
(129, 256, 1, 13, 14, 0, 0, 'SIM', 0, ''),
(130, 256, 1, 14, 18, 0, 0, 'SIM', 0, ''),
(131, 256, 1, 15, 69, 0, 0, 'SIM', 0, ''),
(132, 256, 1, 20, 101, 0, 0, 'SIM', 0, ''),
(133, 256, 1, 21, 112, 0, 0, 'SIM', 0, ''),
(134, 256, 1, 27, 135, 0, 0, 'SIM', 0, ''),
(135, 256, 1, 28, 141, 0, 0, 'SIM', 0, ''),
(136, 256, 1, 30, 185, 0, 0, 'SIM', 0, ''),
(137, 256, 1, 35, 235, 0, 0, 'SIM', 0, ''),
(138, 256, 1, 42, 278, 0, 0, 'SIM', 0, ''),
(139, 256, 1, 47, 313, 0, 0, 'SIM', 0, ''),
(140, 38, 1, 13, 13, 0, 0, 'SIM', 0, ''),
(141, 38, 1, 14, 17, 0, 0, 'SIM', 0, ''),
(142, 38, 1, 15, 90, 0, 0, 'SIM', 0, ''),
(143, 38, 1, 16, 93, 0, 0, 'SIM', 0, ''),
(144, 38, 1, 18, 96, 0, 0, 'SIM', 0, ''),
(145, 38, 1, 20, 103, 0, 0, 'SIM', 0, ''),
(146, 38, 1, 22, 117, 0, 0, 'SIM', 0, ''),
(147, 38, 1, 22, 120, 0, 0, 'SIM', 0, ''),
(148, 38, 1, 23, 123, 0, 0, 'SIM', 0, ''),
(149, 38, 1, 24, 127, 0, 0, 'SIM', 0, ''),
(150, 38, 1, 25, 130, 0, 0, 'SIM', 0, ''),
(151, 38, 1, 30, 168, 0, 0, 'SIM', 0, ''),
(152, 38, 1, 30, 173, 0, 0, 'SIM', 0, ''),
(153, 38, 1, 30, 174, 0, 0, 'SIM', 0, ''),
(154, 38, 1, 30, 182, 0, 0, 'SIM', 0, ''),
(155, 39, 1, 14, 33, 0, 0, 'SIM', 0, ''),
(156, 39, 1, 14, 36, 0, 0, 'SIM', 0, ''),
(157, 39, 1, 15, 45, 0, 0, 'SIM', 0, ''),
(158, 39, 1, 15, 54, 0, 0, 'SIM', 0, ''),
(159, 41, 1, 14, 43, 0, 0, 'SIM', 0, ''),
(160, 41, 1, 15, 86, 0, 0, 'SIM', 0, ''),
(161, 41, 1, 22, 114, 0, 0, 'SIM', 0, ''),
(162, 41, 1, 31, 216, 0, 0, 'SIM', 0, ''),
(163, 42, 1, 14, 35, 0, 0, 'SIM', 0, ''),
(164, 42, 1, 14, 36, 0, 0, 'SIM', 0, ''),
(165, 42, 1, 15, 45, 0, 0, 'SIM', 0, ''),
(166, 42, 1, 15, 47, 0, 0, 'SIM', 0, ''),
(167, 42, 1, 15, 51, 0, 0, 'SIM', 0, ''),
(168, 42, 1, 15, 57, 0, 0, 'SIM', 0, ''),
(169, 42, 1, 15, 58, 0, 0, 'SIM', 0, ''),
(170, 42, 1, 15, 65, 0, 0, 'SIM', 0, ''),
(171, 43, 1, 15, 57, 0, 0, 'SIM', 0, ''),
(172, 43, 1, 15, 61, 0, 0, 'SIM', 0, ''),
(173, 43, 1, 15, 62, 0, 0, 'SIM', 0, ''),
(174, 43, 1, 15, 69, 0, 0, 'SIM', 0, ''),
(175, 43, 1, 15, 81, 0, 0, 'SIM', 0, ''),
(176, 43, 1, 15, 89, 0, 0, 'SIM', 0, ''),
(177, 43, 1, 16, 93, 0, 0, 'SIM', 0, ''),
(178, 43, 1, 17, 95, 0, 0, 'SIM', 0, ''),
(179, 44, 1, 13, 13, 0, 0, 'SIM', 0, ''),
(180, 44, 1, 13, 15, 0, 0, 'SIM', 0, ''),
(181, 44, 1, 14, 17, 0, 0, 'SIM', 0, ''),
(182, 44, 1, 14, 19, 0, 0, 'SIM', 0, ''),
(183, 44, 1, 14, 21, 0, 0, 'SIM', 0, ''),
(184, 44, 1, 14, 28, 0, 0, 'SIM', 0, ''),
(185, 45, 1, 13, 14, 0, 0, 'SIM', 0, ''),
(186, 45, 1, 14, 22, 0, 0, 'SIM', 0, ''),
(187, 45, 1, 14, 26, 0, 0, 'SIM', 0, ''),
(188, 45, 1, 15, 75, 0, 0, 'SIM', 0, ''),
(189, 45, 1, 15, 85, 0, 0, 'SIM', 0, ''),
(190, 45, 1, 15, 89, 0, 0, 'SIM', 0, ''),
(191, 45, 1, 17, 95, 0, 0, 'SIM', 0, ''),
(192, 46, 1, 15, 53, 0, 0, 'SIM', 0, ''),
(193, 46, 1, 19, 99, 0, 0, 'SIM', 0, ''),
(194, 46, 1, 20, 101, 0, 0, 'SIM', 0, ''),
(195, 46, 1, 20, 102, 0, 0, 'SIM', 0, ''),
(196, 46, 1, 20, 105, 0, 0, 'SIM', 0, ''),
(197, 46, 1, 20, 106, 0, 0, 'SIM', 0, ''),
(198, 47, 1, 14, 24, 0, 0, 'SIM', 0, ''),
(199, 47, 1, 14, 26, 0, 0, 'SIM', 0, ''),
(200, 47, 1, 14, 27, 0, 0, 'SIM', 0, ''),
(201, 47, 1, 14, 28, 0, 0, 'SIM', 0, ''),
(202, 48, 1, 14, 19, 0, 0, 'SIM', 0, ''),
(203, 48, 1, 14, 23, 0, 0, 'SIM', 0, ''),
(204, 48, 1, 14, 27, 0, 0, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_promocoes`
--

CREATE TABLE IF NOT EXISTS `tb_promocoes` (
  `idpromocao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `imagem_principal` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_promocoes`
--

INSERT INTO `tb_promocoes` (`idpromocao`, `titulo`, `descricao`, `imagem`, `imagem_principal`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `url`) VALUES
(1, 'promoção em destaque', NULL, '1705201712382660003532..jpg', '1705201712378657524080.jpg', 'SIM', NULL, 'promocao-em-destaque', '', '', '', NULL, '/produto/hjhjhjhjh'),
(2, 'promoção 01', '<div>\r\n	teste 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div>\r\n<div>\r\n	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div>\r\n<div>\r\n	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div>\r\n<div>\r\n	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div>\r\n<div>\r\n	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div>\r\n<div>\r\n	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '2004201707401356301440..jpg', NULL, 'NAO', NULL, 'promocao-01', '', '', '', NULL, ''),
(46, 'promoção 02', NULL, '2104201701261234340349..jpg', NULL, 'NAO', NULL, 'promocao-02', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE IF NOT EXISTS `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Promoções', 'Promoções', NULL, NULL, 'SIM', NULL, NULL),
(5, 'Orçamento', 'Orçamento', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Lojas', 'Lojas', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(9, 'Trabalhe Conosco', 'Trabalhe Conosco', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `imagem_icone`, `imagem_principal`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(5, 'SUSPENSÃO', '<p>\r\n	Fundada em 1990 com a filosofia de sempre contribuir para evolu&ccedil;&atilde;o tecnol&oacute;gica,nos especializamos a cada dia para propor<br />\r\n	cionar trabalhos de alta qualidade para nossa clientela.&nbsp;</p>', '2702201701321337852680..jpg', '2304201712261250287984.png', '2508201602594049189843.jpg', 'SIM', NULL, 'suspensao', '', '', '', 0, '', ''),
(57, 'BALANCEAMENTO', '<p>\r\n	Redu&ccedil;&atilde;o de custos com manuten&ccedil;&atilde;o: ACM protege a parede externa da polui&ccedil;&atilde;o e do clima.</p>\r\n<p>\r\n	Mant&eacute;m a modernidade da fachada por muito tempo.</p>', '2702201701333778186899..jpg', '2304201712271152020262.png', '', 'SIM', NULL, 'balanceamento', '', '', '', 0, '', ''),
(58, 'REPARO ADEQUADO', '<div>\r\n	REPARO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712321329314538.png', '', 'SIM', NULL, 'reparo-adequado', NULL, NULL, NULL, 0, '', ''),
(59, 'CALIBRAGEM', '<div>\r\n	CALIBRAGEM Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712321217690215.png', '', 'SIM', NULL, 'calibragem', NULL, NULL, NULL, 0, '', ''),
(60, 'RODÍZIO DE PNEUS', '<div>\r\n	RODIZIO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712331396517216.png', '', 'SIM', NULL, 'rodizio-de-pneus', NULL, NULL, NULL, 0, '', ''),
(61, 'BALANCEAMENTO', '<div>\r\n	BALANCEAMENTO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712341265113446.png', '', 'SIM', NULL, 'balanceamento', NULL, NULL, NULL, 0, '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_unidades`
--

CREATE TABLE IF NOT EXISTS `tb_subcategorias_unidades` (
  `idsubcategoriaunidade` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaunidade` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sugestoes_mensagens`
--

CREATE TABLE IF NOT EXISTS `tb_sugestoes_mensagens` (
  `idsugestaomensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `id_categoriamensagem` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_sugestoes_mensagens`
--

INSERT INTO `tb_sugestoes_mensagens` (`idsugestaomensagem`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `id_categoriamensagem`) VALUES
(1, 'Podem existir mil obstáculos', '<p>\r\n	Podem existir mil obst&aacute;culos, mas nada far&aacute; com que meu amor por ti morra. Atravessarei at&eacute; os maiores mares, mas n&atilde;o existir&aacute; &aacute;gua suficiente que afogue o amor que sinto por voc&ecirc;.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'podem-existir-mil-obstaculos', 'Podem existir mil obstáculos', '', '', NULL, 4),
(2, 'Subirei até a montanha', 'Subirei até a montanha mais alta do mundo, só para te ver, e de lá gritarei seu nome para ver se me ouve, e se me ouvires, direi uma só frase: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(3, 'E quando o vento passar', 'E quando o vento passar, levará consigo o que eu disse, e quando ele soprar em seu ouvido, escutarás junto ao vento: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, 'E toda vez que o vento soprar', 'E toda vez que o vento soprar em seu ouvido, não será só apenas o vento, mas eu dizendo que te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(5, 'Desejo a você', '<p>\r\n	Um anivers&aacute;rio cheio de paz...<br />\r\n	Que os sentimentos mais puros<br />\r\n	se concretizem em gestos de bondade,<br />\r\n	e o amor encontre abertas as portas<br />\r\n	do seu cora&ccedil;&atilde;o.<br />\r\n	Que voc&ecirc; possa guardar deste anivers&aacute;rio<br />\r\n	as melhores lembran&ccedil;as.<br />\r\n	E que tudo contribua para sua felicidade.</p>\r\n<p>\r\n	Abra&ccedil;os e Feliz Anivers&aacute;rio!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'desejo-a-voce', '', '', '', NULL, 1),
(6, 'Seja Muito Feliz, Hoje e Sempre', 'Feliz aniversário! Que hoje todos os sorrisos se abram para você, que seu coração seja inundado pela alegria e gratidão pela vida e que as homenagens abundem, pois você merece viver um dia muito especial.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 'Duradouro e feliz', '<p>\r\n	A vida &eacute; feita de momentos bons e ruins. Aproveitem intensamente os bons e estejam unidos para superar os ruins. Assim se constr&oacute;i um casamento duradouro e feliz!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'duradouro-e-feliz', '', '', '', NULL, 2),
(8, 'O amor de um homem', '<p>\r\n	O amor de um homem por uma mulher &eacute; muito mais profundo e verdadeiro quando Deus acontece primeiro na vida do casal.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'o-amor-de-um-homem', 'O amor de um homem', 'O amor de um homem', 'O amor de um homem', NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_tipos_servicos` (
  `idtiposervico` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_servicos`
--

INSERT INTO `tb_tipos_servicos` (`idtiposervico`, `titulo`, `descricao`, `imagem`, `ordem`, `ativo`, `url_amigavel`) VALUES
(1, 'BALANCEAMENTO', '<p>\r\n	<strong>De Olho no Balanceamento</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Conhe&ccedil;a os sinais que indicam pneus desbalanceados e quando fazer a manuten&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Balanceamento &eacute; o processo de compensa&ccedil;&atilde;o feito para equilibrar o conjunto de pneu e rodas do ve&iacute;culo. Ele &eacute; importante para evitar o desgaste prematuro dos pneus e dos componentes da suspens&atilde;o e da dire&ccedil;&atilde;o.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Para que serve o balanceamento dos pneus?</strong></p>\r\n<p>\r\n	O balanceamento &eacute; necess&aacute;rio para suprimir trepida&ccedil;&otilde;es que possam ocorrer no volante do carro. Al&eacute;m disto, quando h&aacute; desbalanceamento, a dire&ccedil;&atilde;o torna-se inst&aacute;vel e ocorre um desgaste irregular dos pneus.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Sinais que indicam pneus desbalanceados</strong></p>\r\n<p>\r\n	A maneira mais comum de perceber um ve&iacute;culo com pneus desbalanceados &eacute; quando o motorista sente trepida&ccedil;&atilde;o no volante - ou &agrave;s vezes vibra&ccedil;&otilde;es no ve&iacute;culo todo &ndash; ao alcan&ccedil;ar velocidade m&eacute;dia de 60km/h.</p>\r\n<p>\r\n	Por&eacute;m, quando os pneus do eixo trativo e dos eixos auxiliares est&aacute; desbalanceado, &eacute; mais dif&iacute;cil perceber as vibra&ccedil;&otilde;es. Por este motivo &eacute; necess&aacute;rio balancear todas as rodas com frequ&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quando o balanceamento deve ser feito?</strong></p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de pneus;</p>\r\n<p>\r\n	Sempre que for efetuado conserto em pneus ou c&acirc;maras;</p>\r\n<p>\r\n	Por ocasi&atilde;o de vibra&ccedil;&otilde;es no volante ou guid&atilde;o;</p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de elementos do conjunto rodante &ndash; por exemplo: pastilhas de freios, rolamento da roda, pe&ccedil;as da suspens&atilde;o, etc;</p>\r\n<p>\r\n	Para conjuntos de bicicletas ou motocicletas que tenham rodas com raios. Neste caso, verifique periodicamente a tens&atilde;o e o estados deles;</p>\r\n<p>\r\n	A cada 10 mil quil&ocirc;metros rodados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Aproveite que agora voc&ecirc; sabe mais sobre balanceamento e leve seu carro ou moto para a manuten&ccedil;&atilde;o!</strong></p>', '1404201711471240525988.png', 0, 'SIM', 'balanceamento'),
(14, 'CALIBRAGEM', '<p>\r\n	<strong>Manter seu pneu calibrado</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Motorista sob press&atilde;o: Sua aten&ccedil;&atilde;o &eacute; fundamental quando o assunto &eacute; calibragem.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pode parecer um simples detalhe, daqueles que a gente s&oacute; lembra quando notamos alguma coisa errada. Mas a press&atilde;o dos pneus &eacute; importante n&atilde;o s&oacute; para evitar acidentes, como tamb&eacute;m para garantir mais economia para o seu bolso.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E o segredo para evitar muitos problemas est&aacute; na aten&ccedil;&atilde;o do motorista, que deve tornar o cuidado com os pneus um h&aacute;bito mais frequente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma recente pesquisa realizada pela Michelin em 2013, revelou n&uacute;meros que chamam a aten&ccedil;&atilde;o. Ao avaliar carros e caminhonetes em 5 cidades brasileiras, apenas 69% dos ve&iacute;culos tinham a press&atilde;o correta. E em 9% deles, a falta de calibragem adequada representava alto risco de acidentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para se ter uma ideia, levando essa propor&ccedil;&atilde;o em considera&ccedil;&atilde;o, &eacute; poss&iacute;vel afirmar que o Brasil tem aproximadamente 3,3 milh&otilde;es de ve&iacute;culos com alto potencial de provocar acidentes. Com consequ&ecirc;ncias, inclusive, para outros ve&iacute;culos.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	N&atilde;o &eacute; &agrave; toa que nosso pa&iacute;s ocupa a triste marca de 4&ordm; lugar em todo o mundo em acidentes graves no tr&acirc;nsito.</p>\r\n<div>\r\n	&nbsp;</div>', '1404201711481128883418.png', 0, 'SIM', 'calibragem'),
(15, 'RODÍZIO DE PNEUS', '<p>\r\n	<strong>Como fazer o rod&iacute;zio dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para obter o comportamento ideal do ve&iacute;culo em curvas, nas freadas, nas pistas molhadas e nas situa&ccedil;&otilde;es de emerg&ecirc;ncia &eacute; importante que os quatro pneus em uso tenham um desgaste semelhante entre eles. Por&eacute;m, cada um dos pneus que est&aacute; rodando em um ve&iacute;culo ser&aacute; submetido a diferentes esfor&ccedil;os e, por consequ&ecirc;ncia, ter&aacute; desgastes diferentes. Por exemplo, o eixo de tra&ccedil;&atilde;o vai exigir mais do pneu que o outro eixo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ao longo do tempo essas diferen&ccedil;as de desgaste se acentuar&atilde;o e poder&atilde;o comprometer o equil&iacute;brio do ve&iacute;culo. Por isso, &eacute; importante realizar rod&iacute;zios peri&oacute;dicos, alternando a posi&ccedil;&atilde;o dos pneus no ve&iacute;culo e para se obter semelhantes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O rod&iacute;zio b&aacute;sico consiste em montar os pneus que estavam no eixo dianteiro na parte traseira, conservando o mesmo lado do ve&iacute;culo, independente do tipo do pneu, se ele &eacute; direcional, assim&eacute;trico, caminhonete, etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por&eacute;m, em algumas situa&ccedil;&otilde;es espec&iacute;ficas, pode ser realizado um rod&iacute;zio em X, passando o pneu dianteiro direito para a posi&ccedil;&atilde;o traseira esquerda e vice-versa. Um exemplo da necessidade deste tipo de rod&iacute;zio &eacute; um ve&iacute;culo que fa&ccedil;a mais curvas para um lado que para outro em sua utiliza&ccedil;&atilde;o di&aacute;ria. Neste caso, os pneus de um lado se desgastar&atilde;o mais que o outro lado, e o rod&iacute;zio b&aacute;sico n&atilde;o ir&aacute; conseguir equilibrar os desgastes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Eacute; necess&aacute;rio verificar o tipo de desgaste dos pneus para decidir o melhor rod&iacute;zio a fazer. O especialista em pneus presente nas lojas MICHELIN poder&aacute; orientar melhor sobre essas op&ccedil;&otilde;es de rod&iacute;zio.</p>', '1404201711481274437521.png', 0, 'SIM', 'rodizio-de-pneus'),
(16, 'SUSPENSÃO', '<p>\r\n	<strong>Suspens&atilde;o</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O sistema de suspens&atilde;o &eacute; respons&aacute;vel pela estabilidade do autom&oacute;vel e tem a fun&ccedil;&atilde;o de absorver todas as irregularidades do solo e evitar que trancos e solavancos incomodem os usu&aacute;rios. O sistema compreende o conjunto de todos os componentes mec&acirc;nicos que se articulam e unem as rodas do ve&iacute;culo &agrave; carroceira.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>H&aacute; cinco componentes essenciais no sistema de suspens&atilde;o, s&atilde;o eles:</strong></p>\r\n<p>\r\n	Molas</p>\r\n<p>\r\n	Amortecedores</p>\r\n<p>\r\n	Barras estabilizadoras</p>\r\n<p>\r\n	Pinos esf&eacute;ricos (piv&ocirc;s)</p>\r\n<p>\r\n	Bandejas de suspens&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Recomenda-se que a manuten&ccedil;&atilde;o da suspens&atilde;o seja feita regularmente ou quando surgir algum problema entre os intervalos de revis&atilde;o. Geralmente, esse intervalo acontece a cada 7.000 km, quando &eacute; sugerida uma avalia&ccedil;&atilde;o no balanceamento das rodas. Aproveite e fa&ccedil;a uma revis&atilde;o na suspens&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um ve&iacute;culo sem molas e amortecedores &eacute; desconfort&aacute;vel para seus usu&aacute;rios, com vibra&ccedil;&otilde;es no carro e ru&iacute;dos no painel, principalmente em pisos irregulares. Fortes impactos podem ocasionar danos e ocasionar trincas na estrutura do autom&oacute;vel, comprometendo sua vida &uacute;til. Os amortecedores limitam oscila&ccedil;&otilde;es no ve&iacute;culo, tornado a dirigibilidade muito mais segura e est&aacute;vel. Contudo, a continuidade do trabalho provoca desgaste no amortecedor. Portanto, fique atento. A vida &uacute;til do amortecedor &eacute; longa, mas aos 40.000 km fa&ccedil;a uma revis&atilde;o preventiva. Caso necess&aacute;rio, providencia rapidamente sua substitui&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Al&eacute;m dos amortecedores, certifique-se que os seus acess&oacute;rios e componentes (coifas de prote&ccedil;&atilde;o da haste, batentes e os coxins) estejam em ordem, pois eles tamb&eacute;m sofrem desgastes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O sistema de suspens&atilde;o influi diretamente no comportamento do ve&iacute;culo e pode provocar:</strong></p>\r\n<p>\r\n	Desgaste irregular dos pneus</p>\r\n<p>\r\n	Ru&iacute;dos indesej&aacute;veis</p>\r\n<p>\r\n	Descontrole do ve&iacute;culo</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Todo o cuidado &eacute; pouco! Dirija atenciosamente, preventivamente, e mantenha o seu ve&iacute;culo em boas condi&ccedil;&otilde;es de uso com Griffe Pneus, garantindo, assim, a sua seguran&ccedil;a e sua fam&iacute;lia</strong>.</p>', '1904201712361284610069.png', 0, 'SIM', 'suspensao'),
(17, 'REPARO ADEQUADO', '<p>\r\n	<strong>Recomenda&ccedil;&otilde;es na hora de reparar o pneu</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Qual a tecnologia e o material recomendado para a repara&ccedil;&atilde;o?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Na hora de fazer o conserto dos pneus do seu carro, opte sempre pela tecnologia oferecida nas revendas MICHELIN, a pe&ccedil;a de repara&ccedil;&atilde;o dos pneus (PRP), tamb&eacute;m conhecida como o plug de repara&ccedil;&atilde;o, destinado aos reparos de danos de 3 a 6 mm em pneus com c&acirc;mara.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	No processo de repara&ccedil;&atilde;o adotado pela MICHELIN, o pneu &eacute; examinado em toda a sua estrutura, principalmente na parte interna, a fim de detectar danos que possam prejudicar a mobilidade do ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Essa tecnologia aplicada corretamente garante a veda&ccedil;&atilde;o total do furo, evitando a perda de ar durante a rodagem e garantindo a m&aacute;xima seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A qualidade e a efic&aacute;cia da repara&ccedil;&atilde;o &eacute; de inteira e exclusiva responsabilidade da empresa que a realizou.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A garantia do pneu reparado passa a ser responsabilidade da empresa que efetuou a repara&ccedil;&atilde;o.</p>', '1904201712371146856439.png', 0, 'SIM', 'reparo-adequado'),
(18, 'ALINHAMENTO', '<p>\r\n	<strong>A import&acirc;ncia do alinhamento</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Saiba quando e onde fazer o alinhamento dos pneus do carro</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um aspecto muito importante que voc&ecirc; deve cuidar sempre na manuten&ccedil;&atilde;o do seu carro &eacute; o alinhamento. Ve&iacute;culos com &acirc;ngulos da suspens&atilde;o e dire&ccedil;&atilde;o incorretos podem apresentar problemas de comportamento e de seguran&ccedil;a.</p>\r\n<p>\r\n	Al&eacute;m disto, um alinhamento irregular causa um maior desgaste ao pneu e pode causar danos &agrave; suspens&atilde;o, dire&ccedil;&atilde;o e rodagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O que &eacute; o alinhamento?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Alinhamento &eacute; o processo de regulagem dos &acirc;ngulos da dire&ccedil;&atilde;o e suspens&atilde;o do ve&iacute;culo. Basicamente s&atilde;o tr&ecirc;s &acirc;ngulos a ser verificados no alinhamento: converg&ecirc;ncia/diverg&ecirc;ncia, c&acirc;mber e c&aacute;ster.</p>\r\n<p>\r\n	3 boas raz&otilde;es para fazer o alinhamento dos pneus constantemente</p>\r\n<p>\r\n	Voc&ecirc; economiza dinheiro, j&aacute; que seus pneus v&atilde;o durar mais;</p>\r\n<p>\r\n	A dire&ccedil;&atilde;o ficar&aacute; mais macia, pois haver&aacute; menor resist&ecirc;ncia de rolamento;</p>\r\n<p>\r\n	Voc&ecirc; ter&aacute; maior controle de dire&ccedil;&atilde;o, aumentando a seguran&ccedil;a do ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quando fazer o alinhamento dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Em todas as revis&otilde;es peri&oacute;dicas estipuladas pelo fabricante do ve&iacute;culo ou pelo menos a cada 7000 quil&ocirc;metros;</p>\r\n<p>\r\n	Sempre ap&oacute;s um impacto forte contra buracos, pedras, guias ou outros objetos;</p>\r\n<p>\r\n	Sempre que houver a substitui&ccedil;&atilde;o de algum elemento da suspens&atilde;o ou da dire&ccedil;&atilde;o;</p>\r\n<p>\r\n	Toda vez que notar algum comportamento estranho no ve&iacute;culo, tendendo a ir mais para um lado ou com dificuldade de se manter na trajet&oacute;ria;</p>\r\n<p>\r\n	Quando forem verificados desgastes irregulares nos pneus;</p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de pneus.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quanto tempo demora para fazer o alinhamento dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O processo &eacute; r&aacute;pido: a maioria dos alinhamentos na frente e atr&aacute;s demoram apenas 30 minutos!</p>', '1904201712381392393457.png', 0, 'SIM', 'alinhamento'),
(19, 'reparo galeria', '', '0905201704265474095071.jpg', 0, 'NAO', 'reparo-galeria');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` longtext,
  `imagem` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `descricao`, `imagem`, `ordem`, `ativo`, `url_amigavel`) VALUES
(1, 'CARRO', '', '1404201711551385879377.png', 0, 'SIM', 'carro'),
(2, 'MOTO', '', '1404201711561142586706.png', 0, 'SIM', 'moto'),
(3, 'CAMINHONETE', '', '1404201711571169892926.png', 0, 'SIM', 'caminhonete'),
(13, 'TRATORES', '', '1404201711581277238393.png', 0, 'SIM', 'tratores'),
(14, 'CAMINHÃO', '', '1404201711571114687468.png', 0, 'SIM', 'caminhao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_unidades`
--

CREATE TABLE IF NOT EXISTS `tb_unidades` (
  `idunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_place` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaunidade` int(11) NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `ddd1` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd2` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd3` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd4` varchar(10) CHARACTER SET latin1 NOT NULL,
  `telefone1` varchar(255) CHARACTER SET latin1 NOT NULL,
  `telefone2` varchar(255) CHARACTER SET latin1 NOT NULL,
  `telefone3` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `telefone4` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `id_unidade` int(11) NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_unidades`
--

INSERT INTO `tb_unidades` (`idunidade`, `titulo`, `endereco`, `src_place`, `id_categoriaunidade`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `telefone1`, `telefone2`, `telefone3`, `telefone4`, `id_unidade`, `imagem`) VALUES
(2, 'GOIÂNIA - JARDIM GOIÁS', 'RUA C-69 Nº 99 - JARDIM GOIÁS', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d61153.47560235291!2d-49.295200962930394!3d-16.672267774845956!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1abc70916d7%3A0x116e733ee7fce10a!2sGriffe+Pneus+Michellin+Jardim+Goi%C3%A1s+-+Flamboyant!5e0!3m2!1spt-BR!2sbr!4v1494515755823', 103, 'SIM', NULL, 'goiania--jardim-goias', NULL, NULL, NULL, '(62)', '(62)', '', '', '3515-1111', '99646-7175', '', '', 0, '1105201705566596084812..jpg'),
(45, 'GOIÂNIA - SETOR BUENO', 'AV 85 Nº 3376 - SETOR BUENO', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61153.47748275862!2d-49.295201009588446!3d-16.67226189216418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1220d555557%3A0xfce1794c20456c2a!2sGriffe+Pneus+Michellin+Av.+85!5e0!3m2!1spt-BR!2sbr!4v1494515798931', 103, 'SIM', NULL, 'goiania--setor-bueno', NULL, NULL, NULL, '(62)', '(62)', '', '', '3281-9111', '99902-1458', '', '', 0, '1105201705575162477418..jpg'),
(46, 'GOIÂNIA - SETOR OESTE', 'RUA 7 QUADRA F-5 LT.63 - SETOR OESTE', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61153.47936316319!2d-49.29520105624647!3d-16.672256009484233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef15d5366d807%3A0xfa8e71558e087d65!2sGriffe+Pneus+Michellin+Setor+Oeste+-+Pra%C3%A7a+Tamandare!5e0!3m2!1spt-BR!2sbr!4v1494515830496', 103, 'SIM', NULL, 'goiania--setor-oeste', NULL, NULL, NULL, '(62)', '(62)', '', '', '3093-3111', '99928-7973', '', '', 0, '1105201705586441336969..jpg'),
(47, 'GOIÂNIA - INDEPENDÊNCIA', 'AV. INDEPENDÊNCIA Nº 3778 - CENTRO', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61153.481243566515!2d-49.2952011029045!3d-16.67225012680612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef3c365cb79b5%3A0x1b37bfc2148998b7!2sGriffe+Pneus+Michellin+Av.+Independ%C3%AAncia!5e0!3m2!1spt-BR!2sbr!4v1494515865181', 103, 'SIM', NULL, 'goiania--independencia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3945-1111', '99826-4582', '', '', 0, '1105201705596289078688..jpg'),
(48, 'GOIÂNIA - SETOR COIMBRA', 'PÇ VALTER SANTOS Nº 242 - COIMBRA', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61153.483123968625!2d-49.29520114956255!3d-16.672244244129846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef4008d273a37%3A0xb4fd1a4955d263fb!2sGriffe+Pneus+Michellin+St.+Coimbra+-+Pra%C3%A7a+Walter+Santos!5e0!3m2!1spt-BR!2sbr!4v1494516087085', 103, 'SIM', NULL, 'goiania--setor-coimbra', NULL, NULL, NULL, '(62)', '(62)', '', '', '3086-3111', '99827-4026', '', '', 0, '1105201705582320413639..jpg'),
(49, 'ANÁPOLIS - JUNDIAÍ', 'AV. BRASIL SUL Nº. 45 - JUNDIAÍ', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d490082.0874286701!2d-49.229881932623954!3d-16.334880669546592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ea46357bfc721%3A0xaae7be11f01a9bf9!2sGriffe+Pneus+Michellin+An%C3%A1polis!5e0!3m2!1spt-BR!2sbr!4v1494516166697', 104, 'SIM', NULL, 'anapolis--jundiai', NULL, NULL, NULL, '(62)', '(62)', '', '', '3329-0250', '99838-2680', '', '', 0, '1105201705596030615290..jpg'),
(50, 'BRASÍLIA - 514 ASA SUL', 'SCRS Q.514 BL .B LJ. 69 TÉRREO - ASA SUL', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30709.57576796205!2d-47.922783966249625!3d-15.819924494749198!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3aaa689bac13%3A0x2014d8bb390cf0af!2sGriffe+Pneus+Michellin+514+Asa+Sul!5e0!3m2!1spt-BR!2sbr!4v1494516216615', 105, 'SIM', NULL, 'brasilia--514-asa-sul', NULL, NULL, NULL, '(61)', '(61)', '', '', '3245-6111', '99656-3872', '', '', 0, '1105201705549519898299..jpg'),
(51, 'BRASÍLIA - TAGUATINGA', 'QSD 23 LT 8 PISTÃO SUL - TAGUATINGA', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30705.691160586448!2d-48.06213046044924!3d-15.845482999999982!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a328899ed8105%3A0xb67e7e8c867d5113!2sGriffe+Pneus+Michellin+Taguatinga!5e0!3m2!1spt-BR!2sbr!4v1494516251077', 105, 'SIM', NULL, 'brasilia--taguatinga', NULL, NULL, NULL, '(61)', '(61)', '', '', '3562-2222', '99656-5825', '', '', 0, '1105201705562321072411..jpg'),
(52, 'BRASÍLIA - GUARÁ', 'SIA TRECHO 2 LT. 770 LOJA  1 - GUARÁ', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30711.714601015232!2d-47.97632346044927!3d-15.805834999999979!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3052f42d790f%3A0x4a035a5dd673867b!2sGriffe+Pneus+Michellin+SIA!5e0!3m2!1spt-BR!2sbr!4v1494516278939', 105, 'SIM', NULL, 'brasilia--guara', NULL, NULL, NULL, '(61)', '(61)', '', '', '3361-3111', '99938-8634', '', '', 0, '1105201705559762172276..jpg'),
(53, 'BRASÍLIA - 506 ASA SUL', 'SHC/S CR QD 506 BL. A LJ. 54 - ASA SUL', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30709.576407407472!2d-47.92278400139371!3d-15.819920284249184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3ac76fbb26df%3A0xb33a537aa71c7511!2sGriffe+Pneus+Michellin+506+Asa+Sul!5e0!3m2!1spt-BR!2sbr!4v1494516305100', 105, 'SIM', NULL, 'brasilia--506-asa-sul', NULL, NULL, NULL, '(61)', '(61)', '', '', '3346-8111', '99682-0922', '', '', 0, '1105201705544240516349..jpg'),
(54, 'BRASÍLIA - ASA NORTE', 'SEP NORTE Q.503 CJ. A BL. B - ASA NORTE', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30709.576620555927!2d-47.9227840131084!3d-15.819918880749409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3aaa6700864b%3A0x3be37111561079e5!2sGriffe+Pneus+Michellin+503+Asa+Norte!5e0!3m2!1spt-BR!2sbr!4v1494516335632', 105, 'SIM', NULL, 'brasilia--asa-norte', NULL, NULL, NULL, '(61)', '(61)', '', '', '3425-3444', '99633-9365', '', '', 0, '1105201705537890407294..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_unidades_categorias`
--

CREATE TABLE IF NOT EXISTS `tb_unidades_categorias` (
  `id` int(11) NOT NULL,
  `id_unidade` int(11) NOT NULL,
  `id_categoriaunidade` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_uniformes`
--

CREATE TABLE IF NOT EXISTS `tb_uniformes` (
  `iduniforme` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_uniformes`
--

INSERT INTO `tb_uniformes` (`iduniforme`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Uniformes de Saúdes', '2409201605381218447454.png', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'uniformes-de-saudes', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(8, 'Uniformes de Limpeza geral', '2409201605411332434728.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-de-limpeza-geral', NULL),
(9, 'Uniformes Cozinheiros', '2409201605401358107866.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-cozinheiros', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `idusuario` int(10) unsigned NOT NULL,
  `nome` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `rg` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `tel_celular` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tel_residencial` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `nome`, `email`, `senha`, `cpf`, `rg`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `tel_celular`, `tel_residencial`, `ativo`, `ordem`, `url_amigavel`) VALUES
(7, 'Marcio André da Silva', 'marcio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '1905364', 'Quadra 204 Conjunto 17 Lote', '08', 'Casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8606-6484', '(61) 3456-7864', 'SIM', 0, 'marcio-andre-da-silva'),
(8, 'Marcio', 'marciomas@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '8272916', 'Quadra 204 conjunto 20 lote', '1', '', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8539-8898', '(61) 3334-0987', 'SIM', 0, 'marcio'),
(9, 'MOBILE', '', 'cb7e7bca423a4b5a591a2c5bf49c688b', '697834785', '7548675', 'MOBILE', '8', 'MOBILE', 'MOBILE', 'MOBILE', 'MO', '9765341867', '4581236714', 'SIM', 0, 'mobile'),
(10, 'Joana Dark', 'joana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1905361', 'Quadra 204 conjunto 17 lote', '8', 'casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8604-9092', '(61) 3847-4939', 'SIM', 0, 'joana-dark'),
(11, 'Fabiana', 'fabiana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1896788', 'Quadra 200 lote', '8', 'apt 101', 'recanto das emas', 'brasilia', 'DF', '(61) 9837-8787', '(61) 8308-9221', 'SIM', 0, 'fabiana'),
(12, 'fabio', 'fabio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '9128937981', 'quadra 200 lote', '4', 'apt 5', 'recanto das emas', 'brasíliad', 'DF', '(61) 9373-8287', '(51) 8189-3637', 'SIM', 0, 'fabio'),
(13, 'mobile', 'mobile@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '576576576', '576576', 'quadra 204', '8', 'casa 1', 'recanto das emas', 'brasília', 'df', '7861872358761', '675126785673', 'SIM', 0, 'mobile'),
(14, 'User14', 'user14@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 conjunto 20', '5', 'casa 1', 'Recanto das Emas', 'Brasília', 'DF', '8765-0987', '3332-0987', 'SIM', 0, 'user14'),
(15, 'Homeweb', 'homewebbrasil@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 100', '8', 'casa 3', 'asa sul', 'brasília', 'df', '8765-0989', '3437-9876', 'SIM', 0, 'homeweb'),
(16, 'Terezinha Aparecida', 'terezinha@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 lote', '10', 'casa 5', 'Recanto das Emas', 'Brasília', 'DF', '91069282', '3423-0987', 'SIM', 0, 'terezinha-aparecida'),
(17, 'Miguel José', 'miguel@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 106 conjunto 20 lote', '10', 'apt 202', 'Recanto das Emas', 'Brasília', 'DF', '8876-0988', '3561-0987', 'SIM', 0, 'miguel-jose'),
(18, 'JOANA SILVA', 'joanasilva@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'QUADRA 100 LOTE', '34', 'CASA 2', 'RECANTO DAS EMAS', 'BRASÍLIA', 'DF', '8765-0998', '3456-0098', 'SIM', 0, 'joana-silva'),
(19, 'Bianca Barros', 'biancabarros1@hotmail.com', '729987adc7a9936a7ae9ba744291e35d', '', '', 'RUA SACRAMENTO', '0', 'QD 35, LT 20', 'CARDOSO', 'APARECIDA DE  GOIANIA', '74', '96871100', '62 35186845', 'SIM', 0, 'bianca-barros'),
(20, 'HOMEWEB TESTE', 'atendimento.sites@homewebbrasil.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'ENDEREÇO DO CONTATO', 'TESTE', 'COMPLEMENTO', 'TESTE', 'TESTE', 'GO', '(22) 2222-2222', '(22) 2222-2222', 'SIM', 0, 'homeweb-teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas`
--

CREATE TABLE IF NOT EXISTS `tb_vendas` (
  `idvenda` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `mensagem_cartao` longtext COLLATE utf8_unicode_ci,
  `id_frete` int(11) DEFAULT NULL,
  `endereco_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_venda` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'AGUARDANDO PAGAMENTO',
  `ponto_referencia` longtext CHARACTER SET utf8,
  `observacoes` longtext COLLATE utf8_unicode_ci,
  `horario_entrega` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg_email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `msg_deposito` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_pagamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_frete` double NOT NULL,
  `total` double NOT NULL,
  `data_pagamento` date NOT NULL,
  `data_separacao_entrega` date NOT NULL,
  `data_entrega` date NOT NULL,
  `data_entrega_cliente` date NOT NULL,
  `celular_contato` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_vendas_produtos` (
  `id_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `valor` double DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  ADD PRIMARY KEY (`idatuacao`);

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  ADD PRIMARY KEY (`idcategoriamensagem`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_categorias_unidades`
--
ALTER TABLE `tb_categorias_unidades`
  ADD PRIMARY KEY (`idcategoriaunidade`);

--
-- Indexes for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  ADD PRIMARY KEY (`idequipamento`);

--
-- Indexes for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  ADD PRIMARY KEY (`idequipe`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  ADD PRIMARY KEY (`idface`);

--
-- Indexes for table `tb_fornecedores`
--
ALTER TABLE `tb_fornecedores`
  ADD PRIMARY KEY (`idfornecedor`);

--
-- Indexes for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  ADD PRIMARY KEY (`idfrete`);

--
-- Indexes for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  ADD PRIMARY KEY (`idgaleria`);

--
-- Indexes for table `tb_galerias_equipamentos`
--
ALTER TABLE `tb_galerias_equipamentos`
  ADD PRIMARY KEY (`id_galeriaequipamento`);

--
-- Indexes for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  ADD PRIMARY KEY (`id_galeriageral`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`id_galeriaservico`);

--
-- Indexes for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  ADD PRIMARY KEY (`id_galeriauniformes`);

--
-- Indexes for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  ADD PRIMARY KEY (`idmarcaveiculo`);

--
-- Indexes for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  ADD PRIMARY KEY (`idmodeloveiculo`);

--
-- Indexes for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  ADD PRIMARY KEY (`idprodutoaplicacao`);

--
-- Indexes for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Indexes for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  ADD PRIMARY KEY (`idpromocao`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- Indexes for table `tb_subcategorias_unidades`
--
ALTER TABLE `tb_subcategorias_unidades`
  ADD PRIMARY KEY (`idsubcategoriaunidade`);

--
-- Indexes for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  ADD PRIMARY KEY (`idsugestaomensagem`);

--
-- Indexes for table `tb_tipos_servicos`
--
ALTER TABLE `tb_tipos_servicos`
  ADD PRIMARY KEY (`idtiposervico`);

--
-- Indexes for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- Indexes for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  ADD PRIMARY KEY (`idunidade`);

--
-- Indexes for table `tb_unidades_categorias`
--
ALTER TABLE `tb_unidades_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  ADD PRIMARY KEY (`iduniforme`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  ADD PRIMARY KEY (`idvenda`);

--
-- Indexes for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  ADD PRIMARY KEY (`id_venda`,`id_produto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  MODIFY `idatuacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  MODIFY `idcategoriamensagem` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `tb_categorias_unidades`
--
ALTER TABLE `tb_categorias_unidades`
  MODIFY `idcategoriaunidade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  MODIFY `idequipamento` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  MODIFY `idequipe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  MODIFY `idface` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_fornecedores`
--
ALTER TABLE `tb_fornecedores`
  MODIFY `idfornecedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  MODIFY `idfrete` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  MODIFY `idgaleria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_galerias_equipamentos`
--
ALTER TABLE `tb_galerias_equipamentos`
  MODIFY `id_galeriaequipamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  MODIFY `id_galeriageral` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=421;
--
-- AUTO_INCREMENT for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `id_galeriaservico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  MODIFY `id_galeriauniformes` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2010;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  MODIFY `idmarcaveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  MODIFY `idmodeloveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=801;
--
-- AUTO_INCREMENT for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  MODIFY `idprodutoaplicacao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  MODIFY `idpromocao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_subcategorias_unidades`
--
ALTER TABLE `tb_subcategorias_unidades`
  MODIFY `idsubcategoriaunidade` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  MODIFY `idsugestaomensagem` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_tipos_servicos`
--
ALTER TABLE `tb_tipos_servicos`
  MODIFY `idtiposervico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  MODIFY `idunidade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tb_unidades_categorias`
--
ALTER TABLE `tb_unidades_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  MODIFY `iduniforme` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `idusuario` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  MODIFY `idvenda` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  MODIFY `id_venda` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
