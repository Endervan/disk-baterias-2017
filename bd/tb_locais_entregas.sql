-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Ago-2018 às 23:55
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diskbaterias`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_locais_entregas`
--

CREATE TABLE `tb_locais_entregas` (
  `idlocalentrega` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `faixa_inicial` int(8) NOT NULL,
  `faixa_final` int(8) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_locais_entregas`
--

INSERT INTO `tb_locais_entregas` (`idlocalentrega`, `titulo`, `uf`, `faixa_inicial`, `faixa_final`, `ativo`, `ordem`, `url_amigavel`, `valor`) VALUES
(1, 'Goiânia', 'GO', 74000000, 74894999, 'SIM', 0, 'goiania', 0),
(2, 'Aparecida De Goiania', 'GO', 74900000, 74994999, 'SIM', 0, 'aparecida-de-goiania', 0),
(3, 'Anápolis', 'GO', 75000000, 75161000, 'SIM', 0, 'anapolis', 0),
(4, 'Nerópolis', 'GO', 75460000, 75460000, 'SIM', 0, 'neropolis', 0),
(5, 'Nova Veneza', 'GO', 75470000, 75470000, 'SIM', 0, 'nova-veneza', 0),
(6, 'Santo Antonio De Goiás', 'GO', 75375000, 75375000, 'SIM', 0, 'santo-antonio-de-goias', 0),
(7, 'Inhumas', 'GO', 75400000, 75400000, 'SIM', 0, 'inhumas', 0),
(8, 'Goianira', 'GO', 75370000, 75370000, 'SIM', 0, 'goianira', 0),
(9, 'Aragoiânia', 'GO', 75360000, 75360000, 'SIM', 0, 'aragoiania', 0),
(10, 'Trindade', 'GO', 75380001, 75394499, 'SIM', 0, 'trindade', 0),
(11, 'Bela Vista de Goiás', 'GO', 75240000, 75240000, 'SIM', 0, 'bela-vista-de-goias', 0),
(12, 'Senador Canedo', 'GO', 75250000, 75250000, 'SIM', 0, 'senador-canedo', 0),
(13, 'Terezópolis de Goiás', 'GO', 75175000, 75175000, 'SIM', 0, 'terezopolis-de-goias', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_locais_entregas`
--
ALTER TABLE `tb_locais_entregas`
  ADD PRIMARY KEY (`idlocalentrega`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_locais_entregas`
--
ALTER TABLE `tb_locais_entregas`
  MODIFY `idlocalentrega` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
