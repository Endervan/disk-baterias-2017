<div class="col-xs-3 lista-passos-carrinho">
  <div class="list-group top25">
    <a class="list-group-item btn-lg  <?php if(Url::getURL( 0 ) == "meus-pedidos"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto(); ?>/meus-pedidos" > <i class="fa fa-file-text-o right25" aria-hidden="true"></i> MEU PEDIDOS</a>
    <a class="list-group-item btn-lg  <?php if(Url::getURL( 0 ) == "meus-dados"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto(); ?>/meus-dados" > <i class="fa fa-user right25" aria-hidden="true"></i> MEUS DADOS</a>
    <a class="list-group-item btn-lg  <?php if(Url::getURL( 0 ) == "alterar-senha"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto(); ?>/alterar-senha" > <i class="fa fa-unlock-alt right25" aria-hidden="true"></i> ALTERAR SENHA</a>
  </div>
</div>
