<div class="container-fluid rodape bg-topo-rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container">
			<div class="row">

				<div class="col-xs-4 top35">
					<a href="<?php echo Util::caminho_projeto() ?>/">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="">
					</a>
				</div>

				<div class="col-xs-8 padding0 ">

					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<div class="col-xs-12 menu-rodape bottom10">
						<ul class="nav nav-tabs navbar-right">
							<li>
								<a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/"><h6>HOME</h6>
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/empresa"><h6>A EMPRESA</h6>
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "carrinho"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/produtos"><h6>PRODUTOS</h6>
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/servicos"><h6>SERVIÇOS</h6>
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/dicas"><h6>DICAS</h6>
								</a>
							</li>


							<li>
								<a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/fale-conosco"><h6>FALE CONOSCO</h6>
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
									href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco"><h6>TRABALHE CONOSCO</h6>
								</a>
							</li>

						</ul>
					</div>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->

					<div class="col-xs-10 padding0">

						<div class="col-xs-3 top15">
							<input class="btn btn_default input100" type="button" value="LIGUE AGORA">
						</div>

						<div class="col-xs-4 top25 media">
							<div class="media-left media-middle">
								<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
							</div>
							<div class="media-body ">
								<h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?></span></h5>
							</div>
							<h6><span>TELEVENDAS</span></h6>
						</div>

						<?php if (!empty($config[telefone2])): ?>
	            <div class="col-xs-4 top25 media">
	              <div class="media-left media-middle">
	                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/whats_topo.png" alt="">
	              </div>
	              <div class="media-body">
	                <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd2]) ?> <?php Util::imprime($config[telefone2]) ?></span></h5>
	              </div>
								<h6><span>WHATSAPP</span></h6>
	            </div>
	          <?php endif; ?>

						<div class="clearfix"></div>


						<?php if (!empty($config[telefone3])): ?>
	            <div class="col-xs-4 col-xs-offset-3 media">
	              <div class="media-left media-middle">
	                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
	              </div>
	              <div class="media-body">
	                <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd3]) ?> <?php Util::imprime($config[telefone3]) ?></span></h5>
	              </div>
	            </div>
	          <?php endif; ?>

						<?php if (!empty($config[telefone4])): ?>
	            <div class="col-xs-4 media">
	              <div class="media-left media-middle">
	                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
	              </div>
	              <div class="media-body">
	                <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd4]) ?> <?php Util::imprime($config[telefone4]) ?></span></h5>
	              </div>
	            </div>
	          <?php endif; ?>

						<div class="clearfix"></div>



						<div class="col-xs-3">
							<input class="btn btn_default input100" type="button" value="ONDE ESTAMOS">
						</div>

						<div class="col-xs-9 top15 bottom25 media">
							<div class="media-left media-middle">
								<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/localizacao.png" alt="">
							</div>
							<div class="media-body">
								<p class="media-heading"> <?php Util::imprime($config[endereco]) ?></p>
							</div>
						</div>


					</div>

					<div class="col-xs-2 padding0 text-right top50">
						<?php if ($config[google_plus] != "") { ?>
							<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
								<i class="fa fa-google-plus fa-2x top10 right15"></i>
							</a>
						<?php } ?>
						<a href="http://www.homewebbrasil.com.br" target="_blank">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
						</a>
					</div>






				</div>




			</div>
		</div>


		<div class="container-fluid">
			<div class="row rodape-baixo">
				<div class="col-xs-12 text-center top10 bottom10">
					<h5>© Copyright  DISKBATERIAS GOIÂNIA</h5>
				</div>
			</div>
		</div>
