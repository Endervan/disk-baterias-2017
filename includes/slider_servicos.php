<!--  ==============================================================  -->
<!--   EMPRESA E SERVICOS -->
<!--  ==============================================================  -->

<!-- Place somewhere in the <body> of your page -->
<div class="col-xs-12 padding0 servicos flexslider  top30">
  <ul class="slides">

    <?php
    $result = $obj_site->select("tb_servicos");
    if(mysql_num_rows($result) > 0){
      $i = 0;
      while($row = mysql_fetch_array($result)){
        ?>
        <li class=" relativo ">
            <div class="col-xs-12 text-center">
              <a class="" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" role="button">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 227, array("class"=>"imput100", "alt"=>"$row[titulo]")) ?>
                  <div class="mais_servicos_geral text-right">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/mais_servicos.png" alt="">
                  </div>
                </a>
                <h6 class="top25 text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
            </div>
        </li>
        <?php
      }
    }
    ?>
    <!-- items mirrored twice, total of 12 -->
  </ul>
</div>

<!--  ==============================================================  -->
<!--   EMPRESA E SERVICOS -->
<!--  ==============================================================  -->
