
<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):

  //  ARAMAZENO O LOCAL DE ENTREGA
  $_SESSION[id_bairro_entrega] = $_POST[bairro];
  $_SESSION[id_cidade] = $_POST[cidade];


  if(!isset($_SESSION[usuario])):
    Util::script_location(Util::caminho_projeto() . "/autenticacao");
  else:
    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
  endif;


endif;



# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if(isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

  $obj_carrinho->atualiza_itens($_POST['qtd']);
  $_SESSION[id_cidade] = $_POST[cidade];

endif;






# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

 $action = base64_decode($_GET[action]);
  $id = base64_decode($_GET[id]);

  //  ESCOLHO A OPCAO
  switch($action):

    case 'del':
    $obj_carrinho->del_item($id);
    break;

  endswitch;

endif;



# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] = 'add'):

  $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>

<div class="container-fluid bg-topo">
  <div class="row">


    <div class="container">
      <div class="row">
        <div class="col-xs-4 top10">
          <a href="<?php echo Util::caminho_projeto() ?>" title="Home">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="Home" class="input100">
          </a>
        </div>
        <div class="col-xs-8 padding0">

          <!--  ==============================================================  -->
          <!--telefones  -->
          <!--  ==============================================================  -->
          <div class="col-xs-4 col-xs-offset-2 mr-auto padding0 telefone_topo">
            <div class="media top10">
              <div class="media-left media-middle">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/ligue_topo.png" alt="">
              </div>
              <div class="media-body">
                <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?></span></h5>
              </div>
            </div>
          </div>

          <?php if (!empty($config[telefone2])): ?>
            <div class="col-xs-3 padding0 media">
              <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/whats_topo.png" alt="">
              </div>
              <div class="media-body">
                <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd2]) ?> <?php Util::imprime($config[telefone2]) ?></span></h5>
              </div>
            </div>
          <?php endif; ?>
          <!--  ==============================================================  -->
          <!--telefones  -->
          <!--  ==============================================================  -->


          <!--  ==============================================================  -->
          <!--CARRINHO -->
          <!--  ==============================================================  -->
          <div class="col-xs-3 relativo top10">
            <?php if(count($_SESSION[produtos]) == 0): ?>
              <a class="" href="javascript:void(0);" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/carrinho_vazio.png" alt="">
              </a>

              <div class="dropdown-menu titulo_carrinho  topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">



                <div class="col-xs-8  col-xs-offset-4">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_comprar1 btn_continuar">
                    <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR COMPRANDO <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                  </a>
                </div>
              </div>


            <?php else: ?>

              <a class="" href="javascript:void(0);" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/carrinho.png" alt="">
                <span class="numero_carinho"><?php echo count($_SESSION[produtos])?></span>
              </a>


              <div class="dropdown-menu titulo_carrinho  topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">
                <!-- <form action="<?php //echo Util::caminho_projeto(); ?>/orcamento/" method="post" name="form_produto_final" id="form_produto_final" > -->


                  <table class="table">
                    <tbody>

                      <?php foreach($_SESSION[produtos] as $key=>$dado_top):  ?>
                        <tr>
                          <td>
                            <?php $obj_site->redimensiona_imagem("../uploads/$dado_top[imagem]", 50, 50, array('alt'=>$dado_top[titulo])); ?>
                          </td>
                          <td align="left" class="col-xs-9">
                            <h6 class="text-uppercase"><span><?php Util::imprime($dado_top[titulo]) ?></span></h6>
                          </td>


                          <td class="col-xs-3 text-center">
                            <h4>R$ <?php echo Util::formata_moeda($dado_top[preco]) ?></h4>
                          </td>

                          <td class="text-center">
                            <a class="btn btn-vermelho" href="?id=<?php echo base64_encode($key) ?>&action=<?php echo base64_encode("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                              <img src="<?php echo Util::caminho_projeto() ?>/imgs/remove.png" alt="">
                            </a>
                          </td>

                        </tr>
                        <?php $total_top += $dado_top[preco] * $dado_top[qtd]; ?>
                      <?php endforeach; ?>

                    </tbody>
                  </table>

                  
                  <div class="col-xs-12 text-right relativo pg0">
                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento">
                    <i class="fa fa-shopping-cart finalizar right5" aria-hidden="true"></i>
                    <input type="submit" name="btn_enviar" id="btn_enviar" class="btn btn_finalizar btn-lg" value="FINALIZAR PEDIDO"  />
                  </a>
                  </div>
                  

                <!-- </form> -->


              </div>


            <?php endif ?>


          </div>
          <!--  ==============================================================  -->
          <!--CARRINHO -->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!--LOGIN -->
          <!--  ==============================================================  -->
          <?php /*
          <div class="col-xs-3 top5 menu-topo-usuario">
           
            <div class="dropdown">
              <?php if (!isset($_SESSION[usuario])): ?>
                <a id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/login.png" alt="">
                </a>
                <ul class="dropdown-menu lista-categorias-submenu menu-topo-usuario" aria-labelledby="dLabel-topo">
                  <li><a  href="<?php echo Util::caminho_projeto() ?>/autenticacao"><i class="fa fa-sign-in right10" aria-hidden="true"></i> Entrar</a></li>

                </ul>

              <?php else: ?>

                <a class="btn usuario_topo text-center text-capitalize" id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-user pull-left top5 " aria-hidden="true"></i>Ola, <?php Util::imprime($_SESSION[usuario][nome],8) ?> <i class="fa fa-angle-down pull-right top5" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu lista-categorias-submenu menu-topo-usuario" aria-labelledby="dLabel-topo">
                  <li><a href="<?php echo Util::caminho_projeto() ?>/meus-pedidos"><i class="fa fa-shopping-cart right10" aria-hidden="true"></i> Meus pedidos</a></li>
                  <li><a href="<?php echo Util::caminho_projeto() ?>/meus-dados"><i class="fa fa-user right10" aria-hidden="true"></i>  Meus dados</a></li>
                  <li><a href="<?php echo Util::caminho_projeto() ?>/alterar-senha"><i class="fa fa-key right10" aria-hidden="true"></i> Alterar senha</a></li>
                  <li><a href="<?php echo Util::caminho_projeto() ?>/logoff"><i class="fa fa-sign-out right10" aria-hidden="true"></i> Sair</a></li>
                <?php endif ?>
              </ul>
            </div>
            
          </div>
          */ ?>


          <!--  ==============================================================  -->
          <!--LOGIN -->
          <!--  ==============================================================  -->

          <div class="clearfix">  </div>
          <div class="col-xs-8 col-xs-offset-4 top5 pesquisas_dicas">
            <form  action="<?php echo Util::caminho_projeto() ?>/produtos/"  method="post">
              <div class="input-group">
                <input name="busca_produtos" type="text" class="form-control input_dicas" placeholder="PESQUISAS PRODUTOS">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"> <i class="fa fa-search" type="submit" aria-hidden="true"></i></button>
                </span>
              </div><!-- /input-group -->
            </form>
          </div>

          


          <div class="clearfix"></div>


          <!--  ==============================================================  -->
          <!--MENU -->
          <!--  ==============================================================  -->
          <div class="col-xs-12 padding0">
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right menu_topo lato top10">
                <li >
                  <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">
                    <h6>HOME</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa" >
                    <h6>A EMPRESA</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "carrinho"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos" >
                    <h6>PRODUTOS</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/servicos" >
                    <h6>SERVIÇOS</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/dicas" >
                    <h6>DICAS</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco" >
                    <h6>FALE CONOSCO</h6>
                  </a>
                </li>
                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" >
                    <h6>TRABALHE CONOSCO</h6>
                  </a>
                </li>

              </ul>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--MENU -->
          <!--  ==============================================================  -->


        </div>
      </div>
    </div>

  </div>
</div>
