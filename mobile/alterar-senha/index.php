
<?php
require_once("../../class/Include.class.php");

$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if(!$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/mobile/produtos";
  header("location: $caminho ");

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn_formulario .btn-lg .pull-right {
    display: none;
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 26); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>

</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>MEUS DADOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row">
    <div class="col-12">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  top5" href="<?php echo Util::caminho_projeto() ?>/mobile/meus-dados">MEUS DADOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho  left5 top5" href="<?php echo Util::caminho_projeto() ?>/mobile/meus-pedidos">MEUS PEDIDOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho active left5 top5" href="#">ALTERAR SENHA</a></li>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>





  <div class="row">



    <!--  ==============================================================  -->
    <!-- meus dados-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">
      <div class="col-12 top50">
        <h5><b>CRIAR NOVA CONTA</b></h5>
      </div>

      <?php
      if(isset($_GET[btn_cadastrar])):

        $obj_usuario->atualiza_dados($_GET);

        echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Senha atualizado com sucesso.</div>";

      endif;
      ?>

      <form method="get" class="p2" action="#" target = "_top" >



        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha" data-minlength="6"    class="input-form input100 block border-none p0 m0" placeholder="SENHA NOVA" required>
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha2"    class="input-form input100 block border-none p0 m0" placeholder="CONFIRMAR SENHA NOVA" required>
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>



      <div class="col-12 text-right">
        <input type="submit"
        value="ENVIAR"
        name="btn_cadastrar"
        class="btn  btn_formulario btn-lg">

      </div>

  </form>
</div>
<!--  ==============================================================  -->
<!--  meus dados-->
<!--  ==============================================================  -->

</div>





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
