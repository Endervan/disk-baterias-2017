<?php

require_once("../../class/Include.class.php");
$obj_site = new Site();

$obj_usuario = new Usuario();


//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/endereco-entrega" );

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 144px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>AUTENTICAÇÃO</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row">
    <div class="col-12">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  top5" href="#">CARRINHO</a></li>
        <li role="presentation"><a class="btn btn_carrinho active left5 top5" href="#">CONTA</a></li>
        <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">ENTREGA</a></li>
        <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">PAGAMENTO</a></li>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>





  <div class="row">


    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12 padding0 alt_formulario">
      <div class="col-12 top50">
        <h5><b>MINHA CONTA</b></h5>
      </div>


      <?php
      # ==============================================================  #
      # VERIFICO SE E PARA EFETUAR O LOGIN
      # ==============================================================  #
      if(isset($_GET[btn_login])):

        if($obj_usuario->verifica_usuario($_GET[email], $_GET[senha])):

          if (isset($_SESSION[produtos])) {
            Util::script_location(Util::caminho_projeto()."/mobile/endereco-entrega");
          }else{
            Util::script_location(Util::caminho_projeto()."/mobile/produtos");
          }

        else:
          echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Usuário ou senha inválido.</div>";
        endif;

      endif;
      ?>


      <form method="get" class="p2" action="#" target = "_top" >


        <div class="col-12 ampstart-input inline-block  top30 form_contatos m0 p0 mb3">

          <div class="relativo">
            <input type="email" name="email" class="input-form input100 block border-none p0 m0"  placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>


          <div class="relativo">
            <input type="password" name="senha" class="input-form input100 block border-none p0 m0" placeholder="SENHA" required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>



          <div class="col-8 padding0 text-right top10">
            <a class="btn1" href="<?php echo Util::caminho_projeto() ?>/mobile/recuperar-senha" title="Esqueci minha senha">
              <h2><b>Esqueci minha senha</b></h2>
            </a>
          </div>

          <div class="col-4 padding0">
            <input type="submit"
            value="ENVIAR"
            name="btn_login"
            class="btn  btn_formulario btn-lg pull-right">
          </div>

        </div>


      </form>
    </div>

    <!--  ==============================================================  -->
    <!-- SOU CADASTRADO-->
    <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!-- NAO SOU CADASTRADO-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">
      <div class="col-12 top50">
        <h5><b>CRIAR NOVA CONTA</b></h5>
      </div>

      <?php
      # ==============================================================  #
      # VERIFICO SE E PARA CADASTRAR O USUARIO
      # ==============================================================  #
      if(isset($_GET[btn_cadastrar])):

        if($obj_usuario->cadastra_usuario($_GET) != false)
        {
          Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
        }
        else
        {
          echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Esse email já está cadastrado.</div>";
        }

      endif;
      ?>
      <form method="get" class="p2" action="#" target = "_top" >

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="nome" class="input-form input100 block border-none p0 m0" placeholder="NOME">
            <span class="fa fa-user form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="email" class="input-form input100 block border-none p0 m0" placeholder="E-MAIL">
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
        </div>



        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_residencial" class="input-form input100 block border-none p0 m0" placeholder="TELEFONE">
            <span class="fa fa-phone form-control-feedback"></span>
          </div>
        </div>

        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_celular" class="input-form input100 block border-none p0 m0" placeholder="CELULAR">
            <span class="fa fa-mobile form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="endereco" class="input-form input100 block border-none p0 m0" placeholder="ENDEREÇO">
            <span class="fa fa-address-book form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="numero" class="input-form input100 block border-none p0 m0" placeholder="NÚMERO">
            <span class="fa fa-calculator form-control-feedback"></span>
          </div>
        </div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="complemento" class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO">
            <span class="fa fa-list form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="bairro" class="input-form input100 block border-none p0 m0" placeholder="BAIRRO">
            <span class="fa fa-home form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="cidade" class="input-form input100 block border-none p0 m0" placeholder="CIDADE">
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="uf" class="input-form input100 block border-none p0 m0" placeholder="UF">
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha" class="input-form input100 block border-none p0 m0" placeholder="SENHA">
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha2" class="input-form input100 block border-none p0 m0" placeholder="CONFIRMAR SENHA">
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 text-right">
          <div class="top15 bottom25">
            <button type="submit" class="btn btn_formulario" name="btn_cadastrar">
              CADASTRAR
            </button>
          </div>
        </div>

      </form>
    </div>
    <!--  ==============================================================  -->
    <!--  NAO SOU CADASTRADO-->
    <!--  ==============================================================  -->

  </div>





  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>
