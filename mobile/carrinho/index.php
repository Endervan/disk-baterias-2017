<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_carrinho = new Carrinho();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];





# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] == 'add'):

  $obj_carrinho->add_item($_GET['idproduto']);

endif;


# ==============================================================  #
# VERIFICO SE PARA FINALIZAR O CARRINHO, VERIFICA SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_GET[btn_enviar])):

  //if(!isset($_SESSION[usuario])):
  //  Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
  //else:
    Util::script_location(Util::caminho_projeto()."/mobile/endereco-entrega");
  //endif;

endif;





# ==============================================================  #
# VERIFICO SE E PARA ATUALIZAR O CARRINHO
# ==============================================================  #
if(isset($_GET[btn_atualizar])):

  $obj_carrinho->atualiza_itens($_GET['qtd']);


endif;



# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DA CIDADE
# ==============================================================  #
if(isset($_GET[cidade])):
  $_SESSION[id_cidade] = $_GET[cidade];
endif;


# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DO BAIRRO
# ==============================================================  #
if(isset($_GET[bairro])):
  $_SESSION[id_bairro_entrega] = $_GET[bairro];
endif;


# ==============================================================  #
# VERIFICO SE E PARA LIMPAR A SESSAO DA ENTREGA
# ==============================================================  #
if(isset($_GET[alterarlocal]) and $_GET[alterarlocal] = 'sim' ):
  unset($_SESSION[id_cidade]);
  unset($_SESSION[id_bairro_entrega]);
endif;



# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

  $action = ($_GET[action]);
  $id = ($_GET[id]);

  //  ESCOLHO A OPCAO
  switch($action):

    case 'del':
    $obj_carrinho->del_item($id);
    break;



  endswitch;

endif;






?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 25); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>MEU CARRINHO</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->




<?php
  
  # ==============================================================  #
  # VERIFICO SE E PARA VERIFICAR A TAXA DE ENTREGA
  # ==============================================================  #
  if(isset($_GET[cep])):
    
    $_SESSION[local_entrega] = $obj_carrinho->verifica_local_entrega($_GET[cep]);

    if($_SESSION[local_entrega] == false){
      $erro_entrega = 'sim';  
      unset($_SESSION[dados_entrega]);
    }else{
      $_SESSION[dados_entrega] = $obj_carrinho->busca_endereco($_GET[cep]);
    }
  endif;

  
  ?>



  <!--  ==============================================================  -->
  <!--  PAGINA DO CARRINHO -->
  <!--  ==============================================================  -->
    <div class="row">
      <div class="col-12">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav  top15">
          <li role="presentation"><a class="btn btn_carrinho active top5" href="#">CARRINHO</a></li>
          <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">CONTA</a></li>
          <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">ENTREGA</a></li>
          <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">PAGAMENTO</a></li>
        </ul>
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
      </div>

    </div>


    <form method="get" action="#" target="_top">
      <div class="row">

        <!--  ==============================================================  -->
        <!-- CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-12 tb-lista-itens top10">


          <?php if(count($_SESSION[produtos]) == 0): ?>

            <div class="alert alert-danger text-center top10">
              <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
            </div>

            <div class="text-center top10 bottom10">
              <a class="btn btn_orcamento btn_verde" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" >CONTINUAR COMPRANDO</a>
            </div>


          </div>

        <?php else: ?>


          <table class="table">
            <tbody>

              <tr class="tabela">
                <td></td>
                <td></td>
                <td><span>VALOR</span></td>
                <td class="text-center"><span>QDT.</span></td>
                <td class="text-center"><span>REMOVE</span></td>
              </tr>


              <?php foreach($_SESSION[produtos] as $key=>$dado):  ?>

                <tr>

                  <td>
                    <amp-img
                    height="50" width="50"
                    src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dado[imagem]); ?>" alt="<?php echo Util::imprime($dado[titulo]) ?>">
                  </amp-img>
                </td>
                <td align="left">
                  <h1 class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></h1>
                </td>

                <td class="padding0" align="left">
                  <h1>R$ <?php echo Util::formata_moeda($dado[preco]) ?></h1>
                </td>
                <td class="text-center">
                  <input type="number" min="1" class="input-lista-prod-orcamentos" name="qtd[]" value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                  <input name="idproduto[]" type="hidden" value="<?php echo $_SESSION[produtos][idproduto]; ?>"  />
                </td>
                <td class="text-center">
                  <a href="?id=<?php echo ($key) ?>&action=<?php echo ("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                    <amp-img
                    height="18" width="18"
                    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/excluir.png" alt="">
                  </amp-img>

                </a>
              </td>
            </tr>

            <?php $total += $dado[preco] * $dado[qtd]; ?>
          <?php endforeach; ?>






        </tbody>
      </table>

    <?php endif; ?>


    <?php if(count($_SESSION[produtos]) > 0): ?>
      <div class="col-12 text-right">
        <input class="btn btn_orcamento btn_verde" type="submit" name="btn_atualizar" id="btn_atualizar" value="ATUALIZAR CARRINHO"  />
      </div>
    <?php endif; ?>


    <!--  ==============================================================  -->
    <!-- CARRINHO-->
    <!--  ==============================================================  -->

    <div class="border-calcular-taxa-entrega col-12 top10 pb5">

    <div class="col-12 top10 bottom10">
      <h5 class="text-center text-success"><strong>CALCULAR TAXA DE ENTREGA</strong></h5>
    </div> 

    <div class="col-6">&nbsp;</div>

    <div class="col-4 ">
      <div class="form-group ">
          <input type="text" maxlength="8" name="cep" class="form-control fundo-form1 btn btn_mais_prod col-12" placeholder="CEP" value="<?php echo $_SESSION[dados_entrega][cep] ?>">
      </div>
    </div>

    <div class="col-2 ">
      <div class="form-group ">
        <input type="submit" name="btn_taxa_entrega" id="btn_taxa_entrega" class="btn btn_orcamento btn_verde" value="ok"  />
      </div>
    </div>


    </div>


    <?php if($erro_entrega == 'sim'): ?>

        <div class="col-12 top15 text-center bg-danger">
          <h6>Infelizmente não efetuamos entrega em sua região.</h6>    
        </div>

    <?php else: ?>

        <?php if(!empty($_SESSION[dados_entrega][cep]) ): ?>

          <div class="col-12 top15">
            <h5 class=" text-center">LOCAL DE ENTREGA </h5>
          </div>


          <div class="col-12 top10">
            <table class="input100">
              <tr>
                <td><b>Cidade</b></td>
                <td><strong>Bairro</strong></td>
                <td align="right"><strong>Taxa de Entrega</strong></td>
              </tr>
              <tr>

                <td><p class=""> <?php Util::imprime( $_SESSION[dados_entrega][bairro]) ?></p></td>

                <td><p class=""> <?php Util::imprime( $_SESSION[dados_entrega][localidade]) ?></p></td>
                <td align="right">
                  <?php
                    if ($_SESSION[local_entrega][valor] == 0) {
                      echo "<span class='text-success'><b>GRÁTIS</b></span>";
                    }else{
                      echo ' R$'.Util::formata_moeda($_SESSION[local_entrega][valor] );
                    }
                  ?>
                </td>
              </tr>
            </table>
          </div>



        <?php endif; ?>
    <?php endif; ?>                    


    <div class="col-12 top15 text-right">
      <h5>VALOR TOTAL :
        <b>  R$ <?php echo Util::formata_moeda($total + $_SESSION[local_entrega][valor]) ?></b>
      </h5>

      <div class="clearfix"></div>
      <div class="pull-right">
        <?php if(!empty($_SESSION[dados_entrega][cep]) and count($_SESSION[produtos]) > 0 ): ?>
          <input type="submit" name="btn_enviar" id="btn_enviar" class="btn btn_orcamento btn_verde top20" value="FINALIZAR PEDIDO"  />
        <?php endif; ?>
      </div>

      <div class="pull-left">
        <a class="btn btn_continue top20" href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
          <i class="fa fa-shopping-cart right5" aria-hidden="true"></i>CONTINUE NO SITE <i class="fa fa-angle-right left5" aria-hidden="true"></i>
        </a>
      </div>

    </div>


    <!--  ==============================================================  -->
    <!-- CAL CARRINHO -->
    <!--  ==============================================================  -->
  </div>
</div>

</form>
<!--  ==============================================================  -->
<!--  PAGINA DO CARRINHO -->
<!--  ==============================================================  -->






<div class="row">
  <div class="col-12 padding0 top20">
    <amp-img
    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/fundo_carrinho.jpg"
    width="320"
    height="174"
    layout="responsive"
    alt="">
  </amp-img>
</div>
</div>


</div>

</div>

<?php require_once("../includes/rodape.php") ?>

</body>



</html>
