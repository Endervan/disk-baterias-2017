<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>




</head>

<body class="bg-interna">
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'servicos';
  require_once("../includes/topo.php")
  ?>

  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>SERVIÇOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row  ">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <div class=" col-12">
        <div class="top15">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>


  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->



  <div class="row top20">

  <div class="col-12 top5 text-right">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho">
      <h2><span >NOSSSO DIFERENCIAIS</span></h2>
    </a>

  </div>

  </div>


  <div class="row">
    <div class="col-12 padding0">
      <amp-img
      src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/fundo_empresa.jpg"
      width="360"
      height="355"
      layout="responsive"
      alt="">
    </amp-img>
  </div>
  </div>






  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
