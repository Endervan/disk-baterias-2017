<?php
header('AMP-Access-Control-Allow-Source-Origin: http://localhost');

require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
/*
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/mobile/produtos";
  header("location: $caminho ");
endif;
*/


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto(). "/mobile/produtos";
  header("location: $caminho ");
endif;



# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if(isset($_GET[btn_cadastrar])):
  $obj_carrinho->armazena_mensagem_endereco_entrega($_GET);

  $caminho = Util::caminho_projeto(). "/mobile/pagamento";
  header("location: $caminho ");

endif;



// BUSCA META TAGS E TITLE
// BUSCA META TAGS E TITLE
$dados_dentro1 = $obj_site->select_unico("tb_seo", "idseo",1);
$description = $dados_dentro1[description_google];
$keywords = $dados_dentro1[keywords_google];
$titulo_pagina = $dados_dentro1[title_google];


?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
    <?php require_once("../css/geral.css"); ?>
    <?php require_once("../css/topo_rodape.css"); ?>
    <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 25); ?>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
      background-size:  100% 113px;
    }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>




  </head>

  <body class="bg-interna">
    <?php
    $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
    $exibir_link = 'produtos';
    require_once("../includes/topo.php");

    ?>



    <div class="row">
      <div class="col-12 top10 text-center localizacao-pagina">
        <!-- ======================================================================= -->
        <!-- TITULO PAGINA  -->
        <!-- ======================================================================= -->
        <h6><span>ENDERECO DE ENTREGA</span></h6>
        <!-- ======================================================================= -->
        <!-- TITULO PAGINA  -->
        <!-- ======================================================================= -->
      </div>
    </div>


    <!-- ======================================================================= -->
    <!--LISTA CONTATOS  -->
    <!-- ======================================================================= -->
    <?php require_once("../includes/lista_contatos.php") ?>
    <!-- ======================================================================= -->
    <!--LISTA CONTATOS  -->
    <!-- ======================================================================= -->

    <div class="row">
      <div class="col-12">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav  top15">
          <li role="presentation"><a class="btn btn_carrinho  top5" href="#">CARRINHO</a></li>
          <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">CONTA</a></li>
          <li role="presentation"><a class="btn btn_carrinho active left5 top5" href="#">ENTREGA</a></li>
          <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">PAGAMENTO</a></li>
        </ul>
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
      </div>

    </div>







  <div class="row">

    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->
    <div class="col-12 padding0">
      <form method="get" action="#" target="_top">


        <div class="ampstart-input inline-block  form_contatos m0 p0 mb3">

          <div class="col-9 top20">
            <div class="relativo  input100">
              <p>BAIRRO DA ENTREGA: <b><?php  Util::imprime( $_SESSION[dados_entrega][bairro]) ?></b></p>
            </div>
          </div>


          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/carrinho" class="btn btn_carrinho active pull-left" title="Alterar bairro">Alterar</a>
          </div>

          <div class="clearfix"></div>



          <div class="col-12">
            <div class="relativo  top20">
              <input type="text" name="nome_contato" class="input-form input100 block border-none p0 m0" placeholder="NOME DESTINATÁRIO" required>
              <span class="fa fa-user form-control-feedback"></span>
            </div>
          </div>


          <div class="col-6 top10">
            <div class="relativo ">
              <input type="text" name="telefone_contato" class="input-form input100 block border-none p0 m0" placeholder="TELEFONE" >
              <span class="fa fa-phone form-control-feedback"></span>

            </div>
          </div>


          <div class="col-6 top10">
            <div class="relativo  ">
              <input type="text" name="celular_contato" class="input-form input100 block border-none p0 m0" placeholder="CELULAR " required>
              <span class="fa fa-mobile form-control-feedback"></span>
            </div>
          </div>




          <div class="col-5 top10">
            <div class="relativo  ">
              <input type="text" maxlength="8" name="cep_entrega" class="input-form input100 block border-none p0 m0" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][cep]) ?>">
              <span class="fa fa-map-marker form-control-feedback"></span>
            </div>
          </div>


          <div class="col-7 top10">
            <div class="relativo  ">
              <input type="text" name="endereco_entrega" class="input-form input100 block border-none p0 m0" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][logradouro]) ?>">
              <span class="fa fa-address-book form-control-feedback"></span>
            </div>
          </div>

          <div class="col-4 top10">
            <div class="relativo  ">
              <input type="text" name="numero_entrega" class="input-form input100 block border-none p0 m0" placeholder="NR">
              <span class="fa fa-calculator form-control-feedback"></span>

            </div>
          </div>

          <div class="col-8 top10">
            <div class="relativo ">
              <input type="text" name="complemento_entrega" class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO">
              <span class="fa fa-list form-control-feedback"></span>
            </div>
          </div>


          <div class="clearfix"></div>


        <div class="col-12 top10">
          <div class="relativo ">
            <input type="text" name="ponto_referencia" class="input-form input100 block border-none p0 m0" placeholder="PONTO DE REFERÊNCIA" >
            <span class="fa fa-map-marker form-control-feedback"></span>

          </div>
        </div>



          <div class="col-12 top10">
            <div class="relativo ">
              <input type="text" name="bairro" disabled class="input-form input100 block border-none p0 m0" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][bairro]) ?>">
              <span class="fa fa-list form-control-feedback"></span>
            </div>
          </div>


          <div class="col-9 top10">
            <div class="relativo ">
              <input type="text" name="cidade" disabled class="input-form input100 block border-none p0 m0" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][localidade]) ?>">
              <span class="fa fa-list form-control-feedback"></span>
            </div>
          </div>


          <div class="col-3 top10">
            <div class="relativo ">
              <input type="text" name="uf" disabled class="input-form input100 block border-none p0 m0" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][uf]) ?>">
              <span class="fa fa-list form-control-feedback"></span>
            </div>
          </div>





          

          <div class="clearfix"></div>





       



          </div>



          <div class="col-12 text-right ">
            <input type="submit" name="btn_cadastrar" id="btn_enviar" class="btn btn_formulario" value="CADASTRAR"  />


          </div>

        </form>

      <!--  ==============================================================  -->
      <!-- LOCAL DA ENTREGA-->
      <!--  ==============================================================  -->


    </div>
  </div>






  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>
