<!--  ==============================================================  -->
<!-- CARRINHO-->
<!--  ==============================================================  -->
<div class="row">


<div class="col-12 tb-lista-itens top10">


  <?php if(count($_SESSION[produtos]) == 0): ?>

    <div class="alert alert-danger text-center top10">
      <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
    </div>

    <div class="text-center top10 bottom10">
      <a class="btn btn_orcamento btn_verde" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" >CONTINUAR COMPRANDO</a>
    </div>


  </div>

<?php else: ?>


  <table class="table">
    <tbody>

      <tr class="tabela">
        <td></td>
        <td></td>
        <td><span>VALOR</span></td>
        <td class="text-center"><span>QDT.</span></td>
        <td class="text-center"><span>REMOVE</span></td>
      </tr>


      <?php foreach($_SESSION[produtos] as $key=>$dado):  ?>

        <tr>

          <td>
            <amp-img
            height="50" width="50"
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dado[imagem]); ?>" alt="<?php echo Util::imprime($dado[titulo]) ?>">
          </amp-img>
        </td>
        <td align="left">
          <h1 class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></h1>
        </td>

        <td class="padding0" align="left">
          <h1>R$ <?php echo Util::formata_moeda($dado[preco]) ?></h1>
        </td>
        <td class="text-center">
          <input type="number" min="1" class="input-lista-prod-orcamentos" name="qtd[]" value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
          <input name="idproduto[]" type="hidden" value="<?php echo $_SESSION[produtos][idproduto]; ?>"  />
        </td>
        <td class="text-center">
          <a href="?id=<?php echo ($key) ?>&action=<?php echo ("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
            <amp-img
            height="18" width="18"
            src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/excluir.png" alt="">
          </amp-img>

        </a>
      </td>
    </tr>

    <?php $total += $dado[preco] * $dado[qtd]; ?>
  <?php endforeach; ?>






</tbody>
</table>

<?php endif; ?>


<?php if(count($_SESSION[produtos]) > 0): ?>
<div class="col-12 text-right">
<input class="btn btn_orcamento btn_verde" type="submit" name="btn_atualizar" id="btn_atualizar" value="ATUALIZAR CARRINHO"  />
</div>
<?php endif; ?>

</div>



<!--  ==============================================================  -->
<!-- CARRINHO-->
<!--  ==============================================================  -->
