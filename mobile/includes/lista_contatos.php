<div class="row contatos_geral">


  <?php
  if ($exibir_link == "servicos") {
    ?>

    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho">
      <button class="btn btn_orcamento">
        MEU CARRINHO <i class="fa fa-shopping-cart ml-1" aria-hidden="true"></i>
      </button>
    </a>
    <?php
  }else{
    ?>

    <a  class="btn btn_orcamento"
    on="tap:my-lightbox10"
    role="a"
    tabindex="0">
    FILTRA POR CATEGORIA <i class="fa fa-bars left5" aria-hidden="true"></i>
  </a>


  <?php
}
?>


  <amp-lightbox id="my-lightbox10" layout="nodisplay">
    <div class="lightbox" role="a" on="tap:my-lightbox10.close" tabindex="0">
      <?php require_once("../includes/menu_produtos.php") ?>
    </div>
  </amp-lightbox>



<button class="btn btn_orcamento btn_verde pull-right"
on="tap:my-lightbox444"
role="a"
tabindex="0">
<i class="fa fa-phone mr-3" aria-hidden="true"></i>LIGUE AGORA
</button>




<amp-lightbox id="my-lightbox444" layout="nodisplay">
  <div class="lightbox" role="a" on="tap:my-lightbox444.close" tabindex="0">

    <div class="col-12 pt10 pb20 bg_branco">
      <a class="btn btn_orcamento btn_verde bottom50" on="tap:my-lightbox444.close">X</a>

      <div class="col-12 padding0 bg_branco_borde">
        <h4 class="top10 col-8 text-right">
          <a class="btn_orcamento numero" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
          </a>
        </h4>
        <a class="btn btn_orcamento btn_verde col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
          CHAMAR
        </a>
      </div>

      <?php if (!empty($config[telefone2])) :?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top10 col-8 text-right">
            <a class="btn_orcamento numero" href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
              <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>
            </a>
          </h4>
          <a class="btn btn_orcamento btn_verde col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>



      <?php if (!empty($config[telefone3])): ?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top10 col-8 text-right">
            <a class="btn_orcamento numero" href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
              <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>
            </a>
          </h4>
          <a class="btn btn_orcamento btn_verde col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>


      <?php if (!empty($config[telefone4])): ?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top10 col-8 text-right">
            <a class="btn_orcamento numero" href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
              <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>
            </a>
          </h4>
          <a class="btn btn_orcamento btn_verde col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>


    </div>

  </div>
</amp-lightbox>


</div>
