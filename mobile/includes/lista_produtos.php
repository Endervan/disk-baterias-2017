<div class="row">

  <!--  ==============================================================  -->
  <!--   LISTAGEM PRODUTOS -->
  <!--  ==============================================================  -->
  <?php
  $i = 0;
  if(mysql_num_rows($result) == 0){
    echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
  }else{
    while($row = mysql_fetch_array($result)){
      ?>

      <div class="col-6 produtos_geral top25">
        <div class="card">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
            width="150"
            height="90"
            layout="responsive"
            alt="<?php echo Util::imprime($row[titulo]) ?>">
          </amp-img>
        </a>
        <div class="card-body">
          <div class="desc_titulo text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
          <!-- <div class="text-right"><h1 ><?php //echo Util::formata_moeda(($row[preco])); ?></h1></div> -->
        </div>

      </div>
      <div class="d-flex top5">
        <a class="btn btn_mais" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais">MAIS DETALHES</a>

        <a class="btn  btn_mais btn_compra  ml-auto" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $row[0] ?>&tipo_orcamento=produto">
          ORÇAMENTO
        </a>

      </div>
    </div>


    <?php
    if($i == 1){
      echo '<div class="clearfix"></div>';
      $i = 0;
    }else{
      $i++;
    }

  }
}
?>



</div>
<!--  ==============================================================  -->
<!--   LISTAGEM PRODUTOS -->
<!--  ==============================================================  -->
