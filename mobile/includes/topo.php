

<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):

  //  ARAMAZENO O LOCAL DE ENTREGA
  $_SESSION[id_bairro_entrega] = $_POST[bairro];
  $_SESSION[id_cidade] = $_POST[cidade];


  if(!isset($_SESSION[usuario])):
    Util::script_location(Util::caminho_projeto() . "/autenticacao");
  else:
    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
  endif;


endif;



# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if(isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

  $obj_carrinho->atualiza_itens($_POST['qtd']);
  $_SESSION[id_cidade] = $_POST[cidade];

endif;






# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

 $action = base64_decode($_GET[action]);
  $id = base64_decode($_GET[id]);

  //  ESCOLHO A OPCAO
  switch($action):

    case 'del':
    $obj_carrinho->del_item($id);
    break;

  endswitch;

endif;



# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] = 'add'):

  $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>



<div class="row bg_topo">
  <div class="col-2 ">
    <?php
    if(empty($voltar_para)){
      $link_topo = Util::caminho_projeto()."/mobile/";
    }else{
      $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
    }
    ?>
    <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>
  </div>
  <div class="col-8 topo">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-paginas.png" alt="Home" height="50" width="161"></amp-img>
    </a>
  </div>
  <div class="col-2 text-right">
    <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
  </div>



</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
  <ul class="menu-mobile">
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fa fa-home right10" aria-hidden="true"></i> Home</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fa fa-building-o right10" aria-hidden="true"></i> A Empresa</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos"> <i class="fa fa-server right10" aria-hidden="true"></i> Produtos</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos"> <i class="fa fa-handshake-o right10" aria-hidden="true"></i> Serviços</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas"> <i class="fa fa-calendar right10" aria-hidden="true"></i> Dicas</a></li>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco"> <i class="fa fa-envelope right10" aria-hidden="true"></i> Fale Consoco</a></li>
    <?php /*<li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco"> <i class="fa fa-group " aria-hidden="true"></i> Trabalhe Conosco</a></li>*/ ?>


  </ul>


  <?php /*  
  <div class="top20">
    <?php if (!isset($_SESSION[usuario])): ?>
      <a >

        <amp-img
        src="<?php echo Util::caminho_projeto() ?>/imgs/login.png"
        width="166"
        height="37"
        layout="responsive"
        alt="<?php echo Util::imprime($row[titulo]) ?>">
      </amp-img>
      </a>
      <ul class="menu-mobile" aria-labelledby="dLabel-topo">
        <li><a  href="<?php echo Util::caminho_projeto() ?>/mobile/autenticacao"><i class="fa fa-sign-in right10" aria-hidden="true"></i> Entrar</a></li>
      </ul>

    <?php else: ?>

      <a class="btn usuario_topo text-center text-capitalize" id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-user pull-left top5 " aria-hidden="true"></i>Ola, <?php Util::imprime($_SESSION[usuario][nome],8) ?> <i class="fa fa-angle-down pull-right top5" aria-hidden="true"></i>
      </a>
      <ul class="menu-mobile">
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/meus-pedidos"><i class="fa fa-shopping-cart right10" aria-hidden="true"></i> Meus pedidos</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/meus-dados"><i class="fa fa-user right10" aria-hidden="true"></i>  Meus dados</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/alterar-senha"><i class="fa fa-key right10" aria-hidden="true"></i> Alterar senha</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/logoff"><i class="fa fa-sign-out right10" aria-hidden="true"></i> Sair</a></li>
      <?php endif ?>
    </ul>
  </div>
  */
  ?>


</amp-sidebar>
