
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",0);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>




</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo_home">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="50" width="320"></amp-img>
    </div>
  </div>


  <div class=" row font-index ">
    <div class="col-12 text-center top25">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
      <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
      <amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_home.png" alt="Home" height="2" width="191"></amp-img>
        <p><?php Util::imprime($row[descricao]); ?></p>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->
  <div class="row">

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/fale-conosco">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
          width="77"
          height="70"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>


  </div>
  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->


</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
