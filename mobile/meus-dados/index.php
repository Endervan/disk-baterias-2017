
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");

endif;


if(isset($_POST[btn_cadastrar])):

  $obj_usuario->atualiza_dados($_POST);
  Util::script_msg("Dados atualizados com sucesso.");

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn .btn_formulario {
    display: none
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 26); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>

</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>MEUS DADOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row">
    <div class="col-12">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  active top5" href="#">MEUS DADOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho  left5 top5" href="<?php echo Util::caminho_projeto() ?>/mobile/meus-pedidos">MEUS PEDIDOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho left5 top5" href="<?php echo Util::caminho_projeto() ?>/mobile/alterar-senha">ALTERAR SENHA</a></li>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>





  <div class="row">



    <!--  ==============================================================  -->
    <!-- meus dados-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">
      <div class="col-12 top50">
        <h5><b>CRIAR NOVA CONTA</b></h5>
      </div>

      <?php
      # ==============================================================  #
      # VERIFICO SE E PARA CADASTRAR O USUARIO
      # ==============================================================  #
      if(isset($_GET[btn_cadastrar])):

        if($obj_usuario->cadastra_usuario($_GET) != false)
        {
          Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
        }
        else
        {
          Util::alert_bootstrap("Esse email já está cadastrado");
        }

      endif;
      ?>
      <form method="post" class="p2" action-xhr="envia.php" target="_top">

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="nome" value="<?php Util::imprime($_SESSION[usuario][nome]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="NOME">
            <span class="fa fa-user form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="email" value="<?php Util::imprime($_SESSION[usuario][email]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="EMAIL">
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
        </div>



        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_residencial" value="<?php Util::imprime($_SESSION[usuario][tel_residencial]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="TELEFONE">
            <span class="fa fa-phone form-control-feedback"></span>
          </div>
        </div>

        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_celular" value="<?php Util::imprime($_SESSION[usuario][tel_celular]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="CELULAR">
            <span class="fa fa-mobile form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="endereco" value="<?php Util::imprime($_SESSION[usuario][endereco]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="ENDEREÇO">
            <span class="fa fa-address-book form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="numero" value="<?php Util::imprime($_SESSION[usuario][numero]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="NÚMERO">
            <span class="fa fa-calculator form-control-feedback"></span>
          </div>
        </div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="complemento" value="<?php Util::imprime($_SESSION[usuario][complemento]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO">
            <span class="fa fa-list form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="bairro" value="<?php Util::imprime($_SESSION[usuario][bairro]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="BAIRRO">
            <span class="fa fa-home form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="bairro" value="<?php Util::imprime($_SESSION[usuario][bairro]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="BAIRRO">

            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="uf" value="<?php Util::imprime($_SESSION[usuario][uf]) ?>"  class="input-form input100 block border-none p0 m0" placeholder="UF">
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>


        <input type="submit"
        value="ENVIAR"
        class="btn  btn_formulario btn-lg pull-right">



    <div submit-success>
      <template type="amp-mustache">
        Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
      </template>
    </div>

    <div submit-error>
      <template type="amp-mustache">
        Houve um erro, {{name}} por favor tente novamente.
      </template>
    </div>


  </form>
</div>
<!--  ==============================================================  -->
<!--  meus dados-->
<!--  ==============================================================  -->

</div>





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
