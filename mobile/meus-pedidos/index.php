<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");

endif;


if(isset($_POST[btn_cadastrar])):

  $obj_usuario->atualiza_dados($_POST);
  Util::script_msg("Dados atualizados com sucesso.");

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn .btn_formulario {
    display: none
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 26); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>



</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>MEUS PEDIDOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->


  <div class="row">
    <div class="col-12 bottom20">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  top5" href="<?php echo Util::caminho_projeto() ?>/mobile/meus-dados">MEUS DADOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho  active left5 top5" href="#">MEUS PEDIDOS</a></li>
        <li role="presentation"><a class="btn btn_carrinho  left5 top5" href="<?php echo Util::caminho_projeto() ?>/mobile/alterar-senha">ALTERAR SENHA</a></li>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>


  <div class="row bottom50">


    <?php
    $result = $obj_site->select("tb_vendas", "and id_usuario = ".$_SESSION[usuario][idusuario]." order by hora desc");
    if (mysql_num_rows($result) == 0) {?>

      <div class="alert alert-danger">
        <h1 class=''>NAO FOI REALIZADO NENHUM PEDIDO<br></h1>
      </div>
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn_orcamento btn_verde">
        <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR COMPRANDO <i class="fa fa-angle-right left10" aria-hidden="true"></i>
      </a>

     <?php

      }else{

        ?>

        <div class="col-4">
          <h2 class=""><span>Pedido Nr.</span></h2>
        </div>
        <div class="col-4">
          <h2><span>Data</span></h2>
        </div>
        <div class="col-4 text-center">
          <h2><span>Hora</span></h2>
        </div>


        <div class="clearfix">

        </div>


        <amp-accordion disable-session-states  class="top20 pedidos">

        <?php
        while($row = mysql_fetch_array($result)){
          ?>
          <section class="top5">
            <h2 >
              <span class="col-4 text-center">
                <?php echo Util::imprime($row[idvenda]) ?>
              </span>
              <span class=" col-5 ">
                <?php echo Util::formata_data($row[data]) ?>
              </span>
              <span class=" col-3 text-center">
                <?php echo Util::imprime($row[hora], 5) ?>
              </span>
            </h2>


            <div class="conteudo">

              <table class="table">

                <thead>
                  <tr>
                    <th class="">TÍTULO</th>
                    <th class="text-center">QTD</th>
                    <th class="text-right">VALOR</th>
                  </tr>
                </thead>


                <tbody>
                  <?php
                  $b = 0;
                  $result1 = $obj_site->select("tb_vendas_produtos", "and id_venda = '$row[idvenda]' ");
                  if (mysql_num_rows($result1) > 0) {
                    while($row1 = mysql_fetch_array($result1)){
                      ?>
                      <tr class="top10">
                        <th scope="col-6 text-left text-uppercase"><?php echo Util::imprime($row1[titulo]) ?></th>
                        <th class="text-center"><?php echo Util::imprime($row1[qtd]) ?></th>
                        <th class="text-right"><?php echo Util::formata_moeda($row1[valor]) ?></th>
                      </tr>
                      <?php
                      $total_compra = $row1[valor]*$row1[qtd];
                    }
                    $b++;
                  }
                  ?>

                  <tr>
                    <th scope="row"></th>
                    <th class="text-center">TOTAL</th>
                    <th class="text-right"><?php echo Util::formata_moeda($total_compra) ?></th>
                  </tr>

                </tbody>

              </table>


            </div>




          </section>
          <?php
        }

        ?>
        </amp-accordion>

        <?php



      }
      ?>



    </div>



    <?php require_once('../includes/rodape.php'); ?>

  </body>

  </html>
