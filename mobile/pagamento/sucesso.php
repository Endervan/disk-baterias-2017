<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
/*
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto()."/mobile/produtos";
  header("location: $caminho");

endif;
*/

# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto()."/mobile/produtos";
  header("location: $caminho");
endif;





// BUSCA META TAGS E TITLE
$dados_dentro1 = $obj_site->select_unico("tb_seo", "idseo",1);
$description = $dados_dentro1[description_google];
$keywords = $dados_dentro1[keywords_google];
$titulo_pagina = $dados_dentro1[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>

  <?php require_once("../includes/head.php"); ?>


  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 25); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>




</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>PAGAMENTOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row">
    <div class="col-12">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  top5" href="#">CARRINHO</a></li>
        <li role="presentation"><a class="btn btn_carrinho left5 top5" href="#">CONTA</a></li>
        <li role="presentation"><a class="btn btn_carrinho  left5 top5" href="#">ENTREGA</a></li>
        <li role="presentation"><a class="btn btn_carrinho active left5 top5" href="#">PAGAMENTO</a></li>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>


  <div class="row">

    
    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->
    <div class="col-12 top30 ">

      <p><h2>Obrigado pela compra.</h2></p>
			<p>Pedido realizado com sucesso.</p>
			<p>Foi enviado um email para <?php echo $_GET['email'] ?> com mais informações.</p>
			<br>
 

    </div>
    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->


</div>






<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
