
<?php
require_once("../../class/Include.class.php");

$obj_site = new Site();
$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/mobile/produtos" );

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>




  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 26); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>


</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>MEUS DADOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row">
    <div class="col-12">
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
      <ul class="nav  top15">
        <li role="presentation"><a class="btn btn_carrinho  active left5 top5" href="#">RESETAR SENHA</a></li>

      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>

  </div>





  <div class="row">



    <!--  ==============================================================  -->
    <!-- meus dados-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">
      <div class="col-12 top50">
        <h5><b>INFOME O EMAIL CADASTRASTO PARA RECEBER SUA NOVA SENHA</b></h5>
      </div>


      <?php if($_GET['result'] == 'ok'){ ?>
        <div class="col-12 top20">
          <h5 class='text-success'>Sua nova senha foi enviado para o e-mail cadastrado.</h5>
          <br><br>
          <a class='btn_carrinho' href="<?php echo Util::caminho_projeto() ?>/mobile/autenticacao" title=""><h5>Clique aqui para efetuar login</h5></a>
        </div>
      <?php } elseif($_GET['result'] == 'false'){ ?>
        <div class="col-12 text-center top15 bottom15"><b>Não foi possível recuperar sua senha. E-mail não encontrado.</b>
        <?php } ?>

        <form method="get" class="p2" action="envia.php" >

          <div class="col-12 top10">
            <div class="relativo">
              <input type="text" name="email" class="input-form input100 block border-none p0 m0" placeholder="E-MAIL" required>
              <span class="fa fa-lock form-control-feedback"></span>
            </div>
          </div>


          <div class="col-12">
            <input type="submit"
            value="ENVIAR"
            name="btn_recuperar_senha"
            class="btn  btn_formulario btn-lg">
          </div>

        </form>

        <!--  ==============================================================  -->
        <!-- SOU CADASTRADO-->
        <!--  ==============================================================  -->


      </div>
      <!--  ==============================================================  -->
      <!--  meus dados-->
      <!--  ==============================================================  -->

    </div>





    <?php require_once('../includes/rodape.php'); ?>

  </body>

  </html>
