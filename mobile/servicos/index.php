<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 19); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>




</head>

<body class="bg-interna">
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'servicos';
  require_once("../includes/topo.php")
  ?>

  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>SERVIÇOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->





  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->
  <div class="row bg_servicos">

    <?php
    $result = $obj_site->select("tb_servicos");
    if(mysql_num_rows($result) > 0){

      while($row = mysql_fetch_array($result)){
        $i = 0;
        ?>

        <div class="col-6 relativo top15">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
            width="134"
            height="94"
            layout="responsive"
            alt="<?php echo Util::imprime($row[titulo]) ?>">
          </amp-img>
        </a>
        <div class="desc_servico top10 text-center"> <h2 class=" text-uppercase"><?php Util::imprime($row[titulo]); ?></h2></div>

        <div class="mais_servicos">
          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/mais_servicos.png"
          width="30"
          height="30"
          layout="responsive"
          alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
      </div>
    </div>

    <?php
    if ($i==1) {
      echo "<div class='clearfix'>  </div>";
    }else {
      $i++;
    }
  }
}
?>
</div>

</div>
<!--  ==============================================================  -->
<!--   SERVICOS -->
<!--  ==============================================================  -->


<div class="row top20">

  <div class="col-5">
    <button class="btn btn_orcamento btn_verde input100"
    on="tap:my-lightbox444"
    role="a"
    tabindex="0">
  </i>LIGUE AGORA
</button>
</div>


<div class="col-7 top5 text-center">
  <a href="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho">
    <h2><span >COMPRE AGORA</span></h2>
  </a>

</div>

</div>

<!-- ======================================================================= -->
<!--LISTA CONTATOS  -->
<!-- ======================================================================= -->
<?php require_once("../includes/rodape_paginas.php") ?>
<!-- ======================================================================= -->
<!--LISTA CONTATOS  -->
<!-- ======================================================================= -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
