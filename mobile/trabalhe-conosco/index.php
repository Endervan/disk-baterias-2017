<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn .btn_formulario {
    display: none
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 27); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 113px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php")
  ?>

  <div class="row">
    <div class="col-12 top10 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><span>CONTATOS</span></h6>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->

  <div class="row fundo_contatos">
    <div class="col-6 top10">
      <h4 class="text-center">ENTRE EM CONTATO</h4>
      <h4 class="left50 text-right"><span>FALE CONOSCO</span></h4>
    </div>
    <div class="col-6 top10">
      <div class="media">
        <div class="media-left media-middle">
          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_telefone.png"
          width="15"
          class="media-object right5"
          height="15"
          alt="">
        </amp-img>
      </div>
      <div class="media-body">
        <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd1]) ?> </span> <?php Util::imprime($config[telefone1]) ?></h5>
      </div>
    </div>

  </div>


  <div class="col-12 top15">
    <div class="media">
      <div class="media-left media-middle">
        <amp-img
        src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/local.png"
        width="20"
        class="media-object right5"
        height="20"
        alt="">
      </amp-img>
    </div>
    <div class="media-body">
      <p class="media-heading"> <?php Util::imprime($config[endereco]) ?></p>
    </div>
  </div>

</div>

</div>


<div class="row bottom30 ">
  <!-- ======================================================================= -->
  <!-- FORMULARIO   -->
  <!-- ======================================================================= -->
  <div class="col-12 alt_formulario">

    <form method="post" class="p2" action-xhr="envia.php" target="_top">


      <div class="ampstart-input inline-block  form_contatos m0 p0 mb3">
        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
          <span class="fa fa-user form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
          <span class="fa fa-phone form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
          <span class="fa fa-star form-control-feedback"></span>
        </div>

        <div class="relativo">
          <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
          <span class="fa fa-pencil form-control-feedback"></span>
        </div>

      </div>

      <input type="submit"
      value="ENVIAR"
      class="  btn_formulario pull-right">

      <!-- <div class="col-12  text-right bottom40 padding0">
      <div class="relativo btn_formulario">
      <button type="submit"
      value="OK"
      class="ampstart-btn caps btn ">ENVIAR MENSAGEM</button>
    </div>
  </div> -->


  <div submit-success>
    <template type="amp-mustache">
      Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
    </template>
  </div>

  <div submit-error>
    <template type="amp-mustache">
      Houve um erro, {{name}} por favor tente novamente.
    </template>
  </div>

</form>
</div>
<!-- ======================================================================= -->
<!-- FORMULARIO   -->
<!-- ======================================================================= -->

</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row relativo top40">


  <div class="col-12 top5 bottom50">
    <amp-iframe
    width="350"
    height="128"
    layout="responsive"
    sandbox="allow-scripts allow-same-origin allow-popups"
    frameborder="0"
    src="<?php Util::imprime($config[src_place]); ?>">
  </amp-iframe>
</div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
