<?php

$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if(!$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");

endif;







// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MEUS DADOS</span></h1>
        <h4> ATUALIZE NOVA SENHA</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">SENHA</li>
        </ol>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <!-- ======================================================================= -->
          <!-- passo    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/passo-a-passo.php') ?>
          <!-- ======================================================================= -->
          <!-- passo    -->
          <!-- ======================================================================= -->




          <div class="col-xs-9 titulo_carrinho padding0">
            <div class="col-xs-12">  <h3 class="left15">Atualize Sua Senha</h3></div>

            <!--  ==============================================================  -->
            <!--formulario-->
            <!--  ==============================================================  -->
            <div class="col-xs-12  fundo-formulario">

              <?php
              if(isset($_POST[btn_cadastrar])):

                $obj_usuario->atualiza_dados($_POST);
                Util::alert_bootstrap("Senha atualizado com sucesso.");

              endif;
              ?>
              <form class="form-inline FormCadastro" role="form" method="post" enctype="multipart/form-data">



                <div class="col-xs-12 top10">
                  <div class="form-group input100">
                    <input type="password" name="senha" data-minlength="6" required class="form-control fundo-form1 input100 input-lg" placeholder="CADASTRAR NOVA SENHA">
                  </div>
                </div>

                <div class="col-xs-12 top10">
                  <div class="form-group input100">
                    <input type="password" name="senha2" class="form-control fundo-form1 input100 input-lg" placeholder="CONFIRMAR NOVA SENHA">
                  </div>
                </div>



                <div class="col-xs-12 text-right ">
                  <div class="top15 bottom25">
                    <button type="submit" class="btn btn_finalizar" name="btn_cadastrar">
                      CADASTRAR
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- formulario-->
            <!--  ==============================================================  -->

          </div>



        </div>
      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>



<script>
$(document).ready(function() {
  $('.FormCadastro').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      senha: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha2',
            message: 'As senhas não sào iguais'
          }
        }
      },
      senha2: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha',
            message: 'As senhas não sào iguais'
          }
        }
      }
    }


    });
  });
  </script>
