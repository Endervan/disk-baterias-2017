<?php
$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/endereco-entrega" );

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MINHA CONTA</span></h1>
        <h4>CADASTRE SEUS DADOS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">MINHA CONTA</li>
        </ol>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_autenticacao">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <div class="col-xs-3 lista-passos-carrinho">
            <div class="list-group top25">
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled "> <i class="fa fa-shopping-cart right20" aria-hidden="true"></i> MEU CARRINHO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg active"> <i class="fa fa-user right25" aria-hidden="true"></i> MINHA CONTA</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-credit-card right25" aria-hidden="true"></i> PAGAMENTO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-check right25" aria-hidden="true"></i> FINALIZADO</a>
            </div>
          </div>



          <div class="col-xs-9">

            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->
            <div class="col-xs-6">
              <div class="produtos_destaques">
                <div class="col-xs-12 padding0">
                  <blockquote>
                    <h2><b>JÁ SOU CADASTRADO</b></h2>
                  </blockquote>
                </div>
              </div>

              <?php
              # ==============================================================  #
              # VERIFICO SE E PARA EFETUAR O LOGIN
              # ==============================================================  #
              if(isset($_POST[btn_logar])):

                if($obj_usuario->verifica_usuario($_POST[email], $_POST[senha])):

                  if (isset($_SESSION[produtos])) {
                    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
                  }else{
                    Util::script_location(Util::caminho_projeto()."/produtos");
                  }

                else:
                  Util::alert_bootstrap("Usuário ou senha inválido.");
                endif;

              endif;
              ?>

              <div class="col-xs-12  fundo-formulario">
                <form class="form-inline FormLogin " role="form" method="post" enctype="multipart/form-data">

                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    </div>
                  </div>

                  <div class="col-xs-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">
                    </div>
                  </div>


                  <div class="col-xs-6 top15">
                    <a class="btn btn-cinza br0 btn-lg" href="<?php echo Util::caminho_projeto() ?>/recuperar-senha" title="Esqueci minha senha">
                      Esqueci minha senha
                    </a>
                  </div>

                  <div class="col-xs-6 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn_finalizar btn-lg br0" name="btn_logar">
                        ENTRAR
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->



            <!--  ==============================================================  -->
            <!-- NAO SOU CADASTRADO-->
            <!--  ==============================================================  -->
            <div class="col-xs-6">
              <div class="produtos_destaques">
                <div class="col-xs-12 padding0">
                  <blockquote>
                    <h2><b>CRIA NOVA CONTA</b></h2>
                  </blockquote>
                </div>
              </div>

              <div class="fundo-formulario">
                <?php
                # ==============================================================  #
                # VERIFICO SE E PARA CADASTRAR O USUARIO
                # ==============================================================  #
                if(isset($_POST[btn_cadastrar])):

                  if($obj_usuario->cadastra_usuario($_POST) != false)
                  {
                    Util::script_location(Util::caminho_projeto() . "/endereco-entrega");
                  }
                  else
                  {
                    Util::alert_bootstrap("Esse email já está cadastrado");
                  }

                endif;
                ?>
                <form class="form-inline FormCadastro pt30" role="form" method="post" enctype="multipart/form-data">

                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="nome" class="form-control fundo-form1 input-lg input100" placeholder="NOME">
                    </div>
                  </div>

                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    </div>
                  </div>



                  <div class="col-xs-6 top10">
                    <div class="form-group  input100">
                      <input type="text" name="tel_residencial" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                    </div>
                  </div>

                  <div class="col-xs-6 top10">
                    <div class="form-group  input100">
                      <input type="text" name="tel_celular" class="form-control fundo-form1 input-lg input100" placeholder="CELULAR">
                    </div>
                  </div>

                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="endereco" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">
                    </div>
                  </div>

                  <div class="col-xs-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="numero" class="form-control fundo-form1 input-lg input100" placeholder="NÚMERO">
                    </div>
                  </div>

                  <div class="col-xs-7 top10">
                    <div class="form-group  input100">
                      <input type="text" name="complemento" class="form-control fundo-form1 input-lg input100" placeholder="COMPLEMENTO">
                    </div>
                  </div>

                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="bairro" class="form-control fundo-form1 input-lg input100" placeholder="BAIRRO">
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-7 top10">
                    <div class="form-group  input100">
                      <input type="text" name="cidade" class="form-control fundo-form1 input-lg input100" placeholder="CIDADE">
                    </div>
                  </div>

                  <div class="col-xs-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="uf" class="form-control fundo-form1 input-lg input100" placeholder="UF">
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">
                    </div>
                  </div>

                  <div class="col-xs-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha2" class="form-control fundo-form1 input100 input-lg" placeholder="CONFIRMAR SENHA">
                    </div>
                  </div>

                  <div class="col-xs-12 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn_finalizar btn-lg br0" name="btn_cadastrar">
                        CADASTRAR
                      </button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!-- NAO SOU CADASTRADO-->
            <!--  ==============================================================  -->







          </div>
        </div>
        <!-- ======================================================================= -->
        <!--  DESCRICAO -->
        <!-- ======================================================================= -->


      </div>
    </div>
  </div>













  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(document).ready(function() {
  $('.FormLogin').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      senha: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },



    }
  });
});
</script>


<script>
$(document).ready(function() {
  $('.FormCadastro').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      tel_residencial: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      tel_celular: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      numero: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      uf: {
        validators: {
          notEmpty: {

          }
        }
      },

      senha: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha2',
            message: 'As senhas não sào iguais'
          }
        }
      },
      senha2: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha',
            message: 'As senhas não sào iguais'
          }
        }
      }

    }
  });
});
</script>
