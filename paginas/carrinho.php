<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();

// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];







# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):

  if(!isset($_SESSION[usuario])):
    Util::script_location(Util::caminho_projeto() . "/autenticacao");
  else:
    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
  endif;


endif;



# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if(isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

  $obj_carrinho->atualiza_itens($_POST['qtd']);
  //$_SESSION[id_cidade] = $_POST[cidade];

endif;


# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

 $action = base64_decode($_GET[action]);
  $id = base64_decode($_GET[id]);

  //  ESCOLHO A OPCAO
  switch($action):

    case 'del':
    $obj_carrinho->del_item($id);
    break;

  endswitch;

endif;



# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] == 'add'):

  $obj_carrinho->add_item($_GET['idproduto']);

endif;





?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


  <script type="text/javascript">
  $(document).ready(function(){
    $('#cidade').change(function(){
      $('#bairro').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_taxas.php?id='+$('#cidade').val());
    });
  });
  </script>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>




<body class="bg-interna">


  <?php
  
  # ==============================================================  #
  # VERIFICO SE E PARA VERIFICAR A TAXA DE ENTREGA
  # ==============================================================  #
  if(isset($_POST[btn_taxa_entrega])):

    $_SESSION[local_entrega] = $obj_carrinho->verifica_local_entrega($_POST[cep]);

    if($_SESSION[local_entrega] == false){
      Util::alert_bootstrap("<h6>Infelizmente não efetuamos entrega em sua região.</h6> <p> </p> ");
    }else{
      $_SESSION[dados_entrega] = $obj_carrinho->busca_endereco($_POST[cep]);
    }
  endif;


  ?>










  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MEU CARRINHO</span></h1>
        <h4>COMPRE AGORA - PAGAMENTO ONLINE</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">MEU CARRINHO</li>
        </ol>
      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <div class="col-xs-3 lista-passos-carrinho">
            <div class="list-group top25">
              <a href="javacript:void(0);" class="list-group-item btn-lg active"> <i class="fa fa-shopping-cart right20" aria-hidden="true"></i> MEU CARRINHO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-user right25" aria-hidden="true"></i> MINHA CONTA</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-credit-card right25" aria-hidden="true"></i> PAGAMENTO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-check right25" aria-hidden="true"></i> FINALIZADO</a>
            </div>
          </div>


          <div class="col-xs-9 titulo_carrinho">

            <h3>ITENS SELECIONADOS</h3>


            <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto_final" id="form_produto_final" >


              <!--  ==============================================================  -->
              <!-- CARRINHO-->
              <!--  ==============================================================  -->
              <div class="tb-lista-itens top30">


                <?php if(count($_SESSION[produtos]) == 0): ?>
                  <div class="alert alert-danger">
                    <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
                  </div>

                  <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_comprar1 btn_continuar top10 btn-lg">
                    <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR COMPRANDO <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                  </a>
                </div>

              <?php else: ?>




                <table class="table">
                  <tbody>



                    <?php foreach($_SESSION[produtos] as $key=>$dado):  ?>
                      <tr>
                        <td>
                          <?php $obj_site->redimensiona_imagem("../uploads/$dado[imagem]", 50, 50, array('alt'=>$dado[titulo])); ?>
                        </td>
                        <td align="left" class="col-xs-9">
                          <h6 class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></h6>
                        </td>
                        <td class="text-center">
                          <i>QTD.</i>
                          <input type="number" class="input-lista-prod-orcamentos" min="1" name="qtd[]" value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">

                          <input name="idproduto[]" type="hidden" value="<?php echo $_SESSION[produtos][idproduto]; ?>"  />
                        </td>

                        <td class="col-xs-3 text-center">
                          <div class="text-center">VALOR</div>
                          <h4>R$ <?php echo Util::formata_moeda($dado[preco]) ?></h4>
                        </td>

                        <td class="text-center">
                          <i>REMOVE</i>

                          <a class="btn btn-vermelho" href="?id=<?php echo base64_encode($key) ?>&action=<?php echo base64_encode("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/remove.png" alt="">
                          </a>
                        </td>

                      </tr>
                      <?php $total += $dado[preco] * $dado[qtd]; ?>
                    <?php endforeach; ?>


                  </tbody>
                </table>



                <div class="col-xs-12 padding0 carrinho_total">
                  <!--  ==============================================================  -->
                  <!-- CAL CARRINHO -->
                  <!--  ==============================================================  -->
                  <div class="media">
  
                    <div class="col-xs-4 col-xs-offset-3 ">
                      <p class="top15 pull-right text-success"><strong>CALCULAR TAXA DE ENTREGA</strong></p>
                    </div> 

                    <div class="col-xs-3">
                      <div class="form-group ">
                          <input type="text" maxlength="8" name="cep" class="form-control fundo-form1 input-lg " placeholder="CEP" value="<?php echo $_SESSION[dados_entrega][cep] ?>">
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="form-group ">
                        <input type="submit" name="btn_taxa_entrega" id="btn_taxa_entrega" class="btn btn_finalizar" value="ok"  />
                      </div>
                    </div>



                      <div class="clearfix"></div>

                      <div class="col-xs-12">
                        <?php if(isset($_SESSION[produtos]) == 0): ?>
                          <p class="top15 pull-right">
                            TAXA DE ENTREGA: R$ 0,00</p>
                          <?php else: ?>
                            <p class="top15 pull-right">
                              TAXA DE ENTREGA:
                              <?php                              
                                if ($_SESSION[local_entrega][valor] == 0) {
                                  echo "<span class='text-success'><b>GRÁTIS</b></span>";
                                }else{
                                  echo '<span class="text-success"><b>R$ '.Util::formata_moeda($_SESSION[local_entrega][valor] ).'</b></span>';
                                }
                              ?>
                            </p>
                          <?php endif; ?>
                        </div>

                        <div class="clearfix"></div>


                        <p class="col-xs-12 top15 bottom20 text-right">
                          <b>VALOR TOTAL: R$ <?php echo Util::formata_moeda($total + $_SESSION[local_entrega][valor]) ?></b>
                        </p>

                        <div class="clearfix"></div>

                        <?php if(count($_SESSION[produtos]) > 0): ?>

                          <div class="col-xs-4  text-center">
                            <input class="right20 left20 btn btn_finalizar btn-lg" type="submit" name="btn_atualizar" id="btn_atualizar" value="ATUALIZAR CARRINHO"  />
                          </div>

                        <?php endif; ?>




                        <div class="col-xs-3 col-xs-offset-5 text-right relativo pg0">
                          <?php if(!empty($_SESSION[dados_entrega][cep]) ): ?>
                            <i class="fa fa-shopping-cart finalizar right5" aria-hidden="true"></i>
                            <input type="submit" name="btn_enviar" id="btn_enviar" class="btn btn_finalizar btn-lg input100" value="FINALIZAR PEDIDO"  />
                          <?php endif; ?>

                          <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_comprar1 btn_continuar top10 btn-lg right10">
                            <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR NO SITE <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                          </a>

                        </div>



                        <input type="hidden" name="action" value="atualiza_qtd" />

                        <!--  ==============================================================  -->
                        <!-- CAL CARRINHO -->
                        <!--  ==============================================================  -->

                      </div>
                      </div>

                      <!--  ==============================================================  -->
                      <!-- CARRINHO-->
                      <!--  ==============================================================  -->

                    <?php endif; ?>



                  </form>

                </div>

              </div>
            </div>

          </div>
          <!-- ======================================================================= -->
          <!--  DESCRICAO -->
          <!-- ======================================================================= -->

          <div class="container-fluid pagamentos">
          </div>


          <!-- ======================================================================= -->
          <!-- rodape    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/rodape.php') ?>
          <!-- ======================================================================= -->
          <!-- rodape    -->
          <!-- ======================================================================= -->



        </body>

        </html>
