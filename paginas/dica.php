<?php
// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>DICAS</span></h1>
        <h4>IMPORTANTE PARA SEU VEÍCULO</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">DICAS</li>
        </ol>
      </div>
    </div>
  </div>



  <div class="container-fluid bg_dica">
    <div class="row">


      <div class="container">
        <div class="row">
          <!-- ======================================================================= -->
          <!-- DESCRICAO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-xs-7 top25">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 457, 178, array("class"=>"bottom20", "alt"=>"$row[titulo]")) ?>
            <h5><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h5>
            <div class=" top50"><p><?php Util::imprime($dados_dentro[descricao]); ?></p></div>
          </div>
          <!-- ======================================================================= -->
          <!-- DESCRICAO GERAL    -->
          <!-- ======================================================================= -->

          <!-- ======================================================================= -->
          <!-- OUTRAS DICAS    -->
          <!-- ======================================================================= -->
          <div class="col-xs-4 col-xs-offset-1  top100 padding0">
            <?php
            $i = 0;
            $result = $obj_site->select("tb_dicas","order by rand() limit 2");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                ?>


                <div class="col-xs-12 top25 dicas relativo">
                  <a class="btn btn_default btn-lg" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">MAIS DETALHES</a>
                  <a class="" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 176, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                    <h6 class="top25 text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
                    <div class="top20 dica_desc"><p><?php Util::imprime($row[descricao],300) ?></p></div>
                  </a>
                </div>
                <?php
                if ($i == 0) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  $i++;
                }
              }
            }
            ?>
          </div>
          <!-- ======================================================================= -->
          <!-- OUTRAS DICAS    -->
          <!-- ======================================================================= -->


        </div>
      </div>

    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/veja.php') ?>
  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
