<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  10px center no-repeat;
}
</style>

<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>EMPRESA</span></h1>
        <h4>QUEM SOMOS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">EMPRESA</li>
        </ol>
      </div>
    </div>
  </div>



  <div class="container-fluid fundo_empresa">
    <div class="row ">
      <div class="container">
        <div class="row top70">
          <div class="col-xs-6 top10">
            <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
            <div class="col-xs-6 text-right padding0">
              <h1><?php Util::imprime($row1[titulo]); ?></h1>
              <h1>GOIÂNIA</h1>
            </div>
          </div>
          <div class="col-xs-6">
            <p><?php Util::imprime($row1[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="container-fluid container_onde">
    <div class="row">
      <div class="container">
        <div class="row relativo">

          <div class="titulo_home col-xs-6 text-right">
            <h1 class="top20 right50">ONDE ESTAMOS</h1>
          </div>
          <!--  ==============================================================  -->
          <!-- mapa -->
          <!--  ==============================================================  -->
          <div class="col-xs-10 col-xs-offset-2 padding0 ">
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="376" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <!--  ==============================================================  -->
          <!-- mapa -->
          <!--  ==============================================================  -->
          <img class="cara" src="<?php echo Util::caminho_projeto() ?>/imgs/cara_somos.png" alt="">
        </div>
      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/veja.php') ?>
  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
