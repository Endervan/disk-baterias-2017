<?php
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();
//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");
endif;





# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if(isset($_POST[btn_cadastrar])):
  $obj_carrinho->armazena_mensagem_endereco_entrega($_POST);

  $caminho = Util::caminho_projeto(). "/pagamento";
  header("location: $caminho ");

endif;




// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];








?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MINHA CONTA</span></h1>
        <h4>CADASTRE SEUS DADOS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">MINHA CONTA</li>
        </ol>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <div class="col-xs-3 lista-passos-carrinho">
            <div class="list-group top25">
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled "> <i class="fa fa-shopping-cart right20" aria-hidden="true"></i> MEU CARRINHO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-user right25" aria-hidden="true"></i> MINHA CONTA</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg  active"> <i class="fa fa-credit-card right25" aria-hidden="true"></i> ENDEREÇO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-check right25" aria-hidden="true"></i> PAGAMENTOS</a>
            </div>
          </div>




          <div class="col-xs-9">

            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->
            <div class="col-xs-12">
              <div class="produtos_destaques">
                <div class="col-xs-12 padding0 titulo_carrinho">
                  <blockquote>
                    <h3><b>ENDEREÇO PARA ENTREGA</b></h3>
                  </blockquote>
                </div>
              </div>



              <div class="col-xs-12  fundo-formulario">
                <form class="form-inline FormCadastro " role="form" method="post" enctype="multipart/form-data">

                    <div class="col-xs-12 top10">
                        <div class="form-group  input100">
                            <input type="text" name="nome_contato" class="form-control fundo-form1 input-lg input100" placeholder="NOME">
                        </div>
                    </div>

                    <div class="col-xs-4 top10">
                        <div class="form-group  input100">
                            <input type="text" name="telefone_contato" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                        </div>
                    </div>

                    <div class="col-xs-4 top10">
                        <div class="form-group  input100">
                            <input type="text" name="celular_contato" class="form-control fundo-form1 input-lg input100" placeholder="CELULAR">
                        </div>
                    </div>

                    <div class="col-xs-4 top10">
                        <div class="form-group  input100">
                            <input type="number" min="8" maxlength="8" name="cep_entrega" class="form-control fundo-form1 input-lg input100" disabled value="<?php echo $_SESSION[dados_entrega][cep] ?>">
                        </div>
                    </div>



                  <div class="col-xs-8 top10">
                    <div class="form-group  input100">
                      <input type="text" name="endereco_entrega" class="form-control fundo-form1 input-lg input100" disabled value="<?php  Util::imprime( $_SESSION[dados_entrega][logradouro]) ?>">
                    </div>
                  </div>

                  <div class="col-xs-4 top10">
                    <div class="form-group  input100">
                      <input type="text" name="numero_entrega" class="form-control fundo-form1 input-lg input100" placeholder="NÚMERO">
                    </div>
                  </div>


                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="complemento_entrega" class="form-control fundo-form1 input-lg input100" placeholder="COMPLEMENTO">
                    </div>
                  </div>


                  <div class="col-xs-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="ponto_referencia" class="form-control fundo-form1 input-lg input100" placeholder="REFERÊNCIA">
                    </div>
                  </div>



                  <div class="col-xs-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="bairro" disabled class="form-control fundo-form1 input-lg input100" value="<?php  Util::imprime( $_SESSION[dados_entrega][bairro]) ?>">
                    </div>
                  </div>

                  <div class="col-xs-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="cidade" disabled class="form-control fundo-form1 input-lg input100" value="<?php Util::imprime( $_SESSION[dados_entrega][localidade] ) ?>">
                    </div>
                  </div>

                  <div class="col-xs-2 top10">
                    <div class="form-group  input100">
                      <input type="text" name="uf" disabled class="form-control fundo-form1 input-lg input100" value="<?php  Util::imprime( $_SESSION[dados_entrega][uf]) ?>">
                    </div>
                  </div>




                  <div class="col-xs-6 titulo_carrinho top15 ">
                    <a class="btn btn-cinza btn-lg" href="<?php echo Util::caminho_projeto() ?>/carrinho"><h3>TROCAR LOCAL DE ENTREGA</h3></a>
                  </div>


                  <div class="col-xs-6 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn_finalizar btn-lg br0" name="btn_cadastrar">
                        ENVIAR
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->

          </div>





        </div>
      </div>
      <!-- ======================================================================= -->
      <!--  DESCRICAO -->
      <!-- ======================================================================= -->



      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>



    <script>
    $(document).ready(function() {
      $('.FormCadastro').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nome: {
            validators: {
              notEmpty: {

              }
            }
          },


          nome_contato: {
            validators: {
              notEmpty: {

              }
            }
          },

          celular_contato: {
            validators: {
              notEmpty: {

              }
            }
          },

          email: {
            validators: {
              notEmpty: {

              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },
          tel_residencial: {
            validators: {
              notEmpty: {

              },
              phone: {
                country: 'BR',
                message: 'Telefone inválido'
              }
            }
          },
          tel_celular: {
            validators: {
              notEmpty: {

              },
              phone: {
                country: 'BR',
                message: 'Telefone inválido'
              }
            }
          },
          endereco_entrega: {
            validators: {
              notEmpty: {

              }
            }
          },

          cep_entrega: {
            validators: {
              notEmpty: {

              }
            }
          },

          
          numero_entrega: {
            validators: {
              notEmpty: {

              }
            }
          },
          bairro: {
            validators: {
              notEmpty: {

              }
            }
          },
          cidade: {
            validators: {
              notEmpty: {

              }
            }
          },
          uf: {
            validators: {
              notEmpty: {

              }
            }
          },



        }
      });

    });
    </script>
