<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 0);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <?php require_once('./includes/topo.php') ?>


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- FAIXA LINHAS, CATEGORIAS E PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_barra_produtos">
    <div class="row">

      <div class="container">
        <div class="row">

          <div class="titulo_home col-xs-12 top25">
            <h2 class=""><span>BATERIAS EM DESTAQUES</span></h2>
          </div>


          <div class="col-xs-3 padding0 top10">

              <div class="nossa-liha-index">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/titulo-nossas-linhas-produtos.png" alt="">

                <div class="list-group top20">
                  <?php
                  $i = 0;
                  $result = $obj_site->select("tb_categorias_produtos");
                  if (mysql_num_rows($result) > 0) {
                    while($row = mysql_fetch_array($result)){
                      ?>
                      <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo Util::imprime($row[url_amigavel]) ?>" class="list-group-item text-uppercase">
                        <i class="fa fa-ellipsis-v right10"></i>
                        BATERIAS - <?php echo Util::imprime($row[titulo]) ?>
                      </a>
                      <?php
                    }
                    $i++;
                  }
                  ?>
                </div>
              </div>

          </div>



          <div class="col-xs-9 padding0">
            <?php
            $i = 0;
            $result = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' order by rand() limit 6");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                ?>
                <div class="col-xs-4 lista-produto">
                  <div class="grid">
                    <figure class="effect-julia">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 275, 200, array("class"=>"input100", "alt"=>"")) ?>
                      <figcaption>
                        <div class="hover">
                          <p class="col-xs-6">
                            <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produto/<?php echo Util::imprime($row[url_amigavel]) ?>">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                              <br>
                              SAIBA MAIS
                            </a>
                          </p>


                          <p class="col-xs-6">
                            <a class="btn " href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              <br>
                              ORÇAR
                            </a>
                          </p>
                        </div>
                      </figcaption>
                    </figure>

                  </div>

                  <div class="col-xs-12 top20 desc_titulo text-uppercase"><h1><?php echo Util::imprime($row[titulo]) ?></h1></div>
                  <h1 class="col-xs-12 bottom20"><?php echo Util::imprime($row[modelo]) ?></h1>
                  <!-- <h6 class="col-xs-12 text-right bottom20">R$ <?php // echo Util::formata_moeda($row[preco]) ?></h6> -->

                  <a class="btn btn_mais_prod" href="<?php echo Util::caminho_projeto() ?>/produto/<?php echo Util::imprime($row[url_amigavel]) ?>" role="button">MAIS DETALHES</a>
                  <a class="btn btn_mais_prod btn_mais_carrinho pull-right" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">ORÇAMENTO <i class="fa fa-shopping-cart left10"></i></a>

                </div>
                <?php

                if ($i == 2) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  ++$i;
                }
              }
            }
            ?>
          </div>

          <div class="col-xs-12 padding0">
            <a class="btn btn-lg br0 btn_verde top20 pull-right" href="<?php echo Util::caminho_projeto() ?>/produtos">VER TODOS OS PRODUTOS</a>
          </div>
        </div>
      </div>


    </div>

  </div>
  <!-- ======================================================================= -->
  <!-- FAIXA LINHAS, CATEGORIAS E PRODUTOS    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid container_fundo_servicos">
    <div class="row ">


      <div class="container">
        <div class="row">
          <div class="titulo_home col-xs-7 col-xs-offset-5">
            <h1 class="">NOSSOS SERVIÇOS</h1>
          </div>

          <div class="clearfix"></div>

          <?php
          $i = 0;
          $result = $obj_site->select("tb_servicos","ORDER BY RAND() limit 3");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              ?>


              <div class="col-xs-4 servicos top35 ">
                <a class="relativo" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 227, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  <h6 class="top25 text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
                  <a class="" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" role="button">
                    <div class="mais_servicos">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/mais_servicos.png" alt="">
                    </div>
                  </a>
                </a>


              </div>
              <?php
              if ($i == 2) {
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }
            }
          }
          ?>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->
<div class="container-fluid container_fundo_empresa">
  <div class="row ">

    <div class="container top30">
      <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="titulo_home col-xs-12">
          <h1><?php Util::imprime($row1[titulo]); ?></h1>
        </div>

        <div class="col-xs-5 dicas col-xs-offset-7">
          <div class="top20 empresa_desc"><p><?php Util::imprime($row1[descricao],300) ?></p></div>
          <a class="btn btn_default btn-lg" href="<?php echo Util::caminho_projeto() ?>/empresa" role="button">MAIS DETALHES</a>

        </div>

      </div>
    </div>
  </div>
</div>
</div>

<!-- ======================================================================= -->
<!-- EMPRESA HOME -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DICAS HOME -->
<!-- ======================================================================= -->
<div class="container-fluid container_fundo_dicas">
  <div class="row ">


    <div class="container top30">
      <div class="row">
        <div class="titulo_home col-xs-5 col-xs-offset-7">
          <h1 class="left65">DICAS</h1>
        </div>

        <?php
        $i = 0;
        $result = $obj_site->select("tb_dicas","ORDER BY RAND() limit 2");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            ?>
            <!-- ======================================================================= -->
            <!--ITEM 01 REPETI ATE 6 ITENS   -->
            <!-- ======================================================================= -->
            <div class="col-xs-4 dicas relativo">
              <a class="btn btn_default btn-lg" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">MAIS DETALHES</a>
              <a class="" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 176, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                <h6 class="top25 text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
                <div class="top20 dica_desc"><p><?php Util::imprime($row[descricao],300) ?></p></div>
              </a>
            </div>
            <?php
            if ($i == 2) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }
        }
        ?>

      </div>
    </div>
  </div>
</div>
</div>

<!-- ======================================================================= -->
<!-- DICAS HOME -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'none',  //  Navigation type, can be 'bullets', 'thumbnails', 'tabs' or 'none'
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption: true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
      delay: 4000
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
