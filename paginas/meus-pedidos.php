<?php

# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):

  //  ARAMAZENO O LOCAL DE ENTREGA
  $_SESSION[id_bairro_entrega] = $_POST[bairro];
  $_SESSION[id_cidade] = $_POST[cidade];


  if(!isset($_SESSION[usuario])):
    Util::script_location(Util::caminho_projeto() . "/autenticacao");
  else:
    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
  endif;


endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",15) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MINHA CONTA</span></h1>
        <h4>RESUMOS PEDIDOS GERAL</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">PEDIDOS</li>
        </ol>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <!-- ======================================================================= -->
          <!-- passo    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/passo-a-passo.php') ?>
          <!-- ======================================================================= -->
          <!-- passo    -->
          <!-- ======================================================================= -->



          <div class="col-xs-9">


            <div class="col-xs-6">
              <h4>Data</h4>
            </div>
            <div class="col-xs-6">
              <h4>Pedido Número</h4>
            </div>

            <div class="clearfix"></div>

            <div class="panel-group top15" id="accordion" role="tablist" aria-multiselectable="true">



              <?php
              $result = $obj_site->select("tb_vendas", "and id_usuario = ".$_SESSION[usuario][idusuario]." order by hora desc");
              if (mysql_num_rows($result) == 0) {?>

                <div class="alert alert-danger">
                  <h1 class=''>NAO FOI REALIZADO NENHUM PEDIDO<br></h1>
                </div>
                <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_comprar1 btn_continuar top10 btn-lg">
                  <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR COMPRANDO <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                </a>

                <?php
              }else{
                $i = 0;
                while($row = mysql_fetch_array($result)){
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading-<?php echo $i; ?>">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $i; ?>">
                          <span class="col-xs-7">
                            <?php echo Util::formata_data($row[data]) ?> às <?php echo Util::imprime($row[hora], 5) ?>
                          </span>
                          <?php echo Util::imprime($row[idvenda]) ?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse-<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $i; ?>">
                      <div class="panel-body">
                        <table class="table table-condensed">

                          <thead>
                            <tr>
                              <th>TÍTULO</th>
                              <th class="text-center">QTD</th>
                              <th class="text-right">VALOR</th>
                            </tr>
                          </thead>


                          <tbody>
                            <?php
                            $b = 0;
                            $result1 = $obj_site->select("tb_vendas_produtos", "and id_venda = '$row[idvenda]' ");
                            if (mysql_num_rows($result1) > 0) {
                              while($row1 = mysql_fetch_array($result1)){
                                ?>
                                <tr>
                                  <th scope="row"><?php echo Util::imprime($row1[titulo]) ?></th>
                                  <th class="text-center"><?php echo Util::imprime($row1[qtd]) ?></th>
                                  <th class="text-right"><?php echo Util::formata_moeda($row1[valor]) ?></th>
                                </tr>
                                <?php
                                $total_compra = $row1[valor]*$row1[qtd];
                              }
                              $b++;
                            }
                            ?>

                            <tr>
                              <th scope="row"></th>
                              <th class="text-center">TOTAL</th>
                              <th class="text-right"><?php echo Util::formata_moeda($total_compra) ?></th>
                            </tr>

                          </tbody>


                        </table>
                      </div>
                    </div>
                  </div>
                  <?php
                  $i++;
                }
              }
              ?>





            </div>















          </div>





        </div>
      </div>
      <!-- ======================================================================= -->
      <!--  DESCRICAO -->
      <!-- ======================================================================= -->
















      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>


    <?php require_once('./includes/js_css.php') ?>


    <script>
    $(document).ready(function() {
      $('.FormLogin').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

          senha: {
            validators: {
              notEmpty: {

              }
            }
          },
          email: {
            validators: {
              notEmpty: {

              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },



        }
      });
    });
    </script>


    <script>
    $(document).ready(function() {
      $('.FormCadastro').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nome: {
            validators: {
              notEmpty: {

              }
            }
          },
          email: {
            validators: {
              notEmpty: {

              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },
          tel_residencial: {
            validators: {
              notEmpty: {

              },
              phone: {
                country: 'BR',
                message: 'Telefone inválido'
              }
            }
          },
          tel_celular: {
            validators: {
              notEmpty: {

              },
              phone: {
                country: 'BR',
                message: 'Telefone inválido'
              }
            }
          },
          endereco: {
            validators: {
              notEmpty: {

              }
            }
          },
          numero: {
            validators: {
              notEmpty: {

              }
            }
          },
          bairro: {
            validators: {
              notEmpty: {

              }
            }
          },
          cidade: {
            validators: {
              notEmpty: {

              }
            }
          },
          uf: {
            validators: {
              notEmpty: {

              }
            }
          },

          senha: {
            validators: {
              notEmpty: {

              },
              identical: {
                field: 'senha2',
                message: 'As senhas não sào iguais'
              }
            }
          },
          senha2: {
            validators: {
              notEmpty: {

              },
              identical: {
                field: 'senha',
                message: 'As senhas não sào iguais'
              }
            }
          }

        }
      });
    });
    </script>
