<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




    <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>ENVIE SEU</span></h1>
        <h4>ORÇAMENTO</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">MEU CARRINHO</li>
        </ol>
      </div>
    </div>
  </div>


  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      
  <form class="form-inline FormOrcamento" role="form" method="post" enctype="multipart/form-data">
    <div class="container bottom50 top30">
        <div class="row">


        <!-- ======================================================================= -->
        <!-- CARRINHO  -->
        <!-- ======================================================================= -->
        <div class="col-xs-5 tabela_carrinho pt25">
          <h5 >ITENS SELECIONADOS</h5>
          <table class="table table-condensed col-xs-12">
            <tbody>

              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0){
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <tr>
                    <td>
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </td>
                    <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
                    <td class="text-center">
                      QTD.<br>
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-times-circle fa-2x top20"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>

              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0){
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <tr>
                    <td>
                      <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </td>
                    <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
                    <td class="text-center">
                      QTD.<br>
                      <input type="number" min="1" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-times-circle fa-2x top20"></i>
                      </a>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- ======================================================================= -->
        <!-- CARRINHO  -->
        <!-- ======================================================================= -->



      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
      <div class="col-xs-7 input-formularios">

        <div class="fundo_formulario pt25">
          <h5 class="left15">CONFIRME SEUS DADOS</h5>
          <!-- formulario orcamento -->

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME *">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL *">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>


          <div class="clearfix"></div>

          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE *">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="localidade" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">
                <span class="fa fa-home form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="top15">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="OBSERVAÇÕES"></textarea>
                <span class="fa fa-pencil form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <!-- formulario orcamento -->
          <div class="col-xs-12 text-right">
            <div class="text-left top15">
              <span >*campo obrigatório</span>
            </div>
            <div class="">
              <button type="submit" class="btn btn-amarelo" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>


      </div>
      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->


    </div>
  </div>
</form>

</div>
</div>

<!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->
  <div class="container-fluid container_mapa bottom40">
    <div class="row ">

      <div class="container top30">
        <div class="row">

          <div class="col-xs-3 text-right top15">
            <h4 class="right15">NOSSA</h4>
            <h2 class="right15 bottom5 "><span>LOCALIZAÇÃO</span></h2>
          </div>
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="371" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->




<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_POST[idtiposervico][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
  $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Nome: $_POST[nome] <br />
  Email: $_POST[email] <br />
  Telefone: $_POST[telefone] <br />
  Localidade: $_POST[localidade] <br />
  Mensagem: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  </table>

  ";


  if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]))) {
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]));
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}

?>





<script>
$(document).ready(function() {
  $('.FormOrcamento').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu nome'

          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira seu email'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Informe um telefone'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      localidade1: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
