<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto()."/produtos";
  header("location: $caminho");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto()."/produtos";
  header("location: $caminho");
endif;


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


  <script type="text/javascript">
  $(document).ready(function(){
    $('#cidade').change(function(){
      $('#bairro').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_taxas.php?id='+$('#cidade').val());
    });
  });
  </script>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>MINHA CONTA</span></h1>
        <h4>ESCOLHA SEU TIPO DE PAGAMENTO</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">PAGAMENTOS</li>
        </ol>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">

          <div class="col-xs-3 lista-passos-carrinho">
            <div class="list-group top25">
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled "> <i class="fa fa-shopping-cart right20" aria-hidden="true"></i> MEU CARRINHO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> <i class="fa fa-user right25" aria-hidden="true"></i> MINHA CONTA</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg  disabled"> <i class="fa fa-credit-card right25" aria-hidden="true"></i> ENDEREÇO</a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled active"> <i class="fa fa-check right25" aria-hidden="true"></i> PAGAMENTOS</a>
            </div>
          </div>




          <div class="col-xs-9">

            <?php
            if(isset($_POST[tipo_pagamento])):

              # ==============================================================  #
              # VERIFICO SE FOI ENVIADO A OS DADOS
              # ==============================================================  #
              if(isset($_POST[btn_cadastrar])):

                echo $obj_carrinho->finaliza_venda($_POST[tipo_pagamento]);

              endif;

            else:
              ?>
              <form name="form_pagamento" id="form_pagamento" class="form_mensagem formulario1" action="" method="post" >
                <div class="titulo_carrinho">
                  <h3>Selecione o tipo de pagamento desejado.</h3>
                </div>

                <div class="radio col-xs-12 top10">
                  <label>
                    <p>
                    <input type="radio" name="tipo_pagamento" id="paypal" class="validate[required] top0" value="paypal" >
                   PayPal</p>
                  </label>
                </div>

                <div class="radio col-xs-12 top10">
                  <label>
                    <input type="radio" name="tipo_pagamento" id="pagseguro" class="validate[required] top0" value="pagseguro" checked>
                  <p> PagSeguro</p>
                  </label>
                </div>

                <div class="radio col-xs-12 top10">
                  <label>
                    <input type="radio" name="tipo_pagamento" id="maquineta" class="validate[required] top0" value="maquineta" checked>
                  <p> Para pagamento no local - Via maquineta de cartão</p>
                  </label>
                </div>

                <input type="submit" name="btn_cadastrar" id="btn_cadastrar" class="btn top20 btn-lg btn_finalizar" value="ENVIAR" />
              </form>


              <?php
            endif;
            ?>


          </div>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!--  DESCRICAO -->
      <!-- ======================================================================= -->



      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>
