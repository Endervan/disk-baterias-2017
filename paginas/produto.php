<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>PRODUTOS</span></h1>
        <h4>CONFIRA NOSSA LINHA DE BATERIAS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
          <li class="active"><?php echo Util::imprime($dados_dentro[titulo]) ?></li>
        </ol>
      </div>
    </div>
  </div>


  <div class="container-fluid fundo_produto">
    <div class="row">

      <div class="container">
        <div class="row prod-detalhe">



          <div class="col-xs-5 col-xs-offset-1 top30">
            <?php require_once("./includes/slider_produtos_dentro.php"); ?>
          </div>


          <div class="col-xs-5 col-xs-offset-1 top30 ">
            <h3><span class="text-uppercase "><?php echo Util::imprime($dados_dentro[titulo]) ?></span></h3>
            <h4 class="top15"> MARCA : <?php echo Util::imprime($dados_dentro[marca]) ?></h4>
            <h4 class="top10"> DIMENSÃO : <?php echo Util::imprime($dados_dentro[dimensao]) ?></h4>
            <h4 class="top10"> AMPERES : <?php echo Util::imprime($dados_dentro[amperes]) ?></h4>
            <h4 class="top10"> BATERIA A BASE DE TROCA</h4>
            
            <div class="col-xs-12 right10 bottom20 top15 text-right">
              <a href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" class="btn btn-lg btn_comprar1">
                <img class="right10"src="<?php echo Util::caminho_projeto(0) ?>/imgs/icon_cart.png"  alt="">ADICIONAR AO ORÇAMENTO
              </a>
            </div>

            <div class="col-xs-12 padding0 barra_paypal1">
              <!-- <div class="col-xs-6 top20">
                <h3><span>R$ <?php //echo Util::formata_moeda($dados_dentro[preco]) ?></span> </h3>
              </div> -->
              <div class="col-xs-12 top15 text-center">
                <a href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" class="btn btn_comprar">
                  ADICIONAR AO ORÇAMENTO
                </a>
              </div>
            </div>

            <!-- TELEFONES -->
            <div class="col-xs-8 top20 pg0">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/img-televendas.jpg" class="pull-left top5" alt="">
                <div>
                  <p> 
                    <i class="fa fa-phone left5" aria-hidden="true"></i>
                    <?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?> 
                  </p>
                  <p>
                    <img style="width: 20px; margin-top: 3px;" class="pull-left left5" src="<?php echo Util::caminho_projeto() ?>/imgs/whats_topo.png" alt="">
                    <?php Util::imprime($config[ddd2]) ?> <?php Util::imprime($config[telefone2]) ?>
                  </p>
                </div>
            </div>
            <div class="col-xs-4 pg0 top20">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/cartoes.png" class="input100" alt="">
            </div>
            <!-- TELEFONES -->

          </div>

          <div class="clearfix"></div>


          <div class="col-xs-12">
            <p><?php echo Util::imprime($dados_dentro[descricao]) ?></p>

          </div>


        </div>
      </div>
    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/veja.php') ?>
  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
