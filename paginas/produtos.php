<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}


body {
  position: relative;
}
.affix {
  top: 0px;
  z-index: 9999 !important;
}

</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>PRODUTOS</span></h1>
        <h4>CONFIRA NOSSA LINHA DE BATERIAS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">PRODUTOS</li>
        </ol>
      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- FAIXA LINHAS, CATEGORIAS E PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_produtos">
    <div class="row">

      <div class="container">
        <div class="row">


          <div class="col-xs-3 padding0 top10">

            <div class="nossa-liha-index">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/titulo-nossas-linhas-produtos.png" alt="">

              <div class="list-group top20">
                <?php
                $i = 0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo Util::imprime($row[url_amigavel]) ?>" class="list-group-item text-uppercase">
                      <i class="fa fa-ellipsis-v right10"></i>
                      BATERIAS - <?php echo Util::imprime($row[titulo]) ?>
                    </a>
                    <?php
                  }
                  $i++;
                }
                ?>
              </div>
            </div>


          </div>



          <div class="col-xs-9 padding0">
            <?php
            $url1 = Url::getURL(1);

            //  FILTRA AS CATEGORIAS
            if (isset( $url1 )) {
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
            }

            //  FILTRA PELO TITULO
            if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
              $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
            endif;




            $i = 0;

            $result = $obj_site->select("tb_produtos", $complemento);
            if(mysql_num_rows($result) == 0){
              echo "
              <div class=' col-xs-12 text-center top30 '>
              <h2 class='btn-success' style='padding: 20px;'>Nenhum produto encontrado.</h2>
              </div>"
              ;
            }else{
              ?>
              <div class="col-xs-12 top20">
                <h6><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S).</h6>
              </div>
              <?php
              while($row = mysql_fetch_array($result)){
                ?>



                <div class="col-xs-4 lista-produto">
                  <div class="grid">
                    <figure class="effect-julia">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 275, 200, array("class"=>"input100", "alt"=>"")) ?>
                      <figcaption>
                        <div class="hover">
                          <p class="col-xs-6">
                            <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produto/<?php echo Util::imprime($row[url_amigavel]) ?>">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                              <br>
                              SAIBA MAIS
                            </a>
                          </p>

                          <p class="col-xs-6">
                          <a class="btn " href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              <br>
                              ORÇAR
                            </a>
                          </p>
                        </div>
                      </figcaption>
                    </figure>

                  </div>

                  <div class="col-xs-12 top20 desc_titulo text-uppercase"><h1><?php echo Util::imprime($row[titulo]) ?></h1></div>
                  <h1 class="col-xs-12 bottom20"><?php echo Util::imprime($row[modelo]) ?></h1>
                  <!-- <h6 class="col-xs-12 text-right bottom20">R$ <?php // echo Util::formata_moeda($row[preco]) ?></h6> -->

                  <a class="btn btn_mais_prod" href="<?php echo Util::caminho_projeto() ?>/produto/<?php echo Util::imprime($row[url_amigavel]) ?>" role="button">MAIS DETALHES</a>
                  <a class="btn btn_mais_prod btn_mais_carrinho pull-right" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">ORÇAMENTO <i class="fa fa-shopping-cart left10"></i></a>

                </div>
                <?php

                if ($i == 2) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  ++$i;
                }
              }
            }
            ?>
          </div>

          <div class="col-xs-12 padding0">
            <a class="btn btn-lg br0 btn_verde top20 pull-right" href="<?php echo Util::caminho_projeto() ?>/produtos">VER TODOS OS PRODUTOS</a>
          </div>
        </div>
      </div>


    </div>

  </div>
  <!-- ======================================================================= -->
  <!-- FAIXA LINHAS, CATEGORIAS E PRODUTOS    -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->




</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 280,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
