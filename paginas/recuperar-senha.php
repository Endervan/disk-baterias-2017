<?php

$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/autenticacao" );

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 15);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px  center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>RECUPERAR SENHA</span></h1>
        <h4>INFORME SEUS DADOS</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">RECUPERAR SENHA</li>
        </ol>
      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_fundo_carrinho">
    <div class="row">

      <div class="container bottom50 top30">
        <div class="row">



          <div class="col-xs-12 titulo_carrinho">

            <h3>ADICIONE SEU EMAIL CADASTRASTO PARA RECEBER NOVA SENHA</h3>
            <form class="form-inline FormLogin pt30" role="form"  method="post" enctype="multipart/form-data">

            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->
            <?php
            if(isset($_POST['btn_logar'])){
              if($obj_usuario->recupera_senha($_POST['email']) == true){
                Util::alert_bootstrap("
                <h5 class='text-success'>Sua nova senha foi enviado para o e-mail cadastrado.</h5>
                <br><br>
                <a href='".Util::caminho_projeto()."/autenticacao' title=''><h3>Clique aqui para efetuar login</h3></a>
                "); // se nao retornou mostra isso
                //Util::script_location(Util::caminho_projeto() . "/autenticacao");
                //header("location: index.php");
              }else{
                Util::alert_bootstrap("<p class='text-danger'>Não foi possível recuperar sua senha. E-mail não encontrado.</p>");
              }
            }
            ?>

            <div class="col-xs-12  fundo-formulario">

                <div class="col-xs-8 top10">
                  <div class="form-group  input100">
                    <input type="text"  name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL" required>
                  </div>
                </div>


                <div class="col-xs-2 padding0 text-right">
                  <div class="top10 bottom25">
                    <button type="submit" class="btn btn_finalizar input100" id="btn_logar" name="btn_logar">
                      ENVIAR
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->

          </div>

        </div>
      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>




<script>
$(document).ready(function() {
  $('.FormLogin').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      email: {
        validators: {
          notEmpty: {
            message: 'Adicione email Cadastrado'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },


    }
  });
});
</script>
