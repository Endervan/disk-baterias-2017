<?php
// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>SERVICOS</span></h1>
        <h4>GARANTIA E EQUIPE ESPECIALIZADA</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">SERVIÇOS</li>
        </ol>
      </div>
    </div>
  </div>



  <div class="container-fluid bg_dicas">
    <div class="row">


      <div class="container">
        <div class="row top30">
          <!-- ======================================================================= -->
          <!-- DESCRICAO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-xs-5 col-xs-offset-1 titulo_img">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 458, 238, array("class"=>"bottom10", "alt"=>"$row[titulo]")) ?>
            <h5 class="text-uppercase left40"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h5>
          </div>

          <div class="col-xs-6">
            <h3 class="text-uppercase"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h3>
            <h4 class="text-uppercase top20"><span><?php Util::imprime($dados_dentro[subtitulo]); ?></span></h4>
            <div class=" top30"><p><?php Util::imprime($dados_dentro[descricao]); ?></p></div>

            <a class="btn btn_servico top15 btn-lg pull-right" href="<?php echo Util::caminho_projeto() ?>/fale-conosco" role="button">FALE CONOSCO</a>

          </div>
          <!-- ======================================================================= -->
          <!-- DESCRICAO GERAL    -->
          <!-- ======================================================================= -->

        </div>
      </div>

    </div>
  </div>






  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/veja.php') ?>
  <!-- ======================================================================= -->
  <!-- veja tambem    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
