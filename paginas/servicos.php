<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px  center no-repeat;
}
</style>

<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>SERVICOS</span></h1>
        <h4>GARANTIA E EQUIPE ESPECIALIZADA</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">SERVICOS</li>
        </ol>
      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid container_fundo_geral">
    <div class="row ">


      <div class="container">
        <div class="row">


          <!-- ======================================================================= -->
          <!-- veja tambem    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_servicos.php') ?>
          <!-- ======================================================================= -->
          <!-- veja tambem    -->
          <!-- ======================================================================= -->


        </div>
      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->
<?php require_once('./includes/veja.php') ?>
<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 400,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
