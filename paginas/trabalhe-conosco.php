<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 10px center no-repeat;
}
</style>

<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->
      <div class="col-xs-11 col-xs-offset-1 titulo_home top175 bottom100">
        <h1><span>TRABALHE CONOSCO</span></h1>
        <h4>FAÇA PARTE DA NOSSA EQUIPE</h4>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO GERAL    -->
      <!-- ======================================================================= -->

      <div class="col-xs-12 mont">
        <ol class="breadcrumb ">
          <li><a href="<?php echo Util::caminho_projeto() ?>"><i class="fa fa-home right10"></i>HOME</a></li>
          <li class="active">TRABALHE CONOSCO</li>
        </ol>
      </div>
    </div>
  </div>


  <div class="container-fluid bg_fundo_contato">
    <div class="row">

      <div class="container">
        <div class="row">



          <div class="col-xs-5 padding0">

            <div class="col-xs-12 padding0">
              <div class="media top40">
                <div class="media-left media-middle">
                  <img class="media-object left5" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_email.png" alt="">
                </div>
                <div class="media-body">
                  <h3 class="media-heading ml-3"><span>PREENCHA SEUS DADOS</span></h3>
                </div>
              </div>
            </div>




            <div class="col-xs-2 top10 padding0">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone.png" alt="">
            </div>

            <div class="col-xs-10 padding0 telefones_fale top30">

              <div class="col-xs-6 media">
                <div class="media-left media-middle">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
                </div>
                <div class="media-body ">
                  <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?></span></h5>
                </div>
                <h6><span>TELEVENDAS</span></h6>
              </div>

              <?php if (!empty($config[telefone2])): ?>
                <div class="col-xs-6  padding0 media">
                  <div class="media-left media-middle">
                    <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/whats_topo.png" alt="">
                  </div>
                  <div class="media-body">
                    <h5 class="media-heading"> <span>  <?php Util::imprime($config[ddd2]) ?> <?php Util::imprime($config[telefone2]) ?></span></h5>
                  </div>
                  <h6><span>WHATSAPP</span></h6>
                </div>
              <?php endif; ?>

            </div>


            <div class="col-xs-12 padding0">

              <div class="media top10">
                <div class="media-left media-middle">
                  <img class="media-object left5" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_local.png" alt="">
                </div>
                <div class="media-body">
                  <p class="media-heading ml-3 text-uppercase"><?php Util::imprime($config[endereco]) ?></p>
                </div>
              </div>

            </div>


          </div>



          <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
            <div class="col-xs-7 padding0 fundo-formulario">
              <!-- formulario orcamento -->
              <div class="top20 left20">

                <div class="col-xs-12 top15 form-group">
                  <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="ASSUNTO">
                </div>

                <div class="col-xs-6  top15  form-group ">
                  <input type="text" name="nome" class="form-control fundo-form input100" placeholder="NOME">
                </div>

                <div class="col-xs-6  top15 form-group ">
                  <input type="text" name="email" class="form-control fundo-form input100" placeholder="E-MAIL">
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-6  top15 form-group">
                  <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="TELEFONE">
                </div>

                <div class="col-xs-6  top15 form-group">
                  <input type="text" name="escolaridade" class="form-control fundo-form input100" placeholder="ESCOLARIDADE">
                </div>

                <div class="clearfix"></div>


                <div class="col-xs-6  top15 form-group">
                  <input type="text" name="cargo" class="form-control fundo-form input100" placeholder="CARGO PRETEDIDO">
                </div>

                <div class="col-xs-6  top15 form-group">
                  <input type="file" name="curriculo" class="form-control fundo-form input100" placeholder="CURRICULO">
                </div>


                <div class="clearfix"></div>

                <div class="col-xs-12 top15 form-group">
                  <textarea name="mensagem" id="" cols="30" rows="10"  placeholder="MENSAGEM" class="form-control  fundo-form input100"></textarea>
                </div>


                <div class="clearfix"></div>

                <div class="col-xs-12 top15">

                  <div class="pull-right">
                    <button type="submit" class="btn btn_servico  btn-lg" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>
                </div>


              </div>
              <!-- formulario orcamento -->
            </div>
          </form>

          <!-- Tab panes -->


        </div>
      </div>

    </div>
  </div>



  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->
  <div class="container-fluid container_mapa">
    <div class="row ">

      <div class="container top30">
        <div class="row">

          <div class="col-xs-3 text-right top15">
            <h4 class="right15">NOSSA</h4>
            <h2 class="right15 bottom5 "><span>LOCALIZAÇÃO</span></h2>
          </div>
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="371" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Email: ".$_POST[email]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo Pretedido: ".$_POST[cargo]." <br />
  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";

  Util::envia_email($config[email_trabalhe_conosco], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), ($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>


<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
